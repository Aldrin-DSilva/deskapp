package gnukhata.controllers;


import java.util.List;
import java.util.ArrayList;

import gnukhata.globals;

import org.apache.xmlrpc.XmlRpcException;

import sun.text.CodePointIterator;

public class accountController {
	/*
	 * @returns Object[] containing
	 * 	1.group_names.(String[])
	 * 	2.accounting preferences.(Object)
	 * 	3.Debt opening balance.(Object)
	 * 	4.credit opening balance.(Object)
	 */
	public static String[] getAllGroups()
	{
		try
		{
			Object[] result = (Object[])  globals.client.execute("groups.getAllGroups", new Object[]{globals.session[0]});
			String[] groups = new String[result.length];
			for (int i = 0;i < result.length;i++)
			{
				Object[] groupRow = (Object[]) result[i];
				groups[i] = groupRow[1].toString(); 
				
			}
			return groups;
		}
		catch(Exception e)
		{
			e.getMessage();
			return new String[]{};
		}
		
	}
	
	public static Object[] create_account()
	{
		try {
			Object[] groups = (Object[]) gnukhata.globals.client.execute("groups.getAllGroups",new Object[]{gnukhata.globals.session[0]});
			String groups_str[]=new String[groups.length+1];
			groups_str[0]="Please_select_group";
			for(int i=1;i<groups.length;i++)
			{
				Object[] obj=(Object[])groups[i];
					System.out.println(obj[1].toString());
					groups_str[i]=obj[1].toString();
			}
			
			System.out.println("Org pref : "+gnukhata.globals.client.execute("organisation.getPreferences",new Object[]{new Object[]{2},gnukhata.globals.session[0]}));
		//	System.out.println("account.getDrOpeningBalance: "+gnukhata.controllers.xmlrpcconnect.getConnection().execute("account.getDrOpeningBalance",new Object[]{gnukhata.globals.session[0]}));
		//	System.out.println("getCrOpeningBalance: "+gnukhata.controllers.xmlrpcconnect.getConnection().execute("account.getCrOpeningBalance",new Object[]{gnukhata.globals.session[0]}));
		Object orgpref =gnukhata.globals.client.execute("organisation.getPreferences",new Object[]{new Object[]{2},gnukhata.globals.session[0]});
			Object dropen=gnukhata.globals.client.execute("account.getDrOpeningBalance",new Object[]{gnukhata.globals.session[0]});
			Object cropen=gnukhata.globals.client.execute("account.getCrOpeningBalance",new Object[]{gnukhata.globals.session[0]});
			return new Object[]{groups_str,orgpref,dropen,cropen};
			
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Object[]{};
		}
			
		
	}
	/*
	 * 	@param Object[] param contains only one parameter
	 * 	which is the name of the group.
	 * 	@return String[] sub-groups for the group which is params[1].
	 */
	public static String[] getSubGroups(String param)
	{
		//System.out.println("in getSubgroup");
		String[] subgroups;
		Object[] queryParams = new Object[]{param};
		List< Object> serverParams = new ArrayList<Object>();
		serverParams.add(queryParams);
		serverParams.add(globals.session[0]);
		
		try {
			Object[] result=(Object[])gnukhata.globals.client.execute("groups.getSubGroupsByGroup",serverParams);

			System.out.println(Integer.valueOf(result.length));
				if(result.length == 0)
				{
					System.out.println("no data available");
					subgroups=new String[]{"No Sub-Group","Create New Sub-Group"};
				}
				else{
					System.out.println("data available");
					System.out.println(result.length+" no of subgroups");
				subgroups=new String[result.length+1];
				for(int i=0;i<result.length;i++)
				{	Object[] obj=(Object[])result[i];
						subgroups[i]=obj[0].toString();
						
				}
				subgroups[result.length]="Create New Sub-Group";
				}
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			subgroups=new String[]{};
		}
		return subgroups;
	}
	

	
	/*
	 * @param Object[] param accepts 8 objects
	 * 	1.group name
	 * 	2.subgroup name
	 * 	3.new subgroup name;(if you are creating new subgroup.);
	 * 	4.account name.
	 * 	5.account code.
	 * 	6.opening balance.
	 * 	7.currentBalance(which is same as the opening balance and is 
	 * 					@deprecated in the current version of gnukhata.).
	 * 	8.suggested code
	 */
	public static boolean setAccount(Object[] queryParams)
	{
		List<Object> serverParams = new ArrayList<Object>();
		if(queryParams[1].toString().equals("Create New Sub-Group"))
		{
			Object[] subGroupParams = new Object[]{queryParams[0].toString(), queryParams[2].toString()};
			try {
				serverParams.add(subGroupParams);
				serverParams.add(globals.session[0]);
				Object success = globals.client.execute("groups.setSubGroup",serverParams);

				serverParams.clear();
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.getMessage();
				return false;
			}
			
		}
		
		serverParams.add(queryParams);
		serverParams.add(globals.session[0]);
		
		try {
			Object result = globals.client.execute("account.setAccount",serverParams);
			return true;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return false;
		}
		
		
	}
	
	public static String subgroupExists(String subGroupName)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{subGroupName} );
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("groups.subgroupExists",serverParams);
			System.out.println(result.toString());
			return result.toString();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return e.getMessage();
		}
	}
	public static String accountExists(String AccountName)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{AccountName} );
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("account.accountExists",serverParams);
			System.out.println(result.toString());
			return result.toString();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return e.getMessage();
		}
	}
	public static String getSuggestedAccountCode(String codeParam)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{codeParam});
		serverParams.add(globals.session[0]);
		
		try {
			Object result = globals.client.execute("account.getSuggestedCode",serverParams );
			return result.toString();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return "";
		}
	}
	public static double getTotalDr()
	{
		try {
			Object result = globals.client.execute("account.getDrOpeningBalance", new Object[]{globals.session[0]});
			return Double.valueOf(result.toString());
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return 0.00;
		}
		
	}
	public static double getTotalCr()
	{
		try {
			Object result = globals.client.execute("account.getCrOpeningBalance", new Object[]{globals.session[0]});
			return Double.valueOf(result.toString());
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return 0.00;
		}
		
	}
	
	public static String[] getAccountByGroup(String groupname)
	{
		try
		{
			Object[] result = (Object[]) globals.client.execute("account.getAccountByGroup",new Object[]{globals.session[0]});
			String[] accounts = new String[result.length];
			for(int i = 0; i<result.length; i++)
			{

				accounts[i] = result[i].toString();
			}
			return accounts;
		}
		catch(Exception e)
		{
			e.getMessage();
			return new String[]{};
		}
	}
	
	public static String[] getAllAccounts()
	{
		try
		{
			Object[] result = (Object[]) globals.client.execute("account.getAllAccounts",new Object[]{globals.session[0]});
			String[] accounts = new String[result.length];
			for(int i = 0; i<result.length; i++)
			{

				accounts[i] = result[i].toString();
			}
			return accounts;
		}
		catch(Exception e)
		{
			e.getMessage();
			return new String[]{};
		}
	}
		public static String[] getAccountDetails(int searchFlag, String searchValue)
		{
			ArrayList<Object> serverParams = new ArrayList<Object>();
			serverParams.add(new Object[]{searchFlag,searchValue });
			serverParams.add(globals.session[0]);
			
			try {
				Object[] result  = (Object[]) globals.client.execute("account.getAccount",serverParams);
				String[] details = new String[result.length];
				for (int i = 0; i < result.length;i++)
				{
					details[i] = result[i].toString();
				}
				return details;
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.getMessage();
				return new String[]{};
			}
		}
			public static double editAccount(String accountName,String accountCode, String groupname,double openingBalance)
			{
				ArrayList<Object> serverParams = new ArrayList<Object>();
				Object[] queryParams = new Object[]{accountName,accountCode,groupname, openingBalance};
				serverParams.add(queryParams);
				serverParams.add(globals.session[0]);
				try {
					Object result = globals.client.execute("account.editAccount",serverParams);
					return Double.valueOf(result.toString());
				} catch (XmlRpcException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return 0.00;
				}
				
			}
			public static boolean deleteAccount(String accountName)
			{
				ArrayList<Object> serverParams = new ArrayList<Object>();
				serverParams.add(new Object[]{accountName} );
				serverParams.add(globals.session[0]);
				try {
					Object Result = globals.client.execute("account.deleteAccount",serverParams);
					return true;
				} catch (XmlRpcException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				
			}
		}
	
