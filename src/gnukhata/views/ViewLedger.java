package gnukhata.views;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import gnukhata.globals;
import gnukhata.controllers.accountController;
import gnukhata.controllers.transactionController;
import gnukhata.controllers.reportController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
//import org.eclipse.swt.widgets.Widget;
import org.eclipse.swt.widgets.Text;

public class ViewLedger extends Composite{
	
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	String accountName;
	String fromDate;
	String toDate;
	String searchText = "";
	long searchTexttimeout = 0;
	
	String[] financialYear;
	
	Label lblorgName;
	Label lblAccountName;
	Combo dropdownAccountName;
	Combo dropdownFinancialYear;
	
	Label lblPeriod;
	Label lblFromDt;
	Label lblToDt;
	Label lblFinancialYear;
	
	Text txtFromDtDay;
	Text txtToDtDay;
	Text txtFromDtMonth;
	Text txtToDtMonth;
	Text txtFromDtYear;
	Text txtToDtYear;
	
	Label lblFromDash1;
	Label lblFromDash2;
	Label lblToDash1;
	Label lblToDash2;
	Label lblFeatures;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblledgerTitle;
	Label lblSelectProject;
	String oldaccName;
	String oldfromdate;
	String oldenddate;
	String oldprojectName;
	String oldselectproject;
	boolean narration;
	boolean tbflag;
	boolean projectflag;
	boolean ledgerflag;
	
	Combo dropdownProjectName;
	Button btnView;
	Button btnCheck;
	boolean dualflag=false;
	String ProjectName;
	
	long wait=0;
	String tb;
		
		public ViewLedger(Composite parent, int style,String accname,String startDate,String endDate,String ledgerProject,boolean narrationflag1,boolean tbDrilldown,boolean psDrilldown,String tbtype,String projectName,boolean dualledgerflag) {
			super(parent,style);
		// TODO Auto-generated constructor stub
			strOrgName = globals.session[1].toString();
			strFromYear =  globals.session[2].toString();
			strToYear =  globals.session[3].toString();
			
			oldaccName=accname;
			oldfromdate=startDate;
			oldenddate=endDate;
			oldprojectName=ledgerProject;
			oldselectproject=projectName;
			narration=narrationflag1;
			tbflag=tbDrilldown;
			projectflag=psDrilldown;
			tb=tbtype;
			
			dualflag=dualledgerflag;
			
			
			FormLayout formLayout= new FormLayout();
			this.setLayout(formLayout);
		    FormData layout=new FormData();
		    MainShell.lblLogo.setVisible(false);
			MainShell.lblLine.setVisible(false);
			MainShell.lblOrgDetails.setVisible(false);
			    
		  
			Label lblOrgDetails = new Label(this,SWT.NONE);
			lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
			lblOrgDetails.setText(strOrgName+ "\n"+"For Financial Year "+"From "+strFromYear+" To "+strToYear);
			layout.top = new FormAttachment(2);
			layout.left = new FormAttachment(2);
			lblOrgDetails.setLayoutData(layout);
			
			lblLogo = new Label(this, SWT.None);
			layout = new FormData();
			layout.top = new FormAttachment(2);
			layout.left = new FormAttachment(60);
			lblLogo.setLayoutData(layout);
			//Image img = new Image(display, "finallogo1.png");
			lblLogo.setImage(globals.logo);
				
			lblLine = new Label(this, SWT.NONE);
			lblLine.setText("--------------------------------------------------------------------------------------------------------");
			lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(lblLogo , 2);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(99);
			lblLine.setLayoutData(layout);
			
			lblledgerTitle=new Label(this,SWT.NONE);
			lblledgerTitle.setText("View Ledger");
			lblledgerTitle.setFont(new Font(display, "Times New Roman", 20, SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(23);
			layout.left = new FormAttachment(45);
			lblledgerTitle.setLayoutData(layout);
		
			lblAccountName= new Label(this,SWT.NONE);
			lblAccountName.setText("A&ccount Name : ");
			lblAccountName.setFont(new Font(display, "Times New Roman", 16, SWT.BOLD));
			layout=new FormData();
			layout.top=new FormAttachment(lblledgerTitle,40);
			layout.left=new FormAttachment(40);
			lblAccountName.setLayoutData(layout);
			
			dropdownAccountName = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
			layout = new FormData();
			layout.top = new FormAttachment(lblledgerTitle,40);
			layout.left = new FormAttachment(lblAccountName,7);
			layout.right = new FormAttachment(74);
			layout.bottom = new FormAttachment(15);
			dropdownAccountName.add("----------Please select----------");
			dropdownAccountName.setLayoutData(layout);
			dropdownAccountName.setFocus();
			
			lblPeriod=new Label(this, SWT.NONE);
			lblPeriod.setText("Period :");
			lblPeriod.setFont(new Font(display, "Times New Roman", 16, SWT.BOLD));
			layout=new FormData();
			layout.top=new FormAttachment(lblAccountName,31);
			layout.left=new FormAttachment(40);
			lblPeriod.setLayoutData(layout);
		
			lblFromDt=new Label(this, SWT.NONE);
			lblFromDt.setText("&From :");
			lblFromDt.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL));
			layout=new FormData();
			layout.top =new FormAttachment(lblPeriod, 15);
			layout.left=new FormAttachment(48);		
			lblFromDt.setLayoutData(layout);
		    
			txtFromDtDay=new Text(this, SWT.BORDER);
			txtFromDtDay.setMessage("dd");
			txtFromDtDay.setText(strFromYear.substring(0,2));	
			txtFromDtDay.setTextLimit(2);
			layout = new FormData();
			layout.top=new FormAttachment(lblPeriod, 5);
			layout.left = new FormAttachment(lblFromDt, 3);
			txtFromDtDay.setLayoutData(layout);
			txtFromDtDay.setVisible(true);
					
			lblFromDash1 = new Label(this, SWT.NONE);
			lblFromDash1.setText("-");
			lblFromDash1.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
			layout = new FormData();
			layout.left = new FormAttachment(txtFromDtDay,2);
			layout.top = new FormAttachment(lblPeriod, 5);
			lblFromDash1.setLayoutData(layout);
			lblFromDash1.setVisible(true);
			
			txtFromDtMonth=new Text(this, SWT.BORDER);
			txtFromDtMonth.setMessage("mm");
			txtFromDtMonth.setText(strFromYear.substring(3,5));
			txtFromDtMonth.setTextLimit(2);
			layout = new FormData();
			layout.top=new FormAttachment(lblPeriod, 5);
			layout.left = new FormAttachment(lblFromDash1, 3);
			txtFromDtDay.setTabs(1);
			txtFromDtMonth.setLayoutData(layout);
			txtFromDtMonth.setVisible(true);
			
			
			lblFromDash2 = new Label(this, SWT.NONE);
			lblFromDash2.setText("-");
			lblFromDash2.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
			layout = new FormData();
			layout.left = new FormAttachment(txtFromDtMonth,3);
			layout.top = new FormAttachment(lblPeriod, 5);
			lblFromDash2.setLayoutData(layout);
			lblFromDash2.setVisible(true);
			
			
			txtFromDtYear=new Text(this, SWT.BORDER);
			txtFromDtYear.setMessage("yyyy");
			txtFromDtYear.setTextLimit(4);
			txtFromDtYear.setText(strFromYear.substring(6));
			layout = new FormData();
			layout.top=new FormAttachment(lblPeriod, 5);
			layout.left = new FormAttachment(lblFromDash2, 3);
			txtFromDtYear.setLayoutData(layout);
			
			lblToDt=new Label(this, SWT.NONE);
			lblToDt.setText("T&o     :");
			lblToDt.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL));
			layout=new FormData();
			layout.top =new FormAttachment(lblFromDt, 14);
			layout.left=new FormAttachment(48);
			lblToDt.setLayoutData(layout);
			
			txtToDtDay=new Text(this, SWT.BORDER);
			txtToDtDay.setMessage("dd");
			txtToDtDay.setText(strToYear.substring(0,2));
			txtToDtDay.setTextLimit(2);
			layout = new FormData();
			layout.top=new FormAttachment(lblFromDt, 14);
			layout.left = new FormAttachment(lblToDt, 3);
			txtToDtDay.setLayoutData(layout);
			txtToDtDay.setVisible(true);
			
			lblToDash1 = new Label(this, SWT.NONE);
			lblToDash1.setText("-");
			lblToDash1.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
			layout = new FormData();
			layout.left = new FormAttachment(txtToDtDay,2);
			layout.top = new FormAttachment(lblFromDt, 14);
			lblToDash1.setLayoutData(layout);
			lblToDash1.setVisible(true);
			
			txtToDtMonth=new Text(this, SWT.BORDER);
			txtToDtMonth.setMessage("mm");
			txtToDtMonth.setText(strToYear.substring(3,5));
			txtToDtMonth.setTextLimit(2);
			layout = new FormData();
			layout.top=new FormAttachment(lblFromDt, 14);
			layout.left = new FormAttachment(lblFromDash1, 3);
			txtToDtMonth.setLayoutData(layout);
			txtToDtMonth.setVisible(true);
			
			
			lblToDash2 = new Label(this, SWT.NONE);
			lblToDash2.setText("-");
			lblToDash2.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
			layout = new FormData();
			layout.left = new FormAttachment(txtToDtMonth,3);
			layout.top = new FormAttachment(lblFromDt, 14);
			lblToDash2.setLayoutData(layout);
			lblToDash2.setVisible(true);
			
			
			txtToDtYear=new Text(this, SWT.BORDER);
			txtToDtYear.setMessage("yyyy");
			txtToDtYear.setText(strToYear.substring(6));	
			txtToDtYear.setTextLimit(4);
			layout = new FormData();
			layout.top=new FormAttachment(lblFromDt, 14);
			layout.left = new FormAttachment(lblToDash2, 3);
			txtToDtYear.setLayoutData(layout);
			txtToDtYear.setVisible(true);
			
			btnCheck=new Button(this, SWT.CHECK);
			btnCheck.setText("&With Narration");
			layout = new FormData();
			layout.top=new FormAttachment(lblToDt, 19);
			layout.left = new FormAttachment(52);
			btnCheck.setLayoutData(layout);
			btnCheck.setVisible(true);
			
			lblSelectProject= new Label(this,SWT.NONE);
			lblSelectProject.setText("&Select Projects : ");
			lblSelectProject.setFont(new Font(display, "Times New Roman", 16, SWT.BOLD));
			layout=new FormData();
			layout.top=new FormAttachment(btnCheck, 34);
			layout.left=new FormAttachment(40);
			lblSelectProject.setLayoutData(layout);
			
			dropdownProjectName= new Combo(this, SWT.DROP_DOWN| SWT.READ_ONLY);
			layout=new FormData();
			layout.top=new FormAttachment(btnCheck, 34);
			layout.left=new FormAttachment(lblSelectProject,14);
			layout.right=new FormAttachment(70);
			//layout.bottom=new FormAttachment(45);
			dropdownProjectName.setLayoutData(layout);
		    dropdownProjectName.setVisible(true);
			dropdownProjectName.add("No Project");
			
			if(dropdownProjectName.getSelectionIndex()==-1)
			{
				dropdownProjectName.setVisible(false);
				lblSelectProject.setVisible(false);
			}
			
			String[] allProjects = gnukhata.controllers.transactionController.getAllProjects();
			for (int i = 0; i < allProjects.length; i++ )
			{
				lblSelectProject.setVisible(true);
				dropdownProjectName.setVisible(true);
				dropdownProjectName.add(allProjects[i]);
			}
			dropdownProjectName.select(0);
		
			
			btnView= new Button(this, SWT.PUSH);
			btnView.setText(" &View ");
			btnView.setFont(new Font(display, "Times New Roman", 14,SWT.BOLD));
			btnView.setEnabled(false);
			layout = new FormData();
			layout.top=new FormAttachment(dropdownProjectName, 34);
			layout.left = new FormAttachment(53);
			layout.right = new FormAttachment(60);
			
			
			
			
			btnView.setLayoutData(layout);
			String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
			
			

			for (int i = 0; i < allAccounts.length; i++ )
			{
				dropdownAccountName.add(allAccounts[i]);
	
			}
			dropdownAccountName.select(0);
	
			this.getAccessible();
			this.pack();
			
			this.setEvents();
	}
		
		private void setEvents()
		{
			
			
			dropdownAccountName.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					if (dropdownAccountName.getSelectionIndex() ==0)
					{
						btnView.setEnabled(false);
						return;
					}
					btnView.setEnabled(true);
					
					}
					

				
			});
			dropdownAccountName.addKeyListener(new KeyAdapter() {
			
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
						
						if (dropdownAccountName.getSelectionIndex() == 0)
						{
							//dropdownAccountName.setFocus();
							MessageBox	 msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							msg.setMessage("Please select the Account name.");
							msg.open();
							dropdownAccountName.setFocus();
						
							btnView.setEnabled(false);
							return;
						}
						txtFromDtDay.setFocus();
						txtFromDtDay.setSelection(0,2);
						btnView.setEnabled(true);
					}
					
					
				}
				});
			
			
			
			
			dropdownAccountName.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					//code here
					if(arg0.keyCode== SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
						dropdownAccountName.notifyListeners(SWT.Selection ,new Event()  );
						//dropdownFinancialYear.setFocus();
						return;
					}
					/*if(!Character.isLetterOrDigit(arg0.character) )
					{
						return;
					}
					
					*/
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout)
					{
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 500;
					
					for(int i = 0; i < dropdownAccountName.getItemCount(); i++ )
					{
						if(dropdownAccountName.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							dropdownAccountName.select(i);
						}
					}
				}
			});
			
			
			
			txtFromDtDay.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if(txtFromDtDay.getText().length()==txtFromDtDay.getTextLimit())
					{
						//txtDtDOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						

						txtFromDtMonth.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownAccountName.setFocus();
					}
				

				}
			});
			
			txtFromDtDay.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
					if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
					{
						txtFromDtDay.setText("0"+ txtFromDtDay.getText());
						//txtFromDtMonth.setFocus();
						txtFromDtDay.setFocus();
						return;
						
						
						
					}
					}
				

				}
			});
			
			
			txtFromDtMonth.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(txtFromDtMonth.getText().length()==txtFromDtMonth.getTextLimit())
					{
						txtFromDtYear.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtFromDtDay.setFocus();
					}
				

				}
			});
			
			txtFromDtMonth.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
					if(!txtFromDtMonth.getText().equals("") && Integer.valueOf ( txtFromDtMonth.getText())<10 && txtFromDtMonth.getText().length()< txtFromDtMonth.getTextLimit())
					{
						txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
						//txtFromDtMonth.setFocus();
						txtFromDtYear.setFocus();
						return;
						
						
						
					}
					}
				

				}
			});
			
			
		txtFromDtYear.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(txtFromDtYear.getText().length()==txtFromDtYear.getTextLimit())
				{
					txtToDtDay.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtFromDtMonth.setFocus();
				}
			

			}
		});
			
		txtFromDtYear.addFocusListener(new FocusAdapter() {
			
			public void focusLost(FocusEvent args0) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date voucherDate = sdf.parse(txtFromDtYear.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText());
					Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
					Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
					
					if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
					{
						MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
						errMsg.setMessage("please enter the date within the financial year");
						errMsg.open();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtFromDtYear.setFocus();
							txtFromDtYear.setSelection(0,4);
							}
						});
						
						return;
					}
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.getMessage();
				}
			}
		});
		
txtToDtYear.addFocusListener(new FocusAdapter() {
			
			public void focusLost(FocusEvent args0) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date voucherDate = sdf.parse(txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText());
					Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
					Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
					
					if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
					{
						MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
						errMsg.setMessage("please enter the date within the financial year");
						errMsg.open();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtToDtYear.setFocus();
							txtToDtYear.setSelection(0,4);
							}
						});
						
						return;
					}
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.getMessage();
				}
			}
		});
		
		
		txtToDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(txtToDtDay.getText().length()==txtToDtDay.getTextLimit())
				{
					txtToDtMonth.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtFromDtYear.setFocus();
				}
			

			}
		});
		

		txtToDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!	txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && 	txtToDtDay.getText().length()< 	txtToDtDay.getTextLimit())
				{
					txtToDtDay.setText("0"+ txtToDtDay.getText());
					//txtFromDtMonth.setFocus();
					txtToDtMonth.setFocus();
					return;
					
					
					
				}
				}

			}
		});

		
			txtToDtMonth.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(txtToDtMonth.getText().length()==txtToDtMonth.getTextLimit())
					{
						txtToDtYear.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtToDtDay.setFocus();
					}
				

				}
			});
			

			txtToDtMonth.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
					if(!txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< 	txtToDtMonth.getTextLimit())
					{
						txtToDtMonth.setText("0"+ txtToDtMonth.getText());
						//txtFromDtMonth.setFocus();
						txtToDtYear.setFocus();
						return;
						
						
						
					}
					}

				}
			});

			txtToDtYear.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(txtToDtYear.getText().length()==txtToDtYear.getTextLimit())
					{
						btnCheck.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtToDtMonth.selectAll();
						txtToDtMonth.setFocus();
					}
				

				}
			});
			
			btnCheck.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{	
						if(dropdownProjectName.isVisible())
						{
							dropdownProjectName.setFocus();
						}
						else
						{
							btnView.setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtToDtYear.setFocus();
					}
				}
			});
			dropdownProjectName.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode ==SWT.CR)
					{
						//btnView.notifyListeners(SWT.Selection ,new Event() );
						if (dropdownProjectName.getSelectionIndex() == -1)
						{
							MessageBox	 msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							msg.setMessage("Please select the Project name.");
							msg.open();
							dropdownProjectName.setFocus();
							return;
						}
						btnView.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						if(dropdownProjectName.getSelectionIndex()<=1)
						{
						btnCheck.setFocus();
						}
					}
					
				}
				});
			
			btnView.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						if(dropdownProjectName.isVisible())
						{
							dropdownProjectName.setFocus();
						}
						else
						{
							btnCheck.setFocus();
						}
					}
				}
			});
			
			btnView.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//String dualledgerflag=btnView.getData("dualflag").toString();
					if(dropdownAccountName.getSelectionIndex()<=0 )
						
					{
						MessageBox err = new MessageBox(new Shell(),SWT.ERROR|SWT.OK);
						err.setMessage("Please select an account name for seeing it's ledger report");
						err.open();
						dropdownAccountName.setFocus();
						return;
					}
					
					
					if(dualflag==false)
						//make a call to the reportController.getLedger()
					
						{
							
							Composite grandParent = (Composite) btnView.getParent().getParent();
							//MessageBox msg = new MessageBox(new Shell(), SWT.OK);
							//msg.setMessage(grandParent.getText());
							//msg.open();
							String fromDate = txtFromDtYear.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText();
							String toDate = txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText();
							String ProjectName=dropdownProjectName.getItem(dropdownProjectName.getSelectionIndex());
							if(dropdownProjectName.isEnabled()==true)
							{
								ProjectName=dropdownProjectName.getItem(dropdownProjectName.getSelectionIndex());
							}
							if(dropdownProjectName.isEnabled()==false)
							{
								ProjectName="";
							}
					
							String accountname =  dropdownAccountName.getItem(dropdownAccountName.getSelectionIndex());
							boolean narrationFlag=btnCheck.getSelection();
					
					/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("Project name"+projectName);
					msg.open();*/
					
				//ViewLedger(Composite parent,String accnamesent,String fromDate,String enddate,String projectName,boolean narrationflag1,boolean tbDrilldown,boolean psDrilldown,String tbtype,String selectProject,boolean dualledgerflag, int style) {

					//dispose();
					gnukhata.controllers.reportController.showLedger(grandParent, accountname, fromDate, toDate, ProjectName, narrationFlag, false, false, "", "");
						btnView.getParent().dispose();
				//	dispose();
					
						}
					if(dualflag==true)
					{
						
						
						Composite grandParent = (Composite) btnView.getParent().getParent();
						//MessageBox msg = new MessageBox(new Shell(), SWT.OK);
						//msg.setMessage(grandParent.getText());
						//msg.open();
						String fromDate1= txtFromDtYear.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText();
						String toDate1 = txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText();
						String ProjectName1=dropdownProjectName.getItem(dropdownProjectName.getSelectionIndex());
						if(dropdownProjectName.isEnabled()==true)
						{
							ProjectName1=dropdownProjectName.getItem(dropdownProjectName.getSelectionIndex());
						}
						if(dropdownProjectName.isEnabled()==false)
						{
							ProjectName1="";
						}
				
						String accountname1 =  dropdownAccountName.getItem(dropdownAccountName.getSelectionIndex());
						boolean narrationFlag=btnCheck.getSelection();
					
						
							
						gnukhata.controllers.reportController.showDualLedger(grandParent, accountname1,oldaccName, fromDate1,oldfromdate, toDate1,oldenddate, ProjectName1,oldprojectName, narrationFlag,narration, false,false, false,false, "","","", "",true,true);
						
						//gnukhata.controllers.reportController.showLedger(grandParent, accountname, fromDate, toDate, ProjectName, narrationFlag, false, false, "", "",true);
						btnView.getParent().dispose();

				
					}
				}
			});
			
			
			
			//date validations
			
			txtFromDtDay.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(
						KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtToDtDay.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
						
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtToDtMonth.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			
			txtToDtYear.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtFromDtMonth.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtFromDtYear.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtFromDtDay.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtFromDtDay.getText().equals("") && (Integer.valueOf(txtFromDtDay.getText())> 31 || Integer.valueOf(txtFromDtDay.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setMessage("you have entered an invalid date");
						msgdateErr.open();
						
						txtFromDtDay.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtDay.setFocus();
								
							}
						});
						return;
					}
					if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
					{
						txtFromDtDay.setText("0"+ txtFromDtDay.getText());
						//txtFromDtMonth.setFocus();
						return;
						
						
						
					}
					if(txtFromDtDay.getText().equals(""))
					{
						txtFromDtDay.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtDay.setFocus();
								
							}
						});
						return;
				
					}
					/*if(txtFromDtDay.getText().length()==2)
					   {
						   txtFromDtMonth.setFocus();
					   }*/
				}
			});
			
			txtFromDtMonth.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtFromDtMonth.getText().equals("") && (Integer.valueOf(txtFromDtMonth.getText())> 12 || Integer.valueOf(txtFromDtMonth.getText()) <= 0))
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setMessage("you have entered an invalid month");
						msgdateErr.open();
					
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtMonth.setText("");
								txtFromDtMonth.setFocus();
								
							}
						});
						return;
						
					}
					if(! txtFromDtMonth.getText().equals("") && Integer.valueOf ( txtFromDtMonth.getText())<10 && txtFromDtMonth.getText().length()< txtFromDtMonth.getTextLimit())
					{
						txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
						return;
					}
					if(txtFromDtMonth.getText().equals(""))
					{
						
						//txtFromDtMonth.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtMonth.setFocus();
								
							}
						});
						return;
				
					}
					
				}
			});
			
			
			
				txtToDtDay.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && txtToDtDay.getText().length()< txtToDtDay.getTextLimit())
					{
						txtToDtDay.setText("0"+ txtToDtDay.getText());
						return;
						
					}
					if(!txtToDtDay.getText().equals("") && (Integer.valueOf(txtToDtDay.getText())> 31 || Integer.valueOf(txtToDtDay.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setMessage("you have entered an invalid date");
						msgdateErr.open();
						
						txtToDtDay.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtDay.setFocus();
								
							}
						});
						return;
					}
					if(txtToDtDay.getText().equals(""))
					{
						txtToDtDay.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtDay.setFocus();
								txtToDtDay.selectAll();
							}
						});
						return;
					
					}
		
		}
});


				txtToDtMonth.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent arg0) {
						
				// TODO Auto-generated method stub
						//super.focusLost(arg0);
						if(!txtToDtMonth.getText().equals("") && (Integer.valueOf(txtToDtMonth.getText())> 12 || Integer.valueOf(txtToDtMonth.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
							msgdateErr.setMessage("you have entered an invalid month");
							msgdateErr.open();
						
							txtToDtMonth.setFocus();
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtToDtMonth.setText("");
									txtToDtMonth.setFocus();
									
								}
							});
							return;
							
						}
						if(! txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< txtToDtMonth.getTextLimit())
						{
							txtToDtMonth.setText("0"+ txtToDtMonth.getText());
							return;
						}
						
						if(txtToDtMonth.getText().equals(""))
						{
							//txtToDtDay.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtToDtMonth.setFocus();
									
								}
							});
							return;
						}
						
					}
				});
					
				txtFromDtYear.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						if(!txtFromDtYear.getText().trim().equals("") && Integer.valueOf(txtFromDtYear.getText()) < 0000) 
								{
							MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
							msgbox.setMessage("you have entered an invalid year");
							msgbox.open();
						
							txtFromDtYear.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtFromDtYear.setFocus();
									txtFromDtYear.selectAll();
									
								}
							});
							return;
						}
						if(txtFromDtYear.getText().equals(""))
						{
							txtFromDtYear.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtFromDtYear.setFocus();
									txtFromDtYear.selectAll();
									
								}
							});
							return;
						}
						
					
						
					}
				});

				txtToDtYear.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						if(!txtToDtYear.getText().trim().equals("") && Integer.valueOf(txtToDtYear.getText()) < 0000) 
								{
							MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
							msgbox.setMessage("you have entered an invalid year");
							msgbox.open();
						
							txtToDtYear.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtToDtYear.setFocus();
									txtToDtYear.selectAll();
									
								}
							});
							return;
						}
						if(txtToDtYear.getText().equals(""))
						{txtToDtYear.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtYear.setFocus();
								txtToDtYear.selectAll();
								
							}
						});
						}
						return;
					}
				});
				
			

		}


		public void makeaccessible(Control c) {
			/*
			 * getAccessible() method is the method of class Controlwhich is the
			 * parent class of all the UI components of SWT including Shell.so when
			 * the shell is made accessible all the controls which are contained by
			 * that shell are made accessible automatically.
			 */
			c.getAccessible();
		}

		protected void checkSubclass() {
			// this is blank method so will disable the check that prevents
			// subclassing of shells.
		}

		

	}