package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ChangePassword extends Composite{
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	Label lblusername;
	Label lbloldpassword;
	Label lblnewpassword;
	Label lblconfirmpassword;
	Text txtusername;
	Text txtoldpassword;
	Text txtnewpassword;
	Text txtconfirmpassword;
	Button btnsave;
	Button btncancel;
	
	public ChangePassword(Composite parent,int style) {
		super(parent, style);
		
		
		FormLayout formlayout =new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		 
		strToYear =  globals.session[3].toString();
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(67);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(75);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("--------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
			
		Label lblchangepass = new Label(this, SWT.NONE);
		lblchangepass.setText("Change Password");
		lblchangepass.setFont(new Font(display, "Times New Roman", 20, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblchangepass.setLayoutData(layout);
		
		Label lblusername = new Label(this, SWT.NONE);
		lblusername.setText("&Username:");
		lblusername.setFont(new Font(display, "Times New Roman", 16, SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblchangepass,20);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(36);
		lblusername.setLayoutData(layout);
		
		txtusername=new Text(this, SWT.BORDER);
		layout = new FormData();
		layout.top = new FormAttachment(lblchangepass,20);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(48);
		txtusername.setLayoutData(layout);
		
		lbloldpassword = new Label(this, SWT.NONE);
		lbloldpassword.setText("&Old Password :");
		lbloldpassword.setFont(new Font(display, "Times New Roman", 16, SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblusername,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(53);
		lbloldpassword.setLayoutData(layout);
		
		txtoldpassword = new Text(this, SWT.BORDER);
		txtoldpassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblusername,10);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(53);
		txtoldpassword.setLayoutData(layout);
		
		lblnewpassword = new Label(this, SWT.NONE);
		lblnewpassword.setText("&New Password :");
		lblnewpassword.setFont(new Font(display, "Times New Roman", 16, SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lbloldpassword,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(58);
		lblnewpassword.setLayoutData(layout);
		
		txtnewpassword = new Text(this, SWT.BORDER);
		txtnewpassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lbloldpassword,10);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(58);
		txtnewpassword.setLayoutData(layout);
		
		lblconfirmpassword = new Label(this, SWT.NONE);
		lblconfirmpassword.setText("Confirm &Password:");
		lblconfirmpassword.setFont(new Font(display, "Times New Roman", 16, SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblnewpassword,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(63);
		lblconfirmpassword.setLayoutData(layout);
		
		txtconfirmpassword = new Text(this, SWT.BORDER);
		txtconfirmpassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblnewpassword,10);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(63);
		txtconfirmpassword.setLayoutData(layout);
		
		btnsave = new Button(this,SWT.PUSH);
		btnsave.setText("&Save");
		btnsave.setFont(new Font(display, "Times New Roman", 16, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblconfirmpassword,20);
		layout.left = new FormAttachment(40);
	//	layout.right = new FormAttachment(50);
	//	layout.bottom = new FormAttachment(70);
		btnsave.setLayoutData(layout);
		
		btncancel = new Button(this,SWT.PUSH);
		btncancel.setText("&Cancel");
		btncancel.setFont(new Font(display, "Times New Roman", 16, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblconfirmpassword,20);
		layout.left = new FormAttachment(53);
	//	layout.right = new FormAttachment(65);
	//	layout.bottom = new FormAttachment(70);
		btncancel.setLayoutData(layout);
		
		this.pack();
		this.getAccessible();
		this.setEvents();
			
	}
	private void setEvents()
	{
		txtusername.setFocus();
		txtusername.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
						if(StartupController.changePassword(txtusername.getText(), txtoldpassword.getText(),txtnewpassword.getText()))
					{
						
							txtusername.selectAll();
						txtusername.setFocus();
					}
						if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
						{
							
								txtoldpassword.setFocus();
							
						}
						


					
				
			}
				
			
		});
		
/*		txtoldpassword.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0)
			{
			if(StartupController.login(txtusername.getText(), txtoldpassword.getText()))
			{
				txtnewpassword.selectAll();
				txtnewpassword.setFocus();
			}
			else

			{ 
				MessageBox msg= new MessageBox(new Shell(),SWT.OK|SWT.ERROR);
				msg.setMessage("please enter valid password");
				msg.open();
					
			    Display.getCurrent().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					txtoldpassword.selectAll();
					txtoldpassword.setFocus();
				}
			
				
			});
			return;
			}
			}
			
		});
		
*/	
		txtoldpassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						txtnewpassword.setFocus();
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtusername.setFocus();
				}

				
					}
				
			
		});
		
	
		txtnewpassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						txtconfirmpassword.setFocus();
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtoldpassword.setFocus();
				}

				
			}
		});
		txtconfirmpassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						btnsave.setFocus();
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtnewpassword.setFocus();
				}
			}
		});
		btnsave.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btncancel.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtconfirmpassword.setFocus();
				}
			}
		});
		btncancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnsave.setFocus();
				}
			}
		});
		
		
		
		btnsave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
					if (txtusername.getText().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please enter a User name");
						alert.open();
						txtusername.setFocus();
						return;
					}
					
						if (txtoldpassword.getText().equals(""))
						{
							MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							alert.setText("Error!");
							alert.setMessage("Please enter Old Password");
							alert.open();
							txtoldpassword.setFocus();
							return;
						}
						if (txtnewpassword.getText().equals(""))
						{
							MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							alert.setText("Error!");
							alert.setMessage("Please enter New Password");
							alert.open();
							txtnewpassword.setFocus();
							return;
						}
						if (txtconfirmpassword.getText().equals(""))
						{
							MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							alert.setText("Error!");
							alert.setMessage("Please Confirm your New Password");
							alert.open();
							txtconfirmpassword.setFocus();
							return;
						}
						/*if(StartupController.login(txtusername.getText(), txtoldpassword.getText()))
						{
							txtnewpassword.selectAll();
							txtnewpassword.setFocus();
						}
						if(!StartupController.login(txtusername.getText(), txtoldpassword.getText()))
						{ 
							MessageBox msg= new MessageBox(new Shell(),SWT.OK|SWT.ERROR);
							msg.setMessage("please enter valid password");
							msg.open();
							txtoldpassword.selectAll();
							txtoldpassword.setFocus();
						}
						


					
*/			
						if(! txtnewpassword.getText().equals(txtconfirmpassword.getText()))
						{
							MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
							msg1.setMessage("Password doesnt match");
							msg1.open();
							txtconfirmpassword.selectAll();
							txtconfirmpassword.setFocus();
							return;
						

						}

				if (StartupController.changePassword(txtusername.getText(), txtoldpassword.getText(), txtnewpassword.getText()))
				{
					
					/*String uname= txtusername.getText();
					String oldpasswd=txtoldpassword.getText();
					String newpasswd=txtnewpassword.getText();
					String confirmpasswd=txtconfirmpassword.getText();*/
					
					MessageBox msg2= new MessageBox(new Shell(),SWT.OK);
					msg2.setMessage("Password Changed successfully");
					msg2.open();
					
					dispose();
					StartupController.showMainShell(display, 2);

				}
				else
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					msg.setMessage("Please enter valid Username or Password");
					msg.open();
					txtusername.selectAll();
					txtusername.setFocus();
					
				}
				
			}
			
		});
		btncancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				MessageBox msgConfirm = new MessageBox(new Shell(), SWT.YES| SWT.NO| SWT.ICON_QUESTION );
				msgConfirm.setMessage("Are you sure you want to cancel");
				int answer = msgConfirm.open();
				if(answer == SWT.YES)
				{
					btncancel.getShell().getDisplay().dispose();
					MainShell ms = new MainShell(display);
				}
				if(answer== SWT.NO)
				{
					txtusername.setFocus();				
				}
				
			}
			});
		
	}
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}
	

}

