package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.sun.org.apache.bcel.internal.generic.LLOAD;

public class createNewUser extends Composite  {
	
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	
	Label lblUserName;
	Text txtUserName;
	Label lblPassword;
	Text txtPassword;
	Label lblConfirmPassword;
	Text txtConfirmPassword;
	Label lblRole;
	Combo drpdwnRole;
	Button btnSave;
	Button btnCancel;

	Vector<Object> params;
	protected int[] orgNameList;
    public createNewUser(Composite parent,int style) 
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		strToYear =  globals.session[3].toString();
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 18, SWT.ITALIC ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(6);
		layout.left = new FormAttachment(5);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("---------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,2);
		layout.left = new FormAttachment(5);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		
		Label lblNewUser = new Label(this, SWT.NONE);
		lblNewUser.setText("Create New User:");
		lblNewUser.setFont(new Font(display, "Times New Roman", 20, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblNewUser.setLayoutData(layout);
		
		
		lblUserName = new Label(this, SWT.NONE);
		lblUserName.setText("&User Name :");
		lblUserName.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblNewUser,52);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(52);
		//layout.bottom = new FormAttachment(48);
		lblUserName.setLayoutData(layout);
		
		txtUserName = new Text(this, SWT.BORDER);
		txtUserName.setText("");
		layout = new FormData();
		layout.top = new FormAttachment(lblNewUser,52);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(49);
		txtUserName.setLayoutData(layout);
		
		lblPassword = new Label(this, SWT.NONE);
		lblPassword.setText("&Password :");
		lblPassword.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblUserName,10);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(52);
		//layout.bottom = new FormAttachment(65);
		lblPassword.setLayoutData(layout);
		
		txtPassword = new Text(this, SWT.BORDER);
		txtPassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblUserName,10);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(54);
		txtPassword.setLayoutData(layout);
		
		lblConfirmPassword = new Label(this, SWT.NONE);
		lblConfirmPassword.setText("C&onfirm Password :");
		lblConfirmPassword.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblPassword,10);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(52);
		//layout.bottom = new FormAttachment(65);
		lblConfirmPassword.setLayoutData(layout);
		
		txtConfirmPassword = new Text(this, SWT.BORDER);
		txtConfirmPassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblPassword,10);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
	     //layout.bottom = new FormAttachment(54);
		txtConfirmPassword.setLayoutData(layout);
		
		lblRole = new Label(this, SWT.NONE);
		lblRole.setText("S&elect Role:");
		lblRole.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblConfirmPassword,10);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(52);
		//layout.bottom = new FormAttachment(65);
		lblRole.setLayoutData(layout);
		
		
		drpdwnRole = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		layout = new FormData();
		layout.top = new FormAttachment(lblConfirmPassword,10);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(14);
		drpdwnRole.setLayoutData(layout);
		
		/*drpdwnRole.add("--Please select--");
		drpdwnRole.select(0);*/

		drpdwnRole.add("Manager");
		
		drpdwnRole.add("Operator");
		
		
		btnSave = new Button(this,SWT.PUSH);
		btnSave.setText("&Save");
		btnSave.setFont(new Font(display, "Times New Roman", 14, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblRole,40);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(47);
		//layout.bottom = new FormAttachment(65);
		btnSave.setEnabled(false);
		btnSave.setLayoutData(layout);
		
		
		btnCancel = new Button(this,SWT.PUSH);
		btnCancel.setText("&Cancel");
		btnCancel.setFont(new Font(display, "Times New Roman", 14, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblRole,40);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(60);
		//layout.bottom = new FormAttachment(65);
		btnCancel.setLayoutData(layout);
	
	
		this.getAccessible();
		this.setEvents();
		this.pack();
	
		
		
		
}
    
    
    private void setEvents()
	{
    	txtUserName.setFocus();
		
		txtUserName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if (txtUserName.getText().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please enter a User name");
						alert.open();
						txtUserName.setFocus();
						return;
					}
					else
					{
						txtPassword.setFocus();
					}
				}
				
			}
		});
		
		
		txtPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if (txtPassword.getText().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please enter Password");
						alert.open();
						txtPassword.setFocus();
						return;
					}
					else
					{
						txtConfirmPassword.setFocus();
					}
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtUserName.setFocus();					
				}
			}
		});
		
		
		
		txtConfirmPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if (txtConfirmPassword.getText().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please Confirm your Password");
						alert.open();
						txtConfirmPassword.setFocus();
						return;
					}
					else
					{
						drpdwnRole.setFocus();
					}
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtPassword.setFocus();					
				}
			}
		});
		
		
		drpdwnRole.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				if(drpdwnRole.getSelectionIndex()>= 0 )
				{
					btnSave.setEnabled(true);
					
				}
				else
				{
					btnSave.setEnabled(false);
				}
			}
		});
	

    	
		drpdwnRole.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if(drpdwnRole.getSelectionIndex()>= 0  )
					{
						btnSave.setFocus();
					}
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					if(drpdwnRole.getSelectionIndex()==0)
					{
					txtConfirmPassword.setFocus();	
					}
				}
			}
		});
		btnSave.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
				if(e.keyCode==SWT.ARROW_RIGHT)
				{
					btnCancel.setFocus();
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					drpdwnRole.setFocus();
				}
			}
		});
		btnCancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnSave.setFocus();
				}
			}
		});
		
			btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				int role = drpdwnRole.getSelectionIndex();
				if(StartupController.createuser(txtUserName.getText(), txtPassword.getText(), role))
				{
				if(txtPassword.getText().equals(txtConfirmPassword.getText()))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setMessage("user created");
					msg1.open();
					dispose();
					StartupController.showMainShell(display, 2);
				}
				else
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setMessage("Password doesnt match");
					msg1.open();
					txtConfirmPassword.selectAll();
					txtConfirmPassword.setFocus();
					return;
				}
				}
			}
				
		});
			btnCancel.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					MessageBox msgConfirm = new MessageBox(new Shell(), SWT.YES| SWT.NO| SWT.ICON_QUESTION );
					msgConfirm.setMessage("Are you sure you want to cancel");
					int answer = msgConfirm.open();
					if(answer == SWT.YES)
					{
						
						btnCancel.getShell().getDisplay().dispose();
						MainShell ms = new MainShell(display);
					}
					if(answer== SWT.NO)
					{
						txtUserName.setFocus();
					}
					
				}
				});
	}


    public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}
	
    protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
}