package gnukhata.views;

import java.util.ArrayList;
import java.util.Vector;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class ViewUnclearedAccounts extends Composite 
{
	String strOrgName;
	String strFromYear;
	String strToYear;
	int counter=0;

	Table ledgerReconReport;
	TableColumn Srno;
	TableColumn Date;
	TableColumn Particulars;
	TableColumn VoucherNumber;
	TableColumn Dr;
	TableColumn Cr;
	TableColumn Narration;
	TableColumn Clrdate;
	TableColumn Memo;
	TableColumn Remove;
	TableItem headerRow;
	Label lblOrgDetails;
	Label lblheadline;
	Label lblorgname;
	Label lblsrno;
	Label lblDate;
	Label lblParticulars;
	Label lblVoucherNumber;
	Label lblNarration;
	Label lblDr;
	Label lblCr;
	Label lblclrdate;
	Label lblmemo;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblPageName;
	Label lblPeriod;
	TableEditor srnoeditor;
	TableEditor DateEditor;
	TableEditor ParticularEditor;
	TableEditor VoucherNumberEditor;
	TableEditor DrEditotr;
	TableEditor CrEditor;
	TableEditor clreditor;
	TableEditor memoeditor;
	
	TableEditor NarrationEditor;
	static Display display;
	
	Button btnViewBankReco;
	Button btnRemove;
	
	Label cleareditem;
	Button Btnclear;
	String strFromDate;
	String strToDate;
	
	
	
	String bankname="";
	String projectName="";
	String ledgerProject;
	boolean narration;
	boolean projectflag;
	boolean clearflag;
	Button btnok;
	String ToDate;
	String FromDate;
	
	Label lblremove;
	TableEditor removeeditor;
	Button chkremove;
	
	ArrayList<Text> txtcleardates = new ArrayList<Text>();
	ArrayList<Text>  txtmemos= new ArrayList<Text>();
	ArrayList<Button> chkbtnRemoves=new ArrayList<Button>();
//	Vector<Object> reconResult = new Vector<Object>();

	public ViewUnclearedAccounts(Composite parent, int style,String selectbank,String fromdate,String toDate, boolean narrationFlag,String ProjectName, boolean clearedflag,Vector<Object> reconResult) 
	{
		super(parent, style);
		// TODO Auto-generated constructor stub
		
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		ToDate = toDate;
		FromDate = fromdate;
		narration= narrationFlag;
		clearflag = clearedflag;
		bankname=selectbank;
		projectName=ProjectName;
	
		//old values
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    
	    MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
	    
	    strFromDate=fromdate.substring(8)+"-"+fromdate.substring(5,7)+"-"+fromdate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();

		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		lblLogo.setLayoutData(layout);
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		lblOrgDetails.setLayoutData(layout);

		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		
		 lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(35);
		lblheadline.setLayoutData(layout);
	

		
		Label lblAccName=new Label(this, SWT.NONE);
		lblAccName.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
		lblAccName.setText("Bank Name: "+bankname);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(5);
		lblAccName.setLayoutData(layout);
		

		lblPageName = new Label(this, SWT.NONE);
		lblPageName.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		lblPageName.setText("Period From "+strFromDate+" To "+ strToDate);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(60);
		lblPageName.setLayoutData(layout);
		
	    ledgerReconReport= new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    ledgerReconReport.setLinesVisible (true);
		ledgerReconReport.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblPageName,10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(80);
		ledgerReconReport.setLayoutData(layout);
				
		btnRemove =new Button(this,SWT.PUSH);
		btnRemove.setText(" Remove ");
		btnRemove.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReconReport,8);
		layout.left=new FormAttachment(20);
		btnRemove.setLayoutData(layout);
		
		
		btnViewBankReco=new Button(this, SWT.PUSH);
		btnViewBankReco.setText(" Back To View Reconciliation");
		btnViewBankReco.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReconReport,8);
		layout.left=new FormAttachment(btnRemove,20);
		btnViewBankReco.setLayoutData(layout);
		

				
	  
	    this.makeaccssible(ledgerReconReport);
	    this.getAccessible();
	    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	    this.setReport(reconResult);
	    //this.pack();
	    this.setEvents();
	   

	}
	public void makeaccssible(Control c)
	{
		c.getAccessible();
	}
	private void setReport(Vector<Object> reconResult)
	{
		
		ledgerReconReport.setFocus();
		
		lblDate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDate.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		lblDate.setText("    Date   ");
		
		lblParticulars = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblParticulars.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblParticulars = new Label(ledgerReconReport,SWT.BORDER);
		lblParticulars.setText("    		Particulars		     ");
		
		lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblVoucherNumber.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER);
		lblVoucherNumber.setText(" V.Number");
		
		lblDr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDr.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblDr = new Label(ledgerReconReport,SWT.BORDER);
		lblDr.setText(" Debit ");
		
		lblCr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblCr.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblCr = new Label(ledgerReconReport,SWT.BORDER);
		lblCr.setText(" Credit ");
		
		lblNarration = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblNarration.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblNarration.setText("   Narration   ");
		
		lblclrdate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblclrdate.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblclrdate.setText("Clearance Date");
		
		lblmemo = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblmemo.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblmemo.setText("   Memo   ");
		
		lblremove = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblremove.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblremove.setText("   Remove   ");
		
		
		  final int ledgwidth = ledgerReconReport.getClientArea().width;
		  
		 
	    ledgerReconReport.addListener(SWT.MeasureItem, new Listener() 
		{
	    	@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				// Srno.setWidth(5 * ledgwidth / 100);
	    		if(narration==true)
				{
				
					Date.setWidth(8 * ledgwidth / 100);
					Particulars.setWidth(18 * ledgwidth / 100);
					VoucherNumber.setWidth(5 * ledgwidth / 100);
					Dr.setWidth(8 * ledgwidth / 100);
					Cr.setWidth(8 * ledgwidth / 100);
					Narration.setWidth(22 * ledgwidth / 100);
					Clrdate.setWidth(10 * ledgwidth / 100);
					Memo.setWidth(14 * ledgwidth / 100);
					Remove.setWidth(7* ledgwidth / 100);
					
				}
	    		else
				{
					Date.setWidth(10 * ledgwidth / 100);
					Particulars.setWidth(25 * ledgwidth / 100);
					VoucherNumber.setWidth(5 * ledgwidth / 100);
					Dr.setWidth(15 * ledgwidth / 100);
					Cr.setWidth(15 * ledgwidth / 100);
					Clrdate.setWidth(10 * ledgwidth / 100);
					Memo.setWidth(14 * ledgwidth / 100);
					Remove.setWidth(7* ledgwidth / 100);
		
				}


								
				event.height = 11;
			    
			    
			};
		});
		
	    headerRow = new TableItem(ledgerReconReport, SWT.NONE);
		TableItem[] items = ledgerReconReport.getItems();
		Date = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
		Particulars = new TableColumn(ledgerReconReport, SWT.BORDER);
		VoucherNumber = new TableColumn(ledgerReconReport, SWT.RIGHT);
		Dr= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Cr= new TableColumn(ledgerReconReport, SWT.RIGHT);
		if(narration==true)
		{
			Narration=new TableColumn(ledgerReconReport, SWT.BORDER);
		}
		Clrdate= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Memo= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Remove=new TableColumn(ledgerReconReport, SWT.RIGHT);

		
			
		if(narration==true)
		{
		
	    DateEditor = new TableEditor(ledgerReconReport);
		DateEditor.grabHorizontal = true;
		DateEditor.setEditor(lblDate,items[0],0);
		
		ParticularEditor = new TableEditor(ledgerReconReport);
		ParticularEditor.grabHorizontal = true;
		ParticularEditor.setEditor(lblParticulars,items[0],1);
		
		VoucherNumberEditor = new TableEditor(ledgerReconReport);
		VoucherNumberEditor.grabHorizontal = true;
		VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
		
		DrEditotr = new TableEditor(ledgerReconReport);
		DrEditotr.grabHorizontal = true;
		DrEditotr.setEditor(lblDr,items[0],3);
		
		CrEditor = new TableEditor(ledgerReconReport);
		CrEditor.grabHorizontal = true;
		CrEditor.setEditor(lblCr,items[0],4);
		
		NarrationEditor = new TableEditor(ledgerReconReport);
		NarrationEditor.grabHorizontal = true;
		NarrationEditor.setEditor(lblNarration,items[0],5);
		
		
		clreditor = new TableEditor(ledgerReconReport);
		clreditor.grabHorizontal = true;
		clreditor.setEditor(lblclrdate,items[0],6);
		
		memoeditor = new TableEditor(ledgerReconReport);
		memoeditor.grabHorizontal = true;
		memoeditor.setEditor(lblmemo,items[0],7);
		
		removeeditor=new TableEditor(ledgerReconReport);
		removeeditor.grabHorizontal = true;
		removeeditor.setEditor(lblremove,items[0],8);
		
		Date.pack();
		Particulars.pack();
		VoucherNumber.pack();
	    Dr.pack();
		Cr.pack();
		Narration.pack();
		Clrdate.pack();
		Memo.pack();
		Remove.pack();
		}
		else
		{
			

		    DateEditor = new TableEditor(ledgerReconReport);
			DateEditor.grabHorizontal = true;
			DateEditor.setEditor(lblDate,items[0],0);
			
			ParticularEditor = new TableEditor(ledgerReconReport);
			ParticularEditor.grabHorizontal = true;
			ParticularEditor.setEditor(lblParticulars,items[0],1);
			
			VoucherNumberEditor = new TableEditor(ledgerReconReport);
			VoucherNumberEditor.grabHorizontal = true;
			VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
			
			DrEditotr = new TableEditor(ledgerReconReport);
			DrEditotr.grabHorizontal = true;
			DrEditotr.setEditor(lblDr,items[0],3);
			
			CrEditor = new TableEditor(ledgerReconReport);
			CrEditor.grabHorizontal = true;
			CrEditor.setEditor(lblCr,items[0],4);
			
			clreditor = new TableEditor(ledgerReconReport);
			clreditor.grabHorizontal = true;
			clreditor.setEditor(lblclrdate,items[0],5);
			
			memoeditor = new TableEditor(ledgerReconReport);
			memoeditor.grabHorizontal = true;
			memoeditor.setEditor(lblmemo,items[0],6);
			
			removeeditor=new TableEditor(ledgerReconReport);
			removeeditor.grabHorizontal = true;
			removeeditor.setEditor(lblremove,items[0],7);

			Date.pack();
			Particulars.pack();
			VoucherNumber.pack();
		    Dr.pack();
			Cr.pack();
			Clrdate.pack();
			Memo.pack();
			Remove.pack();
		}
		
		if(reconResult.size()==1)
		{
			Object[] unclearedResult = (Object[]) reconResult.get(0);
			for(int unclearedCounter =0; unclearedCounter < unclearedResult.length-7; unclearedCounter ++)
			{
				
				TableItem tbrow = new TableItem(ledgerReconReport , SWT.NONE);
				Object[] ledgerrecorecord = (Object[]) unclearedResult[unclearedCounter];
				tbrow.setText(0, ledgerrecorecord[0].toString());
				tbrow.setText(1, ledgerrecorecord[1].toString());
				tbrow.setText(2, ledgerrecorecord[2].toString());
				tbrow.setText(3, ledgerrecorecord[4].toString());
				tbrow.setText(4, ledgerrecorecord[5].toString());
				
				if(narration==true)
				{
					
					try {
						tbrow.setText(5,ledgerrecorecord[6].toString() );
					} catch (Exception e) {
						// TODO Auto-generated catch block
						tbrow.setText(5,"" );
					}
					tbrow.setText(6,"" );
					tbrow.setText(7,"");
					}
					else
					{
						tbrow.setText(5,"" );
						tbrow.setText(6,"" );
						tbrow.setText(7,"" );
					}
				
				}

		}
		
		if(reconResult.size()==2)
		{
			Object[] clearedResult= (Object[])reconResult.get(0);
			for(int clearedCounter =0; clearedCounter < clearedResult.length; clearedCounter ++)
			{
				
				TableItem tbrow = new TableItem(ledgerReconReport , SWT.NONE);
				Object[] ledgerrecorecord = (Object[]) clearedResult[clearedCounter];
				tbrow.setText(0, ledgerrecorecord[0].toString());
				tbrow.setText(1, ledgerrecorecord[1].toString());
				tbrow.setText(2, ledgerrecorecord[2].toString());
				tbrow.setText(3, ledgerrecorecord[4].toString());
				tbrow.setText(4, ledgerrecorecord[5].toString());
				
				if(narration==true)
				{
					try {
						tbrow.setText(5,ledgerrecorecord[6].toString() );
					} catch (Exception e) {
						// TODO Auto-generated catch block
						tbrow.setText(5,"");
					}
					tbrow.setText(6,ledgerrecorecord[7].toString() );
					try {
						tbrow.setText(7,ledgerrecorecord[8].toString() );
					} catch (Exception e) {
						// TODO Auto-generated catch block
						tbrow.setText(7,"" );
					}
		
					
					chkremove=new Button(ledgerReconReport, SWT.CHECK);
					chkremove.setData("vouchercode",ledgerrecorecord[3]);
					chkremove.setData("cdate",ledgerrecorecord[7].toString().substring(6) + "-" + ledgerrecorecord[7].toString().substring(3,5) + "-" + ledgerrecorecord[7].toString().substring(0,2) );
					chkremove.setData("accname",ledgerrecorecord[1]);
					chkremove.setData("rowindex",clearedCounter);
					chkremove.setVisible(true);
					TableEditor chkeditor= new TableEditor(ledgerReconReport);
					chkeditor.grabHorizontal=true;
					chkeditor.setEditor(chkremove,tbrow,8);
					chkbtnRemoves.add(chkremove);
				}
				else
					{
						tbrow.setText(5,ledgerrecorecord[7].toString() );
						
						try {
							tbrow.setText(6,ledgerrecorecord[8].toString() );
						} catch (Exception e) {
							// TODO Auto-generated catch block
							tbrow.setText(6,"");
						}
						
							chkremove=new Button(ledgerReconReport, SWT.CHECK);
							chkremove.setData("vouchercode",ledgerrecorecord[3]);
							chkremove.setData("cdate",ledgerrecorecord[7].toString().substring(6) + "-" + ledgerrecorecord[7].toString().substring(3,5) + "-" + ledgerrecorecord[7].toString().substring(0,2) );
							chkremove.setData("accname",ledgerrecorecord[1]);
							chkremove.setData("rowindex",clearedCounter);
							chkremove.setVisible(true);
							TableEditor chkeditor= new TableEditor(ledgerReconReport);
							chkeditor.grabHorizontal=true;
							chkeditor.setEditor(chkremove,tbrow,7);
							chkbtnRemoves.add(chkremove);
						
		
					}
			}
		
		
			Object[] unclearedResult=(Object[])reconResult.get(1);
			
			for(int unclearedCounter =0; unclearedCounter < unclearedResult.length-7; unclearedCounter ++)
			{
				
				TableItem tbrow = new TableItem(ledgerReconReport , SWT.NONE);
				Object[] ledgerrecorecord = (Object[]) unclearedResult[unclearedCounter];
				tbrow.setText(0, ledgerrecorecord[0].toString());
				tbrow.setText(1, ledgerrecorecord[1].toString());
				tbrow.setText(2, ledgerrecorecord[2].toString());
				tbrow.setText(3, ledgerrecorecord[4].toString());
				tbrow.setText(4, ledgerrecorecord[5].toString());
				
				if(narration==true)
				{
					
					try {
						tbrow.setText(5,ledgerrecorecord[6].toString() );
					} catch (Exception e) {
						// TODO Auto-generated catch block
						tbrow.setText(5,"" );
					}
					tbrow.setText(6,"" );
					tbrow.setText(7,"");
					}
					else
					{
						tbrow.setText(5,"" );
						tbrow.setText(6,"" );
						tbrow.setText(7,"" );
					}
				
				}
			}

				
		}

		
			
		
		

		

	
	
	public void setEvents()
	{
		ledgerReconReport.setFocus();
		ledgerReconReport.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				chkbtnRemoves.get(0).setFocus();
			}
		});
		
		for(int clearcounter=0; clearcounter < chkbtnRemoves.size(); clearcounter++)
		{
			chkbtnRemoves.get(clearcounter).addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < chkbtnRemoves.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < chkbtnRemoves.size())
						{	
							chkbtnRemoves.get(counter).setFocus();
						}
					}
					/*if(arg0.keyCode==SWT.ARROW_DOWN && counter==chkbtnRemoves.size())
					{
						btnRemove.setFocus();
					}*/
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0 && counter < chkbtnRemoves.size())
						{	
							chkbtnRemoves.get(counter).setFocus();
						}
					}
				}
			});
		}
		
		btnViewBankReco.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnRemove.setFocus();
				}
				
			}
		});
		
		
		btnRemove.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnViewBankReco.setFocus();
				}
				
			}
		});
		
		btnViewBankReco.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewBankReco.getParent().getParent().getParent();
				btnViewBankReco.getParent().getParent().dispose();
					
				viewReconciliation vl=new viewReconciliation(grandParent,SWT.NONE,false);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
		
		
		btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//call the deleteVoucher from the transactionController.
				boolean deleteFlag = false;
				for(int deleteCounter = 0; deleteCounter < chkbtnRemoves.size(); deleteCounter ++ )
				{
					if(chkbtnRemoves.get(deleteCounter).getSelection())
					{
						String AccountName=chkbtnRemoves.get(deleteCounter).getData("accname").toString();
						int VoucherCode=Integer.parseInt(chkbtnRemoves.get(deleteCounter).getData("vouchercode").toString());
						String ClearDate=chkbtnRemoves.get(deleteCounter).getData("cdate").toString();
						int cleardata=Integer.parseInt(chkbtnRemoves.get(deleteCounter).getData("rowindex").toString());
						if(reportController.deleteClearedRecon(AccountName, VoucherCode, ClearDate) )
						{
							deleteFlag = true; 
						}
					}
				}
				if(deleteFlag)
				{
					Composite grandParent = (Composite) btnRemove.getParent().getParent();
					

					gnukhata.controllers.reportController.getClearedUnclearedTransactions(grandParent, bankname, FromDate, ToDate, projectName, narration, clearflag);
					btnRemove.getParent().dispose();
												

				}
			}
		});	
		
		
		
		
	}	
	}


