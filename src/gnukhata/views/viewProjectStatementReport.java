package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class viewProjectStatementReport extends Composite {
	
	int counter = 0;
	static Display display;
	Table tblprojstmt;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn totaloutgoing;
	TableColumn totalincoming;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lbltotaloutgoing;
	Label lbltotalincoming;
	NumberFormat nf;
	String projname;
	Button btnViewpsForAccount;
	Button btnPrint;
	String strdate;
	
	Vector<Object> printProjectStmt = new Vector<Object>();
	ArrayList<Button> accounts=new ArrayList<Button>();
	static String endDateParam = ""; 
	ODPackage sheetstream;
	
	 public viewProjectStatementReport(Composite parent,String toDate, int style, Object[] result,String selectproject)
	{
		
		super(parent,style);
		// TODO Auto-generated constructor stub
		
		projname=selectproject;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(67);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		
		strdate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0, 4);
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3]);
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(75);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("---------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 18, SWT.ITALIC| SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"");
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,0);
		layout.left = new FormAttachment(40);
		lblOrgDetails.setLayoutData(layout);
		

		Label lblheadline=new Label(this, SWT.NONE);
		strdate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0, 4);
		endDateParam = toDate;
		lblheadline.setText("Statement for Project:"+projname+"\t\t\t\t\t\t\t\t\tPeriod "+"From  "+globals.session[2]+" To  "+strdate);
		lblheadline.setFont(new Font(display, "Times New Roman", 15, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,1);
		layout.left = new FormAttachment(6);
		lblheadline.setLayoutData(layout);
		
				
		tblprojstmt = new Table (this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblprojstmt.setLinesVisible (true);
		tblprojstmt.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		layout.bottom = new FormAttachment(91);
		tblprojstmt.setLayoutData(layout);
	
		btnViewpsForAccount =new Button(this,SWT.PUSH);
		btnViewpsForAccount.setText("&Back To Project Statement");
		btnViewpsForAccount.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblprojstmt,20);
		layout.left=new FormAttachment(25);
		btnViewpsForAccount.setLayoutData(layout);

		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblprojstmt,20);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);
		
		this.makeaccessible(tblprojstmt);
		this.getAccessible();
		
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		//int shellwidth = this.getClientArea().width;
		this.setReport(result);
		this.setEvents();
		try {
			sheetstream=ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/ProjectStatement.ots"), "ProjectStatement");
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	 
	private void setReport(Object[]result)
	{
		String[] columns;
		String[] orgname;
		orgname=new String[]{ globals.session[1].toString()};
		columns=new String[]{"Sr.No","Account Name","Group Name","Total Outgoing","Total Incoming"};
		
		lblsrno = new Label(tblprojstmt,SWT.BORDER|SWT.CENTER);
		lblsrno.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblsrno.setText("Sr. No.");
	
		lblaccname = new Label(tblprojstmt,SWT.BORDER|SWT.CENTER);
		lblaccname.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblaccname.setText("    Account Name    ");
		
		lblgrpname = new Label(tblprojstmt,SWT.BORDER|SWT.CENTER);
		lblgrpname.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblgrpname.setText("  Group Name  ");
		
		lbltotaloutgoing = new Label(tblprojstmt,SWT.BORDER|SWT.CENTER);
		lbltotaloutgoing.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lbltotaloutgoing.setText("    	 Total Outgoing     	 ");
		
		lbltotalincoming = new Label(tblprojstmt,SWT.BORDER|SWT.CENTER);
		lbltotalincoming.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lbltotalincoming.setText("    	Total Incoming     	 ");
		
		headerRow = new TableItem(tblprojstmt, SWT.BORDER|SWT.COLOR_DARK_RED);
		
		TableItem[] items = tblprojstmt.getItems();
		srno = new TableColumn(tblprojstmt,SWT.CENTER);
		accname = new TableColumn(tblprojstmt,SWT.CENTER);
		grpname = new TableColumn(tblprojstmt,SWT.CENTER);
		totaloutgoing = new TableColumn(tblprojstmt, SWT.RIGHT);
		totalincoming = new TableColumn(tblprojstmt, SWT.RIGHT);
		
		TableEditor editorsrno = new TableEditor(tblprojstmt);
		editorsrno.grabHorizontal = true;
		editorsrno.setEditor(lblsrno,items[0],0);
		
		TableEditor editorAccName = new TableEditor(tblprojstmt);
		editorAccName.grabHorizontal = true;
		editorAccName.setEditor(lblaccname,items[0],1);
		
		TableEditor editorgrpName = new TableEditor(tblprojstmt);
		editorgrpName.grabHorizontal = true;
		editorgrpName.setEditor(lblgrpname,items[0],2);
		
		TableEditor editortotaloutgoing = new TableEditor(tblprojstmt);
		editortotaloutgoing.grabHorizontal = true;
		editortotaloutgoing.setEditor(lbltotaloutgoing,items[0],3);
		
		TableEditor editortotalincoming = new TableEditor(tblprojstmt);
		editortotalincoming.grabHorizontal = true;
		editortotalincoming.setEditor(lbltotalincoming,items[0],4);

		
		
		final int tblwidth = tblprojstmt.getClientArea().width;
		tblprojstmt.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				srno.setWidth(5 * tblwidth / 100);
				accname.setWidth(30 * tblwidth / 100);
				grpname.setWidth(20 * tblwidth /100);
				totaloutgoing.setWidth(20 * tblwidth / 100);
				totalincoming.setWidth(20 * tblwidth / 100);
				//event.width = 150;
				event.height = 16;
			}
		});
		
        		
		srno.pack();
		accname.pack();
		grpname.pack();
		totaloutgoing.pack();
		totalincoming.pack();
		
		btnPrint.setData("printorgname",orgname);
		btnPrint.setData("printcolumns",columns);
		
		for(int tbcounter = 0; tbcounter< result.length; tbcounter ++)
		{
			TableItem tbrow = new TableItem(tblprojstmt , SWT.NONE);
			Object[] ProRecord = (Object[]) result[tbcounter];
			Object[] tbrecord = (Object[]) result[tbcounter];
			

			if(tbcounter < result.length-1)
			{
			tbrow.setText(0, tbrecord[0].toString());
			
			Button btnAccount = new Button(tblprojstmt , SWT.FLAT);
			TableEditor btnedit = new TableEditor(tblprojstmt);
			btnAccount.setText(tbrecord[1].toString());
			btnedit.grabHorizontal = true;
			btnedit.setEditor(btnAccount,tbrow,1 );
			accounts.add(btnAccount);
		
			//tbrow.setText(1, tbrecord[1].toString());
			tbrow.setText(2, tbrecord[2].toString());
			tbrow.setText(3, tbrecord[3].toString());
			tbrow.setText(4, tbrecord[4].toString());
			Object[] printableRow = new Object[]{ProRecord[0], ProRecord[1],ProRecord[2],ProRecord[3], ProRecord[4]};
			printProjectStmt.add(printableRow);
			}
			else
			{
				
				tbrow.setText(0,"");
				tbrow.setText(1,"");
				tbrow.setText(2," Total ");
				tbrow.setFont(new Font(display, "Times New Roman",12, SWT.BOLD));
				tbrow.setText(3, tbrecord[3].toString());
				tbrow.setText(4, tbrecord[4].toString());
				Object[] printableRow = new Object[]{"","","Total",ProRecord[3],ProRecord[4]};
				printProjectStmt.add(printableRow);
				
				
			}
		}
		tblprojstmt.pack();

	}
	
	
	private void setEvents()
	{
		btnViewpsForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
			}
		});
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewpsForAccount.setFocus();
				}
			}
		});
		
		btnViewpsForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewpsForAccount.getParent().getParent();
				btnViewpsForAccount.getParent().dispose();
					
					ViewProjectStatement vl=new ViewProjectStatement(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
		
		
		tblprojstmt.setFocus();
		tblprojstmt.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(accounts.isEmpty())
				{
					btnViewpsForAccount.setFocus();
				}
				else
				{
				accounts.get(0).setFocus();
				}
				
			}
		} );
		for(int accountcounter = 0; accountcounter < accounts.size(); accountcounter ++ )
		{
			accounts.get(accountcounter).addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					Button btncurrent = (Button) arg0.widget;
					String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
					

					Composite grandParent = (Composite) btncurrent.getParent().getParent().getParent();
					String accName = btncurrent.getText();
					MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage(endDateParam);
					//msg.open();
					
					
					reportController.showLedger(grandParent, accName, fromdate, endDateParam, projname, true, false, true, "", projname);
					btncurrent.getParent().getParent().dispose();
					
					
				}
			});
			accounts.get(accountcounter).addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < accounts.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < accounts.size())
						{
							
						accounts.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0&& counter < accounts.size())
						{
							
							accounts.get(counter).setFocus();
						}
					}
				
					
				}
			});
			btnPrint.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					String[] strPrintCol = new String[]{"","","","",""};
					Object[][] finaldata=new Object[printProjectStmt.size() +1][strPrintCol.length];
					Object[] firstRow = new Object[]{"Sr.No","Account Name","Group Name","Total Outgoing","Total Incoming" };
					finaldata[0] = firstRow;

					
					
					for(int counter=0; counter < printProjectStmt.size(); counter++)
					{
						Object[] printrow=(Object[])printProjectStmt.get(counter);
				//		orgdata[counter]=printrow;
						finaldata[counter +1]=printrow;
					}	
					//printLedgerData.copyInto(finalData);
					
					
					TableModel model = new DefaultTableModel(finaldata,strPrintCol);
					try 
					{
						final File ProjectStatement = new File("/tmp/gnukhata/Report_Output/ProjectStatement" );
						//final File ProjectstmtTemplate =  new File("Report_Templates/ProjectStatement.ots");
						final Sheet ProjectstmtReportSheet = sheetstream.getSpreadSheet().getFirstSheet();
						ProjectstmtReportSheet.ensureRowCount(100000);
						ProjectstmtReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
						ProjectstmtReportSheet.getCellAt(0, 1).setValue("Statement for Project:"+projname+"\t\t\t\t\t\t\t\t\tPeriod "+"From  "+globals.session[2]+" To  "+strdate);
						for(int rowcounter = 0; rowcounter < printProjectStmt.size(); rowcounter ++ )
						{
							Object[] printRow = (Object[]) printProjectStmt.get(rowcounter);
							ProjectstmtReportSheet.getCellAt(0,rowcounter +3).setValue(printRow[0].toString());
							ProjectstmtReportSheet.getCellAt(1,rowcounter +3).setValue(printRow[1].toString());
							ProjectstmtReportSheet.getCellAt(2,rowcounter +3).setValue(printRow[2].toString());
							ProjectstmtReportSheet.getCellAt(3,rowcounter +3).setValue(printRow[3].toString());
							ProjectstmtReportSheet.getCellAt(4,rowcounter +3).setValue(printRow[4].toString());
						}
						OOUtils.open(ProjectstmtReportSheet.getSpreadSheet().saveAs(ProjectStatement));
						//OOUtils.open(AccountReport);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});				
		
		}
		
	}

	 public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
}