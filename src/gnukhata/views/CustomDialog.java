package gnukhata.views;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 */

/**
 * @author krmane
 *
 */
public class CustomDialog extends Dialog {
	int value;
	Label lblMessage;
	Shell parent;
	final Shell shell;

	/**
	 * @param arg0
	 */
	public CustomDialog(Shell dialogParent) {
		super(dialogParent  );
		// TODO Auto-generated constructor stub
		parent = getParent();
		shell = new Shell(parent,SWT.TITLE| SWT.APPLICATION_MODAL | SWT.ICON_QUESTION);
		shell.setText("confirm");
		
		
		shell.setLayout(new GridLayout(2,true));
		GridData gd = new GridData();
		lblMessage  = new Label(shell , SWT.BORDER | SWT.CENTER | SWT.FULL_SELECTION);
		lblMessage.setText("Do you wish to save");
		lblMessage.setFont(new Font(parent.getDisplay(),"Times New Roman", 16, SWT.BOLD ));
		gd.widthHint = 250;
		gd.horizontalSpan = 2;
		lblMessage.setLayoutData(gd);
		Label lblBlan = new Label(shell,SWT.NONE );
		gd = new GridData();
		gd.horizontalSpan = 2;
		lblBlan.setLayoutData(gd);
		Button btnYes = new Button(shell,SWT.PUSH);
		btnYes.setText("&Yes");
		gd = new GridData();
		
		gd.widthHint = 125;
		btnYes.setFocus();
		btnYes.setLayoutData(gd);
		Button btnNo = new Button(shell,SWT.PUSH);
		btnNo.setText("&No");
		gd = new GridData();
		gd.widthHint= 125;
		btnNo.setLayoutData(gd);
		
		Label lblblank2 = new Label(shell, SWT.NONE);
		
		btnYes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				value = SWT.YES;
				shell.dispose();
			}
		});
		btnNo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				value= SWT.NO;
				shell.dispose();
			}
		} );
		//shell.setSize(350, 350);
		
		shell.pack();
	    shell.open();
	    
	 
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public CustomDialog(Shell dialogParent, int style) {
		super(dialogParent , style);
		// TODO Auto-generated constructor stub
		parent = getParent();
		shell = new Shell(parent,SWT.TITLE | SWT.APPLICATION_MODAL | SWT.ICON_QUESTION);
		shell.setText("confirm");
		
		
		shell.setLayout(new GridLayout(2,true));
		GridData gd = new GridData();
		lblMessage  = new Label(shell , SWT.BORDER | SWT.CENTER | SWT.FULL_SELECTION);
		lblMessage.setText("Do you wish to save");
		lblMessage.setFont(new Font(parent.getDisplay(),"Times New Roman", 16, SWT.BOLD ));
		gd.widthHint = 250;
		gd.horizontalSpan = 2;
		lblMessage.setLayoutData(gd);
		Label lblBlan = new Label(shell,SWT.NONE );
		gd = new GridData();
		gd.horizontalSpan = 2;
		lblBlan.setLayoutData(gd);
		Button btnYes = new Button(shell,SWT.PUSH);
		btnYes.setText("&Yes");
		gd = new GridData();
		
		gd.widthHint = 125;
		btnYes.setFocus();
		btnYes.setLayoutData(gd);
		Button btnNo = new Button(shell,SWT.PUSH);
		btnNo.setText("&No");
		gd = new GridData();
		gd.widthHint= 125;
		btnNo.setLayoutData(gd);
		
		Label lblblank2 = new Label(shell, SWT.NONE);
		
		btnYes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				value = SWT.YES;
				shell.dispose();
			}
		});
		btnNo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				value= SWT.NO;
				shell.dispose();
			}
		} );
		//shell.setSize(350, 350);
		
		shell.pack();
	    shell.open();
	    
	 
	}
	public void SetMessage(String Message)
	{
		lblMessage.setText(Message);
		
	}
	public int open()
	{
		
	    Display display = parent.getDisplay();
	    while (!shell.isDisposed()) {
	      if (!display.readAndDispatch())
	        display.sleep();
	    }
	 
	    return value;
	 
		
		
	}

	/**
	 * @param args
	 */
	
	/*public static void main(String[] args) {
		CustomDialog cd = new CustomDialog(new Shell(),SWT.ICON_QUESTION );
		cd.SetMessage("do you wisht to edit");
		int answer = cd.open();
		if(answer == SWT.YES)
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK );
			msg.setMessage("you clicked yes");
			msg.open();
		}
		else
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK );
			msg.setMessage("you clicked no");
			msg.open();
		}
		
		
		
		// TODO Auto-generated method stub

	}
*/
}
