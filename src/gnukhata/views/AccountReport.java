package gnukhata.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.TabExpander;

import gnukhata.globals;
import gnukhata.controllers.reportController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.Style;
import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class AccountReport extends Composite {
	
	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	
	String strFromDate;
	String strToDate;
	
	Label lblLogo;
	Label lblOrgDetails;
	Label lblLine;
	Label lblheadline;
	Label lblListOfAccount;
	
	Label lblSrNo;
	Label lblAccountName;
	Label lblGroupname;
	Label lblSubGroupName;
	
	Button btnPrint;
	
	Table AccountList;
	TableItem tableheader;
	TableColumn srno;
	TableColumn accountname;
	TableColumn groupname;
	TableColumn subgroupname;
	
	static Display display;
	
	Vector<Object>printAccountReport=new Vector<Object>();
	ODPackage  sheetStream;

	public AccountReport(Composite Parent, int Style, Object[] result) {
		// TODO Auto-generated constructor stub
		super(Parent,Style);
		
		/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("parent is:"+getParent());
		msg.open();*/
		
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    /*strFromDate=frmDate.substring(8)+"-"+frmDate.substring(5,7)+"-"+frmDate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);*/
	  
	    lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(60);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		
		lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(40);
		lblheadline.setLayoutData(layout);
		
		lblListOfAccount=new Label(this, SWT.NONE);
		lblListOfAccount.setText("List Of Accounts");
		lblListOfAccount.setFont(new Font(display, "Times New Roman", 14, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(40);
		lblListOfAccount.setLayoutData(layout);
		
		AccountList=new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
		AccountList.setLinesVisible (true);
		AccountList.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblListOfAccount, 10);
		layout.left=new FormAttachment(5);
		layout.right=new FormAttachment(80);
		layout.bottom = new FormAttachment(80);
		AccountList.setLayoutData(layout);
		
		btnPrint=new Button(this, SWT.PUSH);
		btnPrint.setText("&Print");
		btnPrint.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(AccountList, 10);
		layout.left=new FormAttachment(42);
		btnPrint.setLayoutData(layout);
		
		
		this.makeaccssible(AccountList); 
		this.getAccessible();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		int shellwidth = this.getClientArea().width;
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/AccountReport.ots"),"AccountReport");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.setReport(result);
		this.setEvent();
	}
	
	public void makeaccssible(Control c)
	{
		c.getAccessible();
	}
	
	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	
	private void setReport(Object[] result)
	{
		String[] columns;
		String[] orgname;
		orgname=new String[]{ globals.session[1].toString()};
		columns=new String[]{"Sr.No","Account Name","Group Name","Sub-Group Name"};
		//columns=new String[]{"Account Name","Group Name","Sub-Group Name"};
		
		lblSrNo=new Label(AccountList, SWT.BORDER|SWT.CENTER);
		lblSrNo.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblSrNo.setText("Sr. No.");
		
		
		lblAccountName=new Label(AccountList, SWT.BORDER|SWT.CENTER);
		lblAccountName.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblAccountName.setText("Account Name");
		
		lblGroupname=new Label(AccountList, SWT.BORDER|SWT.CENTER);
		lblGroupname.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblGroupname.setText("Group Name");
		
		lblSubGroupName=new Label(AccountList, SWT.BORDER|SWT.CENTER);
		lblSubGroupName.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblSubGroupName.setText("Sub-Group Name");
		
		final int tablewidth=AccountList.getClientArea().width;
		AccountList.addListener(SWT.MeasureItem, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				// TODO Auto-generated method stub
				System.out.println("event fired");
				srno.setWidth(8 * tablewidth / 100);
				accountname.setWidth(40 * tablewidth / 100);
				groupname.setWidth(20 * tablewidth / 100);
				subgroupname.setWidth(30 * tablewidth /100);
				arg0.height = 16;
				
			}
		});
		
		tableheader=new TableItem(AccountList, SWT.BORDER|SWT.COLOR_DARK_RED);
		
		TableItem[] record=AccountList.getItems();
		srno=new TableColumn(AccountList, SWT.CENTER);
		accountname=new TableColumn(AccountList, SWT.LEFT);
		groupname=new TableColumn(AccountList, SWT.LEFT);
		subgroupname=new TableColumn(AccountList, SWT.LEFT);
		
		TableEditor editorsrno=new TableEditor(AccountList);
		editorsrno.grabHorizontal=true;
		editorsrno.setEditor(lblSrNo,record[0],0);
		
		TableEditor editoraccountname=new TableEditor(AccountList);
		editoraccountname.grabHorizontal=true;
		editoraccountname.setEditor(lblAccountName,record[0],1);
		
		TableEditor editorgroupname=new TableEditor(AccountList);
		editorgroupname.grabHorizontal=true;
		editorgroupname.setEditor(lblGroupname,record[0],2);
		
		TableEditor editorsubgroupname=new TableEditor(AccountList);
		editorsubgroupname.grabHorizontal=true;
		editorsubgroupname.setEditor(lblSubGroupName,record[0],3);
		
		srno.pack();
		accountname.pack();
		groupname.pack();
		subgroupname.pack();
		
		btnPrint.setData("printorgname",orgname);
		btnPrint.setData("printcolumns",columns);
		
		for (int tbcounter = 0; tbcounter < result.length; tbcounter++)
		{
			TableItem tbrow=new TableItem(AccountList,SWT.NONE);
			Object[] AccountRecord = (Object[]) result[tbcounter];
			
				tbrow.setText(0, AccountRecord[0].toString());
				tbrow.setText(1, AccountRecord[1].toString());
				tbrow.setText(2, AccountRecord[2].toString());
				tbrow.setText(3, AccountRecord[3].toString());
				
				Object[] printableRow = new Object[]{AccountRecord[0], AccountRecord[1],AccountRecord[2],AccountRecord[3]};
				printAccountReport.add(printableRow);
			
		}
		//AccountList.pack();
	}
	
	private void setEvent()
	{
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//String[] strOrgName=(String[])btnPrint.getData("orgname");
				//String[] strPrintCol=(String[])btnPrint.getData("printcolumns");
				String[] strPrintCol = new String[]{"","","",""};
				
		//		Object[][] orgdata=new Object[printAccountReport.size()][strOrgName.length];
				Object[][] finaldata=new Object[printAccountReport.size() +1][strPrintCol.length];
				Object[] firstRow = new Object[]{"Sr.No.", "Account Name","Group Name","Sub-Group Name"  };
				finaldata[0] = firstRow;
				
				for(int counter=0; counter < printAccountReport.size(); counter++)
				{
					Object[] printrow=(Object[])printAccountReport.get(counter);
			//		orgdata[counter]=printrow;
					finaldata[counter +1]=printrow;
				}
				
				TableModel model = new DefaultTableModel(finaldata,strPrintCol);
				try 
				{	
					/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("print");
					msg.open();*/
					final File AccountReport = new File("/tmp/gnukhata/Report_Output/AccountReport" );
					
					final Sheet accountReportSheet =  sheetStream.getSpreadSheet().getFirstSheet();
					accountReportSheet.ensureRowCount(100000);
					accountReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
					accountReportSheet.getCellAt(0, 1).setValue("List Of Accounts");
					for(int rowcounter = 0; rowcounter < printAccountReport.size(); rowcounter ++ )
					{
						Object[] printRow = (Object[]) printAccountReport.get(rowcounter);
						accountReportSheet.getCellAt(0,rowcounter +3).setValue(printRow[0].toString());
						accountReportSheet.getCellAt(1,rowcounter +3).setValue(printRow[1].toString());
						accountReportSheet.getCellAt(2,rowcounter +3).setValue(printRow[2].toString());
						accountReportSheet.getCellAt(3,rowcounter +3).setValue(printRow[3].toString());
						
					}
					//SpreadSheet.createEmpty(model).saveAs(AccountReport);
					
					//final Sheet AccountReportsheet = SpreadSheet.createFromFile(AccountReport).getSheet(0);
					//AccountReportsheet.getColumn(1).setWidth(new Integer(60));
					//AccountReportsheet.getColumn(2).setWidth(new Integer(40));
					//AccountReportsheet.getColumn(3).setWidth(new Integer(60));
					//AccountReportsheet.duplicateRows(0, 1,2 );
					//AccountReportsheet.getCellAt(0, 0).merge(4, 0);
					//AccountReportsheet.getCellAt(0,0).setStyleName("Align Right");
					//AccountReportsheet.setValueAt(globals.session[1],0 ,0 );
					//AccountReportsheet.insertDuplicatedRows(0, 0);
					OOUtils.open(accountReportSheet.getSpreadSheet().saveAs(AccountReport));
					//OOUtils.open(AccountReport);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	

}
