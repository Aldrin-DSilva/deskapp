package gnukhata.views;

import gnukhata.globals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class ViewBalanceSheetReport extends Composite {
	
	Table tbLiaBalanceSheet;
	Table tbAssetBalancesheet;
	
	TableItem headerRow;
	TableItem AssestHeaderrow;
	
	TableColumn lia_accname;
	TableColumn assets_accname;
	TableColumn asset_tolamt;
	TableColumn lia_tolamt;
	TableColumn lia_amt;
	TableColumn asset_amt;
	
	Label lbllia_accname;
	Label lblassets_accname;
	Label lblasset_tolamt;
	Label lbllia_tolamt;
	Label lbllia_amt;
	Label lblasset_amt;
	Button btnBacktoBalanceSheet;
	Button btnPrint;
	
	TableEditor lia_accnameEditor;
	TableEditor lia_tolamtEditor;
	TableEditor assets_accnameEditor;
	TableEditor asset_tolamtEditor;
	TableEditor lia_amtEditor;
	TableEditor asset_amtEditor;
	
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = ""; 
	NumberFormat nf;
	static Display display;
	String strOrgType;
	Vector<Object> printconventinal = new Vector<Object>();
	
	public ViewBalanceSheetReport(Composite parent, int style,String endDate, Object[] result, Object[] profitloss) {
		// TODO Auto-generated constructor stub
	
		super(parent,style);
		
		strOrgType = globals.session[4].toString();
		
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-----------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		Label lblorgname = new Label(this,SWT.NONE);
		lblorgname.setText(globals.session[1].toString());
		lblorgname.setFont(new Font(display, "Times New Roman", 14, SWT.ITALIC | SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,2);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(14);
		lblorgname.setLayoutData(layout);
				
		Label lblheadline=new Label(this, SWT.NONE);
		String strdate="";
		strdate=endDate.substring(8)+" - "+endDate.substring(5,7)+" - "+endDate.substring(0, 4);
		endDateParam = endDate;
		lblheadline.setText("Balance Sheet as at "+strdate);
		lblheadline.setFont(new Font(display, "Times New Roman", 14, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblorgname,2);
		layout.left = new FormAttachment(35);
		lblheadline.setLayoutData(layout);
			
		tbLiaBalanceSheet= new Table(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tbLiaBalanceSheet.setLinesVisible (true);
		tbLiaBalanceSheet.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(50);
		layout.bottom = new FormAttachment(91);
		tbLiaBalanceSheet.setLayoutData(layout);
		

		tbAssetBalancesheet= new Table(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tbAssetBalancesheet.setLinesVisible (true);
		tbAssetBalancesheet.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,10);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(91);
		tbAssetBalancesheet.setLayoutData(layout);
		
		btnBacktoBalanceSheet =new Button(this,SWT.PUSH);
		btnBacktoBalanceSheet.setText("&Back To Balance Sheet");
		btnBacktoBalanceSheet.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbAssetBalancesheet,20);
		layout.left=new FormAttachment(25);
		btnBacktoBalanceSheet.setLayoutData(layout);
		btnBacktoBalanceSheet.setFocus();
			

		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbAssetBalancesheet,20);
		layout.left=new FormAttachment(50);
		btnPrint.setLayoutData(layout);
		
		
		
		this.makeaccessible(tbLiaBalanceSheet);
		this.getAccessible();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		//int shellwidth = this.getClientArea().width;
		/*MessageBox m = new MessageBox(new Shell(),SWT.OK);
		m.setMessage(Integer.toString(shellwidth));
		m.open();*/

		this.setReport(result,profitloss);
		//this.setEvents();
		
	}
	private void setReport(Object[] result,Object[] profitloss)
	{
		String[] columns;
		String[] orgname;
		orgname=new String[]{ globals.session[1].toString()};
		
		
		lbllia_accname = new Label(tbLiaBalanceSheet,SWT.BORDER|SWT.CENTER);
		lbllia_accname.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		if(strOrgType == "profit making")
		{
			lbllia_accname.setText("CAPITAL AND LIABILITIES");
			columns=new String[]{"CAPITAL AND LIABILITIES","Amount","Amount","PROPERTY AND ASSETS","Amount","Amount"};
		}
		else
		{
			lbllia_accname.setText("CORPUS AND LIABILITIES");
			columns=new String[]{"CORPUS AND LIABILITIES","Amount","Amount","PROPERTY AND ASSETS","Amount","Amount"};
		}
		
		lbllia_amt = new Label(tbLiaBalanceSheet, SWT.BORDER|SWT.CENTER);
		lbllia_amt.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		lbllia_amt.setText("AMOUNT");
		
		lbllia_tolamt = new Label(tbLiaBalanceSheet,SWT.BORDER|SWT.CENTER);
		lbllia_tolamt.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		lbllia_tolamt.setText("AMOUNT");
		
		lblassets_accname = new Label(tbAssetBalancesheet, SWT.BORDER|SWT.CENTER);
		lblassets_accname.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		lblassets_accname.setText("PROPERTY AND ASSETS");
		
		lblasset_amt = new Label(tbAssetBalancesheet,SWT.BORDER|SWT.CENTER);
		lblasset_amt.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		lblasset_amt.setText("AMOUNT");
		
		lblasset_tolamt = new Label(tbAssetBalancesheet, SWT.BORDER|SWT.CENTER);
		lblasset_tolamt.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		lblasset_tolamt.setText("AMOUNT");
		
		final int tblassetbal = tbAssetBalancesheet.getClientArea().width;
		final int tblliabal = tbLiaBalanceSheet.getClientArea().width;
		tbLiaBalanceSheet.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub

				lia_accname.setWidth(40*tblliabal/100);
				lia_amt.setWidth(26*tblliabal/100);
				lia_tolamt.setWidth(26*tblliabal/100);
				event.height = 14;
			}
		});
		
		tbAssetBalancesheet.addListener(SWT.MeasureItem, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				assets_accname.setWidth(40*tblassetbal/100);
				asset_amt.setWidth(25*tblassetbal/100);
				asset_tolamt.setWidth(25*tblassetbal/100);
				event.height = 14;
			}
		});
		
		btnBacktoBalanceSheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnBacktoBalanceSheet.getParent().getParent();
				btnBacktoBalanceSheet.getParent().dispose();
					
				viewBalanceSheet bs = new viewBalanceSheet(grandParent,SWT.NONE);
				bs.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
		
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				String[] orgname=(String[])btnPrint.getData("printorgname");
				String[] columns = (String[]) btnPrint.getData("printcolumns");
				Object[][] orgdata =new Object[printconventinal.size()][orgname.length ];
				Object[][] finalData =new Object[printconventinal.size()][columns.length];
				for(int counter = 0; counter < printconventinal.size(); counter ++ )
				{
					Object[] printRow = (Object[]) printconventinal.get(counter);												
					finalData[counter] = printRow;
				}
					
				//printLedgerData.copyInto(finalData);
				
					TableModel model = new DefaultTableModel(finalData,columns);
					/*try {
						final File conventionalbs = new File("ConventionalBalanceSheet.ods" );
						final File conventionaltemplate = new File("Report_Templates/ConventionalBalSheet.ots");
						//SpreadSheet.createEmpty(model).saveAs(conventionalbs);
						final Sheet conventionalSheet = SpreadSheet.createFromFile(conventionaltemplate).getSheet(0);
						ledgerSheet.getColumn(1).setWidth(new Integer(40));
						ledgerSheet.getColumn(2).setWidth(new Integer(40));
						conventionalSheet.ensureRowCount(100000);
						conventionalSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
						conventionalSheet.getCellAt(0, 1).setValue("Balance Sheet As At "+ globals.session[3].toString());
						for (int rowcounter=0; rowcounter<printconventinal.size(); rowcounter++)
						{
							Object[] printrow = (Object[]) printconventinal.get(rowcounter);
							conventionalSheet.getCellAt(0, rowcounter+3).setValue(printrow[0].toString());
							conventionalSheet.getCellAt(1, rowcounter+3).setValue(printrow[1].toString());
							conventionalSheet.getCellAt(2, rowcounter+3).setValue(printrow[2].toString());
							conventionalSheet.getCellAt(3, rowcounter+3).setValue(printrow[3].toString());
							conventionalSheet.getCellAt(4, rowcounter+3).setValue(printrow[4].toString());
							conventionalSheet.getCellAt(5, rowcounter+3).setValue(printrow[5].toString());
						}
						OOUtils.open(conventionalSheet.getSpreadSheet().saveAs(conventionalbs));

						//OOUtils.open(conventionalbs);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	*/		}
		});
	

		
		headerRow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		TableItem[] items = tbLiaBalanceSheet.getItems();
		lia_accname = new TableColumn(tbLiaBalanceSheet, SWT.BORDER);
		lia_amt = new TableColumn(tbLiaBalanceSheet, SWT.RIGHT);
		lia_tolamt = new TableColumn(tbLiaBalanceSheet, SWT.RIGHT);
		
		AssestHeaderrow = new TableItem(tbAssetBalancesheet, SWT.NONE);
		TableItem[] items1 = tbAssetBalancesheet.getItems();
		assets_accname = new TableColumn(tbAssetBalancesheet,SWT.BORDER);
		asset_amt = new TableColumn(tbAssetBalancesheet, SWT.RIGHT);
		asset_tolamt = new TableColumn(tbAssetBalancesheet, SWT.RIGHT);
		
		lia_accnameEditor = new TableEditor(tbLiaBalanceSheet);
		lia_accnameEditor.grabHorizontal = true;
		lia_accnameEditor.setEditor(lbllia_accname,items[0],0);
		
		lia_amtEditor = new TableEditor(tbLiaBalanceSheet);
		lia_amtEditor.grabHorizontal = true;
		lia_amtEditor.setEditor(lbllia_amt,items[0],1);
		
		lia_tolamtEditor = new TableEditor(tbLiaBalanceSheet);
		lia_tolamtEditor.grabHorizontal = true;
		lia_tolamtEditor.setEditor(lbllia_tolamt,items[0],2);
		
		assets_accnameEditor = new TableEditor(tbAssetBalancesheet);
		assets_accnameEditor.grabHorizontal = true;
		assets_accnameEditor.setEditor(lblassets_accname,items1[0],0);
		
		asset_amtEditor = new TableEditor(tbAssetBalancesheet);
		asset_amtEditor.grabHorizontal = true;
		asset_amtEditor.setEditor(lblasset_amt,items1[0],1);
		
		asset_tolamtEditor = new TableEditor(tbAssetBalancesheet);
		asset_tolamtEditor.grabHorizontal = true;
		asset_tolamtEditor.setEditor(lblasset_tolamt,items1[0],2);
				
		lia_accname.pack();
		lia_amt.pack();
		lia_tolamt.pack();
		assets_accname.pack();
		asset_amt.pack();
		asset_tolamt.pack();
		
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		String rowFlag = "";
		Integer rows=0;
		Double pnlDr = 0.00;
		
		Integer netflag = profitloss.length - 4;
		Integer netTotalIndex = profitloss.length - 3;
		//Integer netTotal = profitloss.length - 3;
		Integer ballength=result.length - 13; 
		Integer tol_capital = result.length - 4;
		Integer tol_reserves = result.length - 3;
		Integer tol_loanlia = result.length - 5;
		Integer tol_currlia = result.length - 6;
		Integer tol_fixesAsset = result.length -8;
		Integer tol_miscellaneous = result.length - 7;
		Integer tol_investment = result.length - 11;
		Integer tol_loansasset = result.length - 10;
		Integer tol_currentasset = result.length -9;
		Integer assSrNo = Integer.parseInt(result[result.length-13].toString());
		Integer liaSrNo = Integer.parseInt(result[result.length-12].toString());
		Integer lialength = result.length - 1;
		Integer asslength = result.length - 2;
		Double TotalDr =Double.parseDouble(result[lialength].toString());
		Double TotalCr = Double.parseDouble(result[asslength].toString());
		Double netTotal = Double.parseDouble(profitloss[netTotalIndex].toString());
		pnlDr = netTotal + TotalDr;
		Double pnlCr = Double.parseDouble(profitloss[netTotalIndex].toString()) + TotalCr;
		Double difamount = 0.00;
		Double balancingTotal = 0.00;
		
		btnPrint.setData("printorgname",orgname);
		btnPrint.setData("printcolumns",columns);
		if(profitloss[netflag].equals("netProfit"))
		{
			if(TotalDr > pnlCr)
			{
				difamount = TotalDr - pnlCr;
			}
			else
			{
				difamount = pnlCr - TotalDr;
			}
			
		}
		else
		{
			if(TotalCr>pnlDr)
			{
				difamount = TotalCr - pnlDr;
			}
			else
			{
				difamount = pnlDr - TotalCr;
			}

		}
		
		if (assSrNo > liaSrNo)
		{
			rowFlag = "liabilities";
			rows = assSrNo - liaSrNo;
		}
		
		
		if(assSrNo < liaSrNo)
		{
			rowFlag = "asset";
			rows = liaSrNo - assSrNo;
		}

		
		int grpcode1=0;
		int grpcode12=0;
		int grpcode11=0;
		int grpcode3=0;
		int grpcode6=0;
		int grpcode2=0;
		int grpcode10=0;
		int grpcode9=0;
		int grpcode13=0;
		
		// For loop to get the length of the accounts of the respective groups from the result 
		for(int cnt =0; cnt < ballength; cnt++)
		{
			Object[] len = (Object[]) result[cnt];
			if(len[1].equals(1))
			{
				 grpcode1++;
			}
			if(len[1].equals(12))
			{
				 grpcode12++;
			}
			if(len[1].equals(11))
			{
				 grpcode11++;
			}
			if(len[1].equals(3))
			{
				 grpcode3++;
			}
			if(len[1].equals(6))
			{
				grpcode6++;
			
			}
			if(len[1].equals(9))
			{
				grpcode9++;
			
			}if(len[1].equals(2))
			{
				grpcode2++;
			
			}
			if(len[1].equals(10))
			{
				grpcode10++;
			
			}
			if(len[1].equals(13))
			{
				grpcode13++;
			
			}
		}
		
		grpcode3 = grpcode1 + grpcode3;
		grpcode11 = grpcode3 + grpcode11;
		grpcode12 = grpcode11 + grpcode12;
		grpcode6 = grpcode12 + grpcode6;
		grpcode2 = grpcode6 +  grpcode2;
		grpcode9 = grpcode2 + grpcode9;
		grpcode10 = grpcode10 + grpcode9;
		grpcode13 = grpcode9 + grpcode13;
		
		/* Code to display the accounts of group Capital,Reserves,Loans,Current Liabilities in the
		Liabilities side of the Balance Sheet */
		TableItem balrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		if(strOrgType.equals("ngo"))
		{
			balrow.setText(0, "CORPUS");
			balrow.setText(2, result[tol_capital].toString());
			Object[] printableRow = new Object[]{"CORPUS","",result[tol_capital]};
			printconventinal.add(printableRow);
		}
		if(strOrgType.equals("profit making"))
		{
			balrow.setText(0, "CAPITAL");
			balrow.setText(2, result[tol_capital].toString());
			Object[] printableRow = new Object[]{"CAPITAL","",result[tol_capital]};
			printconventinal.add(printableRow);
		}
		
		for(int rowcounter =0; rowcounter < grpcode1; rowcounter ++)
		{
			TableItem balrow1 = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			Object[] baldata = (Object[]) result[rowcounter];
			
			if(baldata[1].equals(1))
			{
				balrow1.setText(0, "            " + baldata[2].toString());
				balrow1.setText(1, "            " + baldata[3].toString());
				Object[] printableRow = new Object[]{baldata[2],baldata[3]};
				printconventinal.add(printableRow);
			}
			
		}
		
		TableItem reserve_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		reserve_row.setText(0,"RESERVES");
		reserve_row.setText(2,result[tol_reserves].toString());
		Object[] printableRow = new Object[]{"RESERVES","",result[tol_reserves]};
		printconventinal.add(printableRow);
	
		for(int rowcounter =grpcode11; rowcounter < grpcode12; rowcounter ++)
		{
			TableItem reserve1_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			Object[] baldata2 = (Object[]) result[rowcounter];
			
			if(baldata2[1].equals(12))
			{
				reserve1_row.setText(0, "            " + baldata2[2].toString());
				reserve1_row.setText(1, "            " + baldata2[3].toString());
				printableRow = new Object[]{baldata2[2],baldata2[3]};
				printconventinal.add(printableRow);
			}
		}


		TableItem loan_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		loan_row.setText(0,"LOANS");
		loan_row.setText(2,result[tol_loanlia].toString());
		printableRow = new Object[]{"LOANS","",result[tol_loanlia]};
		printconventinal.add(printableRow);
		for(int rowcounter =grpcode3; rowcounter < grpcode11; rowcounter ++)
		{
			TableItem loan1_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			Object[] baldata3 = (Object[]) result[rowcounter];
			
			if(baldata3[1].equals(11))
			{
				loan1_row.setText(0, "            " + baldata3[2].toString());
				loan1_row.setText(1, "            " + baldata3[3].toString());
				printableRow = new Object[]{baldata3[2],baldata3[3]};
				printconventinal.add(printableRow);

			}
		}
		
		TableItem lia_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		lia_row.setText(0,"CURRENT LIABILITIES");
		lia_row.setText(2,result[tol_currlia].toString());
		printableRow = new Object[]{"CURRENT LIABILITIES","",result[tol_currlia]};
		printconventinal.add(printableRow);
		for(int rowcounter =grpcode1; rowcounter < grpcode3; rowcounter ++)
		{
			TableItem lia1_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			Object[] baldata4 = (Object[]) result[rowcounter];
			
			if(baldata4[1].equals(3))
			{
				lia1_row.setText(0, "            " + baldata4[2].toString());
				lia1_row.setText(1, "            " + baldata4[3].toString());
				printableRow = new Object[]{baldata4[2],baldata4[3]};
				printconventinal.add(printableRow);

			}
		}
		
		TableItem netProfitRow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		if(profitloss[netflag].equals("netProfit"))
		{
			if(strOrgType.equals("profit making"))
			{
				netProfitRow.setText(0,"NET PROFIT");
				printableRow = new Object[]{"NET PROFIT"};
				printconventinal.add(printableRow);
			}
			else
			{
				netProfitRow.setText(0,"NET SURPLUS");
				printableRow = new Object[]{"NET SURPLUS"};
				printconventinal.add(printableRow);
			}
			netProfitRow.setText(2,profitloss[netTotalIndex].toString());
			printableRow = new Object[]{"","",profitloss[netTotalIndex]};
			printconventinal.add(printableRow);
		}
		/*else
		{
			netProfitRow.setText(0,"");
			netProfitRow.setText(1,"");
			netProfitRow.setText(2,"");
			
		}
		*/
		//for loop add empty row in Liabilities table in order to have as much rows as in the Assets table 
		if(rowFlag.equals("liabilities"))
		{
			for(int i = 0; i < rows;i++)
			{
				TableItem liaRow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				liaRow.setText(0,"");
				liaRow.setText(1,"");
				liaRow.setText(2,"");
				printableRow = new Object[]{"","",""};
				printconventinal.add(printableRow);
			}
		}
		
		
		/* Code to display the accounts of groups Fixed Assets,Investment,Current Assets,Assets Loans,
		Miscellaneous Expenses on the Assets side of the Balance Sheet */
		TableItem assestbalrow = new TableItem(tbAssetBalancesheet, SWT.NONE);
		assestbalrow.setText(0,"FIXED ASSETS");
		assestbalrow.setText(2, result[tol_fixesAsset].toString());
		printableRow = new Object[]{"","","","FIXED ASSETS","",result[tol_fixesAsset]};
		printconventinal.add(printableRow);
		
		for(int rowcounter =grpcode12; rowcounter < grpcode6; rowcounter ++)
		{
			TableItem assetrow = new TableItem(tbAssetBalancesheet, SWT.NONE);	
			Object[] Asset_baldata = (Object[]) result[rowcounter];
				
			if(Asset_baldata[1].equals(6))
			{
				assetrow.setText(0, "            " + Asset_baldata[2].toString());
				assetrow.setText(1, "            " + Asset_baldata[3].toString());
				printableRow = new Object[]{"","","",Asset_baldata[2],Asset_baldata[3]};
				printconventinal.add(printableRow);
			}
		}

		TableItem invest_row = new TableItem(tbAssetBalancesheet, SWT.NONE);
		invest_row.setText(0,"INVESTMENTS");
		invest_row.setText(2, result[tol_investment].toString());
		printableRow = new Object[]{"","","","INVESTMENTS","",result[tol_investment]};
		printconventinal.add(printableRow);
		
		for(int rowcounter =grpcode2; rowcounter < grpcode9; rowcounter ++)
		{
			TableItem invest_row1 = new TableItem(tbAssetBalancesheet, SWT.NONE);	
			Object[] invest_baldata = (Object[]) result[rowcounter];
				
			if(invest_baldata[1].equals(9))
			{
				invest_row1.setText(0, "            " + invest_baldata[2].toString());
				invest_row1.setText(1, "            " + invest_baldata[3].toString());
				printableRow = new Object[]{"","","",invest_baldata[2],invest_baldata[3]};
				printconventinal.add(printableRow);
			}
		}
		
		TableItem currasset_row = new TableItem(tbAssetBalancesheet, SWT.NONE);
		currasset_row.setText(0,"CURRENT ASSETS");
		currasset_row.setText(2, result[tol_currentasset].toString());
		printableRow = new Object[]{"","","","CURRENT ASSETS","",result[tol_currentasset]};
		printconventinal.add(printableRow);
		
		for(int rowcounter =grpcode6; rowcounter < grpcode2; rowcounter ++)
		{
			TableItem currasset_row1 = new TableItem(tbAssetBalancesheet, SWT.NONE);	
			Object[] currasset_baldata = (Object[]) result[rowcounter];
				
			if(currasset_baldata[1].equals(2))
			{
				currasset_row1.setText(0, "            " + currasset_baldata[2].toString());
				currasset_row1.setText(1, "            " + currasset_baldata[3].toString());
				printableRow = new Object[]{"","","",currasset_baldata[2],currasset_baldata[3]};
				printconventinal.add(printableRow);
			}
		}
		
		TableItem loansAssets = new TableItem(tbAssetBalancesheet, SWT.NONE);
		loansAssets.setText(0,"ASSET LOANS");
		loansAssets.setText(2,result[tol_loansasset].toString());
		printableRow = new Object[]{"","","","ASSET LOANS","",result[tol_loansasset]};
		printconventinal.add(printableRow);
		for(int rowcounter = grpcode9; rowcounter < grpcode10;rowcounter++)
		{
			TableItem loansAssetsrow = new TableItem(tbAssetBalancesheet, SWT.NONE);
			Object[] loansAssetsdata = (Object[]) result[rowcounter];
			if(loansAssetsdata[1].equals(10))
			{

				loansAssetsrow.setText(0, "            " + loansAssetsdata[2].toString());
				loansAssetsrow.setText(1, "            " + loansAssetsdata[3].toString());}
			printableRow = new Object[]{"","","",loansAssetsdata[2],loansAssetsdata[3]};
			printconventinal.add(printableRow);
		}
		
		TableItem misexp_row = new TableItem(tbAssetBalancesheet, SWT.NONE);
		misexp_row.setText(0,"MISCELLANEOUS EXPENSE");
		misexp_row.setText(2, result[tol_miscellaneous].toString());
		printableRow = new Object[]{"","","","MISCELLANEOUS EXPENSE","",result[tol_miscellaneous]};
		printconventinal.add(printableRow);
		
		
		for(int rowcounter =grpcode10; rowcounter < grpcode13; rowcounter ++)
		{
			TableItem misexp_row1 = new TableItem(tbAssetBalancesheet, SWT.NONE);	
			Object[] misexp_baldata = (Object[]) result[rowcounter];
				
			if(misexp_baldata[1].equals(13))
			{
				misexp_row1.setText(0, "            " + misexp_baldata[2].toString());
				misexp_row1.setText(1, "            " + misexp_baldata[3].toString());
				printableRow = new Object[]{"","","",misexp_baldata[2],misexp_baldata[3]};
				printconventinal.add(printableRow);

			}
		}
		
		TableItem netLoss = new TableItem(tbAssetBalancesheet, SWT.NONE);
		if(profitloss[netflag].equals("netLoss"))
		{
			if(strOrgType.equals("profit making"))
			{
				netLoss.setText(0,"NET LOSS");
				printableRow = new Object[]{"","","","NET LOSS"};
				printconventinal.add(printableRow);
			}
			else
			{
				netLoss.setText(0,"NET DEFICIT");
				printableRow = new Object[]{"","","","NET DEFICIT"};
				printconventinal.add(printableRow);
			}
			netLoss.setText(2,profitloss[netTotalIndex].toString());
			printableRow = new Object[]{"","","","","",profitloss[netTotalIndex]};
			printconventinal.add(printableRow);
		}
		else
		{
			netLoss.setText(0,"");
			netLoss.setText(1,"");
			netLoss.setText(2,"");
			printableRow = new Object[]{"","","","","",""};
			printconventinal.add(printableRow);
		}
		
		//for loop add empty row in Assets table in order to have as much rows as in the Liabilities table 
		if(rowFlag.equals("asset"))
		{
			for(int i = 0; i < rows;i++)
			{
				TableItem assetRow = new TableItem(tbAssetBalancesheet, SWT.NONE);
				assetRow.setText(0,"");
				assetRow.setText(1,"");
				assetRow.setText(2,"");
				printableRow = new Object[]{"","","","","",""};
				printconventinal.add(printableRow);
			}
		}
		
		if(profitloss[netflag].equals("netLoss"))
		{	
			/*// Code to add two empty row in Assets Table
			if(difamount != 0.00)
			{
				for(int i = 0; i < 2;i++)
				{
				TableItem assetRow = new TableItem(tbAssetBalancesheet, SWT.NONE);
				assetRow.setText(0,"");
				assetRow.setText(1,"");
				assetRow.setText(2,"");
				}
			}*/
			TableItem liaTotalTableItem = new TableItem(tbAssetBalancesheet, SWT.NONE);
			liaTotalTableItem.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
			liaTotalTableItem.setText(0,"TOTAL");
			printableRow = new Object[]{"","","","TOTAL"};
			printconventinal.add(printableRow);
			if(difamount != 0.00)
			{
				liaTotalTableItem.setText(2,nf.format(pnlDr));
				printableRow = new Object[]{"","",nf.format(pnlDr)};
				printconventinal.add(printableRow);
			}
			else
			{
				liaTotalTableItem.setText(2,nf.format(pnlDr));
				printableRow = new Object[]{"","","","","",nf.format(pnlDr)};
				printconventinal.add(printableRow);

			}


			TableItem assTotalTableItem = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			assTotalTableItem.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
			assTotalTableItem.setText(0,"TOTAL");
			printableRow = new Object[]{"TOTAL"};
			printconventinal.add(printableRow);

			if(difamount != 0.00)
			{
				assTotalTableItem.setText(2,nf.format(TotalCr));
				printableRow = new Object[]{"","",nf.format(TotalCr)};
				printconventinal.add(printableRow);

			}
			else
			{
				assTotalTableItem.setText(2,nf.format(TotalCr));
				printableRow = new Object[]{"","",nf.format(TotalCr)};
				printconventinal.add(printableRow);

			}
			
		}
		else
		{
		TableItem totalCrTableItem = new TableItem(tbAssetBalancesheet, SWT.NONE);

		totalCrTableItem.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		totalCrTableItem.setText(0,"TOTAL");
		printableRow = new Object[]{"","","","TOTAL"};
		printconventinal.add(printableRow);

	  
		if(difamount != 0.00)
		{
			totalCrTableItem.setText(2,nf.format(TotalDr));
			printableRow = new Object[]{"","","","","",nf.format(TotalDr)};
			printconventinal.add(printableRow);

		}
		else {

			totalCrTableItem.setText(2,nf.format(TotalDr));
			printableRow = new Object[]{"","","","","",nf.format(TotalDr)};
			printconventinal.add(printableRow);

		}
		
		/*// Code to add the two empty row in Liability Table
		if(difamount != 0.00)
		{
			for(int i = 0; i < 2;i++)
			{
			TableItem assetRow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			assetRow.setText(0,"");
			assetRow.setText(1,"");
			assetRow.setText(2,"");
			}
		}*/
		TableItem liaTotal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		liaTotal.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
		liaTotal.setText(0,"TOTAL");
		printableRow = new Object[]{"TOTAL"};
		printconventinal.add(printableRow);

		if(difamount != 0.00)
		{
			liaTotal.setText(2,nf.format(pnlCr));
			printableRow = new Object[]{"","",nf.format(pnlCr)};
			printconventinal.add(printableRow);

		}
		else
		{
			liaTotal.setText(2,nf.format(pnlCr));
			printableRow = new Object[]{"","",nf.format(pnlCr)};
			printconventinal.add(printableRow);

		}
		}

		//Code to show the Difference in Opening Balance to the side where the Total of Liabilities or Assets is less
		if(pnlDr > pnlCr)
		{
		if(difamount != 0.00)
		{	

			TableItem diffbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			diffbal.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
			diffbal.setText(0, "Difference In Opening Balance");
			diffbal.setText(2,nf.format(difamount));
			printableRow = new Object[]{"Difference In Opening Balance","",nf.format(difamount)};
			printconventinal.add(printableRow);

			balancingTotal = difamount + TotalCr;
			System.out.println("bal total" + balancingTotal);
			TableItem balancingbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			balancingbal.setFont(new Font(display, "Times New Roman",11,SWT.BOLD));
			balancingbal.setText(0,"TOTAL");
			balancingbal.setText(2,nf.format(balancingTotal));
			printableRow = new Object[]{"TOTAL","",nf.format(balancingTotal)};
			printconventinal.add(printableRow);

		}
	
		
		}
		else
		{
			if(difamount != 0.00)
			{

				TableItem Assetdiffbal = new TableItem(tbAssetBalancesheet, SWT.NONE);
				Assetdiffbal.setFont(new Font(display, "Times New Roman",12,SWT.BOLD));
				Assetdiffbal.setText(0, "Difference In Opening balance");
				Assetdiffbal.setText(2,nf.format(difamount));
				printableRow = new Object[]{"","","","Difference In Opening balance","",nf.format(difamount)};
				printconventinal.add(printableRow);

				balancingTotal = difamount + TotalDr;
				System.out.println("bal total" + balancingTotal);
				TableItem balancingbal_ass = new TableItem(tbAssetBalancesheet, SWT.NONE);
				balancingbal_ass.setFont(new Font(display, "Times New Roman",12,SWT.BOLD));
				balancingbal_ass.setText(0,"TOTAL");
				balancingbal_ass.setText(2,nf.format(balancingTotal));
				printableRow = new Object[]{"","","","TOTAL","",nf.format(balancingTotal)};
				printconventinal.add(printableRow);

			}
			
			
		}
	}
	public void makeaccessible(Control c) {
		/*
		 * getAccessible() method is the method of class Control which is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}
	
	protected void checkSubclass() {
		// this is blank method so will disable the check that prevents
		// subclassing of shells.
	}



}