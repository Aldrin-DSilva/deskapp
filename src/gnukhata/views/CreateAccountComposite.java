package gnukhata.views;

import java.text.NumberFormat;

import javax.swing.JList.DropLocation;

import gnukhata.globals;
import gnukhata.controllers.accountController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.internal.SWTEventListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;



public class CreateAccountComposite extends Composite
{
	boolean onceEdited = false;
	static Display display;
	TabFolder tfAccount;
	TabItem tiAddNewAccount;
	public static Label lblsavemsg;
Label lblGroupName;
Combo dropdownGroupName;
Label lblSubGroupName;
Combo dropdownSubGroupName;
Label lblAccountName;
Text txtAccountName;
Label lblOpeningBalance; 
Text txtOpeningBalance;
Label lblTotalDrOpeningBalance;
Text txtTotalDrOpeningBalance ;
Label lblTotalCrOpeningBalance;
Text txtTotalCrOpeningBalance;
Label lblDiffInOpeningBalance;
Text txtDiffInOpeningBalance;
Combo dropdownGroupName2;
Button btnSearch;
Button btnEdit;
Button btnConfirm;
Button btnDelete;
TabItem tiEditAccount;
Label lblAccountCode;
Text txtAccountCode;
Label lblnewSubGroupName;
Text  txtnewSubGroupName;
Button btnSave;
Button btnReset;
String searchText = "";
boolean verifyFlag = false; 
long searchTexttimeout = 0;
	String suggestedAccountCode = "null";
	Object[] queryParams = new Object[8];
	CreateAccountComposite cac;
	FindAndEditAccountComposite fec;
	NumberFormat nf;
	public CreateAccountComposite(Composite parent, int style) 
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
	    this.setLayout(new FormLayout());
	   MainShell.lblLogo.setVisible(false);
	   MainShell.lblLine.setVisible(false);
	   MainShell.lblOrgDetails.setVisible(false);

		lblsavemsg = new Label(this, SWT.NONE);
		lblsavemsg.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD|SWT.COLOR_RED));
		FormData layout = new FormData();
		layout.top = new FormAttachment(10);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(93);
		lblsavemsg.setLayoutData(layout);
	   
	    lblGroupName = new Label(this, SWT.NONE);
	    lblGroupName.setText("&Group Name :");
		lblGroupName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(15);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(16);
		layout.bottom = new FormAttachment(20);
		lblGroupName.setLayoutData(layout);
		

		dropdownGroupName = new Combo(this, SWT.READ_ONLY);
		layout = new FormData();
		layout.top = new FormAttachment(15);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(20);
		dropdownGroupName.setLayoutData(layout);
		
		lblSubGroupName = new Label(this, SWT.NONE);
		lblSubGroupName.setText("S&ub-Group Name :");
		lblSubGroupName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(22);
		layout.bottom = new FormAttachment(29);
		lblSubGroupName.setLayoutData(layout);
		
		dropdownSubGroupName = new Combo(this, SWT.READ_ONLY);
		layout = new FormData();
		layout.top = new FormAttachment(23);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(29);
		dropdownSubGroupName.setLayoutData(layout);
		
		lblnewSubGroupName = new Label(this, SWT.NONE);
		lblnewSubGroupName.setText("New Su&b-Group Name:");
		lblnewSubGroupName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(33);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(23);
		layout.bottom = new FormAttachment(38);
		lblnewSubGroupName.setLayoutData(layout);
		lblnewSubGroupName.setVisible(false);
		
		txtnewSubGroupName = new Text(this, SWT.BORDER);
		layout = new FormData();
		layout.top = new FormAttachment(32);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(38);
		txtnewSubGroupName.setLayoutData(layout);
		txtnewSubGroupName.setVisible(false);
		
		lblAccountName = new Label(this, SWT.NONE);
		lblAccountName.setText("A&ccount Name :");
		lblAccountName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(42);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(47);
		lblAccountName.setLayoutData(layout);
		
		txtAccountName = new Text(this, SWT.BORDER);
		layout = new FormData();
		layout.top = new FormAttachment(41);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(47);
		txtAccountName.setLayoutData(layout);
		
		lblAccountCode = new Label(this, SWT.NONE);
		lblAccountCode.setText("Account Code :");
		lblAccountCode.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(26);
		layout.bottom = new FormAttachment(56);
		lblAccountCode.setLayoutData(layout);
		lblAccountCode.setVisible(false);
		
		txtAccountCode = new Text(this, SWT.BORDER);
		layout = new FormData();
		layout.top = new FormAttachment(50);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(56);
		txtAccountCode.setLayoutData(layout);
		txtAccountCode.setVisible(false);
		
		lblOpeningBalance = new Label(this, SWT.NONE);
		lblOpeningBalance.setText("&Opening Balance (₹) :");
		lblOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(54);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(27);
		layout.bottom = new FormAttachment(59);
		lblOpeningBalance.setLayoutData(layout);
		
		txtOpeningBalance = new Text(this, SWT.RIGHT|SWT.BORDER);
		//txtOpeningBalance.setMessage("0.00");
		layout = new FormData();
		layout.top = new FormAttachment(53);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(59);
		txtOpeningBalance.setText("0.00");
		txtOpeningBalance.setLayoutData(layout);
		
		lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalDrOpeningBalance.setText("Total Debit Opening Balance:");
		lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(16);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(25);
		lblTotalDrOpeningBalance.setLayoutData(layout);
		
		
		txtTotalDrOpeningBalance = new Text(this, SWT.RIGHT|SWT.READ_ONLY|SWT.BORDER);
		
		nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setGroupingUsed(false);
		
		txtTotalDrOpeningBalance.setText(nf.format( accountController.getTotalDr()));
		layout = new FormData();
		layout.top = new FormAttachment(26);
		layout.left = new FormAttachment(77);
		layout.right = new FormAttachment(88);
		layout.bottom = new FormAttachment(35);
		txtTotalDrOpeningBalance.setLayoutData(layout);
		
		lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalDrOpeningBalance.setText("(₹):");
		lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(28);
		layout.left = new FormAttachment(89);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(35);
		lblTotalDrOpeningBalance.setLayoutData(layout);
		
		lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalCrOpeningBalance.setText("Total Credit Opening Balance :");
		lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(40);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(45);
		lblTotalCrOpeningBalance.setLayoutData(layout);
		
		
		txtTotalCrOpeningBalance = new Text(this, SWT.RIGHT|SWT.READ_ONLY|SWT.BORDER);
		txtTotalCrOpeningBalance.setText(nf.format(accountController.getTotalCr() ) );
		layout = new FormData();
		layout.top = new FormAttachment(48);
		layout.left = new FormAttachment(77);
		layout.right = new FormAttachment(88);
		layout.bottom = new FormAttachment(57);
		txtTotalCrOpeningBalance.setLayoutData(layout);
		
		lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalCrOpeningBalance.setText(" (₹):");
		lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(89);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(60);
		lblTotalCrOpeningBalance.setLayoutData(layout);
		
		
		lblDiffInOpeningBalance = new Label(this, SWT.NONE);
		lblDiffInOpeningBalance.setText("Difference in Opening Balance :");
		lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(65);
		lblDiffInOpeningBalance.setLayoutData(layout);
		
		
		txtDiffInOpeningBalance = new Text(this, SWT.RIGHT|SWT.BORDER);
		double balDiff = accountController.getTotalDr() - accountController.getTotalCr();
		Double val=Math.abs(balDiff);
		txtDiffInOpeningBalance.setText(nf.format(Double.parseDouble(val.toString())));
		layout = new FormData();
		layout.top = new FormAttachment(68);
		layout.left = new FormAttachment(77);
		layout.right = new FormAttachment(88);
		layout.bottom = new FormAttachment(77);
		txtDiffInOpeningBalance.setLayoutData(layout);
		//txtDiffInOpeningBalance.setText(String.valueOf(Double.valueOf(txtTotalDrOpeningBalance)) - (Double.valueOf(txtTotalCrOpeningBalance)));
		
		lblDiffInOpeningBalance = new Label(this, SWT.NONE);
		lblDiffInOpeningBalance.setText("(₹):");
		lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(70);
		layout.left = new FormAttachment(89);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(77);
		lblDiffInOpeningBalance.setLayoutData(layout);
		
		btnSave = new Button(this, SWT.NONE);
		btnSave.setText("&Save");
		layout = new FormData();
		layout.top = new FormAttachment(80);
		layout.left = new FormAttachment(25);
		layout.right = new FormAttachment(33);
		layout.bottom = new FormAttachment(86);
		btnSave.setLayoutData(layout);
		
		btnReset = new Button(this, SWT.NONE);
		btnReset.setText("R&eset");
		layout = new FormData();
		layout.top = new FormAttachment(80);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(48);
		layout.bottom = new FormAttachment(86);
		btnReset.setLayoutData(layout);
		
		String[] allgroups = gnukhata.controllers.accountController.getAllGroups();
		dropdownGroupName.add("------------Please select-----------");
		dropdownGroupName.select(0);
		dropdownGroupName.setFocus();
		for (int i = 0; i < allgroups.length; i++ )
		{
			dropdownGroupName.add(allgroups[i]);
			
		}
		if(globals.session[5].toString().equals("automatic") )
		{
			txtAccountCode.setEnabled(false);
		}
		
		this.getAccessible();
		//this.makeToolBar();
		this.setEvents();
		this.pack();
	    this.makeaccssible(this);
	   	
	}
	
	private void setEvents()
	{		
		this.txtOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				
				verifyFlag = true;
			}
			public void focusLost(FocusEvent arg0){
				verifyFlag=false;

				try {
					txtOpeningBalance.setText(nf.format(Double.parseDouble(txtOpeningBalance.getText())));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		dropdownGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(! lblsavemsg.getText().equals(""))
				{
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							long now = System.currentTimeMillis();
							long lblTimeOUt = 0;
							while(lblTimeOUt < (now + 2000))
							{
								lblTimeOUt = System.currentTimeMillis();
							}
					
							lblsavemsg.setText("");

						}
				});

					
				}
				//dropdownGroupName.setFocus();
			}
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				
				String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
				String[] accountlist=gnukhata.controllers.accountController.getAccountByGroup(selectedGroup);
				for (int i = 0; i < accountlist.length; i++) 
				{
					MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage(accountlist[i]);
					msg.open();
				}
			}
		});

		
		txtTotalCrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnSave.setFocus();
			}
		});
		txtTotalDrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnSave.setFocus();
			}
		});
		txtDiffInOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnSave.setFocus(); 
			}
		});
		
		this.dropdownGroupName.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
				String[] subGroups = gnukhata.controllers.accountController.getSubGroups(selectedGroup);
				
				/*String[] accountlist=gnukhata.controllers.accountController.getAccountByGroup(selectedGroup);
				for (int i = 0; i < accountlist.length; i++) 
				{
					MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage(accountlist[i]);
					msg.open();
				}*/
				
				
				queryParams[0] = selectedGroup;
				dropdownSubGroupName.removeAll();
				dropdownSubGroupName.add("-------Please select-------");
				dropdownSubGroupName.select(0);
				
				if(selectedGroup.equals("Current Asset") || selectedGroup.equals("Fixed Asset") || selectedGroup.equals("Investment") || selectedGroup.equals("Loans(Asset)"))
				{
					lblOpeningBalance.setText("Debit &Opening Balance");
				}
				if (selectedGroup.equals("Capital") || selectedGroup.equals("Corpus") || selectedGroup.equals("Current Liability") || selectedGroup.equals("Loans(Liability)") || selectedGroup.equals("Miscellaneous Expenses(Asset)") || selectedGroup.equals("Reserves") )
				{
					lblOpeningBalance.setText("Credit &Opening Balance");
				}
				if( selectedGroup.equals("Direct Income") || selectedGroup.equals("Indirect Income") || selectedGroup.equals("Direct Expense") || selectedGroup.equals("Indirect Expense") )
				{
					lblOpeningBalance.setEnabled(false);
					
					txtOpeningBalance.setEnabled(false);
					//dropdownSubGroupName.add("No Sub-Group");
				}
				else
				{
					lblOpeningBalance.setEnabled(true);
					txtOpeningBalance.setEnabled(true);
				}
				
				for (int i = 0; i < subGroups.length; i++ )
					dropdownSubGroupName.add(subGroups[i]);
				//Generating Account code
				if(selectedGroup.equals("Capital"))
				{
					suggestedAccountCode = "CP";
				}
				if(selectedGroup.equals("Current Asset"))
				{
					suggestedAccountCode = "CA";
				}
				if(selectedGroup.equals("Fixed Asset"))
				{
					suggestedAccountCode = "FA";
				}
				if(selectedGroup.equals("Direct Income"))
				{
					suggestedAccountCode = "DI";
				}
				if(selectedGroup.equals("Indirect Income"))
				{
					suggestedAccountCode = "II";
				}
				if(selectedGroup.equals("Current Liability"))
				{
					suggestedAccountCode = "CL";
				}
				if(selectedGroup.equals("Loans(Asset)"))
				{
					suggestedAccountCode = "LA";
				}
				if(selectedGroup.equals("Loans(Liability)"))
				{
					suggestedAccountCode = "LL";
				}
				if(selectedGroup.equals("Miscellaneous Expenses(Asset)"))
				{
					suggestedAccountCode = "ME";
				}
				if(selectedGroup.equals("Direct Expense"))
				{
					suggestedAccountCode = "DE";
				}
				if(selectedGroup.equals("Investment"))
				{
					suggestedAccountCode = "IV";
				}
				if(selectedGroup.equals("Reserves"))
				{
					suggestedAccountCode = "RS";
				}
				if(selectedGroup.equals("Indirect Expense"))
				{
					suggestedAccountCode = "IE";
				}								
			}
		});		
		
		dropdownGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				//code here
				if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
					//dropdownGroupName.notifyListeners(SWT.Selection ,new Event()  );
					dropdownSubGroupName.setFocus();
					return;
				}
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 500;
				
				for(int i = 0; i < dropdownGroupName.getItemCount(); i++ )
				{
					if(dropdownGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						dropdownGroupName.select(i);
					}
				}
			
			}
		});
		
	
		this.dropdownSubGroupName.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				if(dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()).equals("Create New Sub-Group"))
				{
					lblnewSubGroupName.setVisible(true);
					txtnewSubGroupName.setVisible(true);
					queryParams[1] = "Create New Sub-Group";
					
				}
				else
				{
					lblnewSubGroupName.setVisible(false);
					txtnewSubGroupName.setVisible(false);
					queryParams[1] = dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex());
					queryParams[2] = ""; 

				}
			}
			
		});
		this.dropdownSubGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(txtnewSubGroupName.isVisible())
					{
						txtnewSubGroupName.setFocus();
					}
					else
					{
						txtAccountName.setFocus();
					}
				}
				if(e.keyCode== SWT.ARROW_UP )
				{
					if(dropdownSubGroupName.getSelectionIndex() == 0)
					{
						dropdownGroupName.setFocus();
					}
				}
				
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(e.character);
				searchTexttimeout = now + 1000;					
				for(int i = 0; i < dropdownSubGroupName.getItemCount(); i++ )
				{
					if(dropdownSubGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownSubGroupName.select(i);
					}
				}				
			}
		});
		
		
		this.txtnewSubGroupName.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
			{
				if(txtnewSubGroupName.isEnabled())
				{
					txtAccountName.setFocus();
				}
			}
			if(e.keyCode==SWT.ARROW_UP)
			{
				dropdownSubGroupName.setFocus();					
			}		
		}			
		});
		this.txtnewSubGroupName.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) 
		{
			
			if(!txtnewSubGroupName.getText().trim().equals(""))
			{
				txtnewSubGroupName.setText(Character.toUpperCase(txtnewSubGroupName.getText().charAt(0)) + txtnewSubGroupName.getText().substring(1) );
			}
			String result = accountController.subgroupExists(txtnewSubGroupName.getText());
			MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
			if( Integer.valueOf(result) == 1)
			{
				msg.setMessage("The subgroup name you entered already exists");
				msg.open();
				txtnewSubGroupName.setText("");
				txtnewSubGroupName.setFocus();
			}
			
		}
		});
		
		this.txtAccountName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if(!txtAccountName.getText().trim().equals(""))
				{
					if(!onceEdited)
					{
					txtAccountName.setText(Character.toUpperCase(txtAccountName.getText().charAt(0)) + txtAccountName.getText().substring(1) );
					onceEdited = true;
					}
					String result = accountController.accountExists(txtAccountName.getText());
					if (Integer.valueOf(result) == 1)
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msg.setMessage("the account name you entered already exists, please choose another name.");
						msg.open();
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtAccountName.setText("");
								txtAccountName.setFocus();
								
							}
						});
						return;
					
					}
					else
					{
						queryParams[3] = txtAccountName.getText();
					}
					if(globals.session[5].toString().equals("manually") )
					{
						queryParams[4] = "manually";
						suggestedAccountCode = suggestedAccountCode + txtAccountName.getText().substring(0,1);
						suggestedAccountCode = accountController.getSuggestedAccountCode(suggestedAccountCode);
						txtAccountCode.setText(suggestedAccountCode);
						queryParams[7] = suggestedAccountCode;
					}
					else
					{
						queryParams[7] = "";
					}
				}
			}
		});
		this.txtAccountName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(txtOpeningBalance.isEnabled())
					{
						txtOpeningBalance.setFocus();
					}
					else
					{
						btnSave.setFocus();
						btnSave.notifyListeners(SWT.Selection, new Event());
						
					
					}
				}
				if(e.keyCode== SWT.ARROW_UP)
				{
					if(txtnewSubGroupName.getVisible())
					{
						txtnewSubGroupName.setFocus();
					}
					else
					{
						dropdownSubGroupName.setFocus();
					}
					
				}
			}
		});
		this.txtOpeningBalance.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					btnSave.notifyListeners(SWT.Selection, new Event());
				}
				if(e.keyCode == SWT.ARROW_UP)
				{
					txtAccountName.setFocus();
				}
			}
		});
		this.btnSave.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			queryParams[3] = txtAccountName.getText();
			queryParams[4] = globals.session[5];
			if(globals.session[5].toString().equals("automatic"))
			{
				queryParams[7] = "";
			}
			else
			{
				queryParams[7] = txtAccountCode.getText();
			}
			//queryParams[7] = suggestedAccountCode;
			//super.widgetSelected(arg0);
			if(dropdownGroupName.getSelectionIndex()== 0 )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please select a group for this account");
				errMessage.open();
				dropdownGroupName.setFocus();
				return;
			}
			if(dropdownSubGroupName.getSelectionIndex()== 0 )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please select a Sub-group or NO Sub-Group for this account");
				errMessage.open();
				dropdownSubGroupName.setFocus();
				return;
			}
			if(txtAccountName.getText().trim().equals("") )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please enter an Account Name");
				errMessage.open();
				txtAccountName.setFocus();

				return;
			}
			
			if ( dropdownSubGroupName.getText().equals("Create New Sub-Group") && txtnewSubGroupName.getText().trim().equals(""))
			{
				MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
				msg.setText("Error");
				msg.setMessage("Please enter an New Sub-Group Name");
				msg.open();
				txtnewSubGroupName.setFocus();
				return;
			}
			if(txtnewSubGroupName.getVisible())
			{
				queryParams[2] = txtnewSubGroupName.getText();
				
			} 
			if(txtOpeningBalance.getText().trim().equals("")|| txtOpeningBalance.getEnabled()== false )
			{
				queryParams[5] = "0.00";
				queryParams[6] = "0.00";
				
			}
			else
			{
				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				nf.setGroupingUsed(false);

				queryParams[5] = nf.format(Double.valueOf(txtOpeningBalance.getText()));
				queryParams[6] = nf.format(Double.valueOf(txtOpeningBalance.getText()));
			}
			
			if(accountController.setAccount(queryParams))
			{
				/*MessageBox successMsg = new MessageBox(new Shell(),SWT.OK| SWT.ICON_INFORMATION);
				successMsg.setText("success");
				successMsg.setMessage("Account "+ txtAccountName.getText() + " added successfully" );
				successMsg.open();
				*/

				lblsavemsg.setText("Account  " + txtAccountName.getText() + "  added successfully");
				lblsavemsg.setVisible(true);
				
				dropdownGroupName.select(0);
				dropdownSubGroupName.select(0);
				txtAccountCode.setText("");
				txtAccountName.setText("");
				txtOpeningBalance.setText("");
				txtnewSubGroupName.setText("");
				dropdownGroupName.setFocus();
				onceEdited = false;
			}
			else
			{
				MessageBox msg = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
				msg.setMessage("the account could not be saved, please try again.");
				msg.open();
			}
			
			Double totalDrOpeningBalance = 0.00;
			Double totalCrOpeningBalance = 0.00;
			Double diffBalance = 0.00;
			totalCrOpeningBalance= accountController.getTotalCr();
			totalDrOpeningBalance = accountController.getTotalDr();
			txtTotalCrOpeningBalance.setText(nf.format(totalCrOpeningBalance));
			txtTotalDrOpeningBalance.setText( nf.format(totalDrOpeningBalance));
			diffBalance = totalDrOpeningBalance - totalCrOpeningBalance;
			Double val=	Math.abs(diffBalance);
			txtDiffInOpeningBalance.setText(nf.format(Double.parseDouble(val.toString())));
								
		}
		});
			
			this.btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				
				dropdownGroupName.select(0);
				dropdownSubGroupName.select(0);
				txtAccountName.setText("");
				txtAccountCode.setText("");
				txtOpeningBalance.setText("0.00");
				txtTotalDrOpeningBalance.setText("0.00");
				txtTotalCrOpeningBalance.setText("0.00");
				txtnewSubGroupName.setText("");
				txtnewSubGroupName.setVisible(false);
				lblnewSubGroupName.setVisible(false);
				dropdownGroupName.setFocus();
				
			}
		});
			
			txtOpeningBalance.addVerifyListener(new VerifyListener() {
				
				@Override
				
				public void verifyText(VerifyEvent arg0) {
					// TODO Auto-generated method stub
					if(verifyFlag== false)
					{
						arg0.doit= true;
						return;
					}
					if(arg0.keyCode==46)
					{
						return;
					}
					switch (arg0.keyCode) {
		            case SWT.BS:           // Backspace
		            case SWT.DEL:          // Delete
		            case SWT.HOME:         // Home
		            case SWT.END:          // End
		            case SWT.ARROW_LEFT:   // Left arrow
		            case SWT.ARROW_RIGHT: // Right arrow
		            case SWT.KEYPAD_DECIMAL:
		                return;
		        }

		        if (!Character.isDigit(arg0.character)) {
		            arg0.doit = false;  // disallow the action
		        }

					

				}
			});
			}
	 public void makeaccssible(Control c)
		{
			c.getAccessible();
		}
}