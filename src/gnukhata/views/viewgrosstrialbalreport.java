
package gnukhata.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import gnukhata.globals;
import gnukhata.controllers.reportController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class viewgrosstrialbalreport extends Composite{
	static Display display;
	ODPackage sheetStream;
	int counter = 0;
	Table tblgrosstrialbal;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn totaldr;
	TableColumn totalcr;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lbltotaldr;
	Label lbltotalcr;
	Button btnViewTbForAccount;
	Button btnPrint;
    String strdate;
	NumberFormat nf;
	Vector<Object> printGrossTrial = new Vector<Object>();
	ArrayList<Button> accounts = new ArrayList<Button>();

	String endDateParam = "";
	public viewgrosstrialbalreport(Composite parent, String endDate,int style, Object[]tbData1 )
	{
		super(parent,style);
		endDateParam = endDate;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(67);
		layout.right = new FormAttachment(99);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3]);
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(75);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);
		 
		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		Label lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 15, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(40);
		lblheadline.setLayoutData(layout);
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.ITALIC | SWT.BOLD) );
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		lblOrgDetails.setText("Gross Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(25);
		
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		tblgrosstrialbal = new Table (this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION | SWT.LINE_SOLID);
		tblgrosstrialbal.setLinesVisible (true);
		tblgrosstrialbal.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(91);
		tblgrosstrialbal.setLayoutData(layout);
		
		
	  btnViewTbForAccount =new Button(this,SWT.PUSH);
		btnViewTbForAccount.setText("&Back To Trial Balance");
		btnViewTbForAccount.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblgrosstrialbal,20);
		layout.left=new FormAttachment(25);
		btnViewTbForAccount.setLayoutData(layout);

		
	    btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblgrosstrialbal,20);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);

		this.makeaccessible(tblgrosstrialbal);
		this.getAccessible();
		
		//this.setEvents();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/GrossTrialBal.ots"),"GrossTrialBal");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setReport(tbData1);
		setEvents();
		
	}
	
	private void setReport(Object[]tbData1)
	{
		String[] columns;
		columns=new String[]{"Sr.No","Account Name","Group Name","Total Debit","Total Credit"};
		
		lblsrno = new Label(tblgrosstrialbal,SWT.CENTER|SWT.LINE_SOLID);
		lblsrno.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblsrno.setText("Sr. No.");
	
		lblaccname = new Label(tblgrosstrialbal,SWT.CENTER);
		lblaccname.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblaccname.setText("Account Name");
		
		lblgrpname = new Label(tblgrosstrialbal,SWT.CENTER);
		lblgrpname.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblgrpname.setText("Group Name");
		
		lbltotaldr = new Label(tblgrosstrialbal,SWT.CENTER);
		lbltotaldr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lbltotaldr.setText("Total Debit");
		
		lbltotalcr = new Label(tblgrosstrialbal,SWT.CENTER);
		lbltotalcr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lbltotalcr.setText("Total Credit");
		
			
		headerRow = new TableItem(tblgrosstrialbal, SWT.BORDER);
		
		TableItem[] items = tblgrosstrialbal.getItems();
		srno = new TableColumn(tblgrosstrialbal, SWT.BORDER |SWT.CENTER);
		accname = new TableColumn(tblgrosstrialbal, SWT.LEFT);
		grpname = new TableColumn(tblgrosstrialbal, SWT.CENTER);
		totaldr = new TableColumn(tblgrosstrialbal, SWT.RIGHT);
		totalcr = new TableColumn(tblgrosstrialbal, SWT.RIGHT);
		
		TableEditor editorsrno = new TableEditor(tblgrosstrialbal);
		editorsrno.grabHorizontal = true;
		editorsrno.setEditor(lblsrno,items[0],0);
		

		
		TableEditor editorAccName = new TableEditor(tblgrosstrialbal);
		editorAccName.grabHorizontal = true;
		editorAccName.setEditor(lblaccname,items[0],1);
		
		TableEditor editorgrpName = new TableEditor(tblgrosstrialbal);
		editorgrpName.grabHorizontal = true;
		editorgrpName.setEditor(lblgrpname,items[0],2);
		
		TableEditor editorDr = new TableEditor(tblgrosstrialbal);
		editorDr.grabHorizontal = true;
		editorDr.setEditor(lbltotaldr,items[0],3);
		
		TableEditor editorCr = new TableEditor(tblgrosstrialbal);
		editorCr.grabHorizontal = true;
		editorCr.setEditor(lbltotalcr,items[0],4);
		
		final int tblwidth = tblgrosstrialbal.getClientArea().width;
			
		tblgrosstrialbal.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				//event.width = 220;
				srno.setWidth(6 * tblwidth / 100);
				accname.setWidth(30 * tblwidth / 100);
				grpname.setWidth(24 * tblwidth / 100);
				totaldr.setWidth(20 * tblwidth / 100);
				totalcr.setWidth(20 * tblwidth / 100);
				event.height = 14;
			}
		});
		
		srno.pack();
		accname.pack();
		grpname.pack();
		totaldr.pack();
		totalcr.pack();
		double dr = 0.00;
		double cr = 0.00;
		btnPrint.setData("printcolumns",columns);
		for(int tbcounter = 0; tbcounter< tbData1.length; tbcounter ++)
		{
			TableItem tbrow = new TableItem(tblgrosstrialbal , SWT.NONE);
			Object[] tbrecord = (Object[]) tbData1[tbcounter];
			Object[] grossRecord = (Object[]) tbData1[tbcounter];

			if(tbcounter < tbData1.length-1)
			{
				tbrow.setText(0, tbrecord[0].toString());
				Button btnAccount = new Button(tblgrosstrialbal , SWT.FLAT);
				TableEditor btnedit = new TableEditor(tblgrosstrialbal);
				btnAccount.setText(tbrecord[1].toString());
				btnedit.grabHorizontal = true;
				btnedit.setEditor(btnAccount,tbrow,1 );
				accounts.add(btnAccount);
			
				tbrow.setText(1, tbrecord[1].toString());
				tbrow.setText(2, tbrecord[2].toString());
				tbrow.setText(3, tbrecord[3].toString());
				tbrow.setText(4, tbrecord[4].toString());
				
				Object[] printableRow = new Object[]{grossRecord[0], grossRecord[1],grossRecord[2],grossRecord[3], grossRecord[4]};
				printGrossTrial.add(printableRow);												
			}
			else
			{
				tbrow.setText(0,"");
				tbrow.setText(1,"");
				tbrow.setText(2,"Total");
				tbrow.setFont(new Font(display, "Times New Roman",13,SWT.BOLD|SWT.CENTER));
				tbrow.setText(3, tbrecord[0].toString());
				tbrow.setText(4, tbrecord[1].toString());

				Object[] printableRow = new Object[]{"","","Total",grossRecord[0], grossRecord[1]};
				printGrossTrial.add(printableRow);
			}
			
			
		}
		
		TableItem closingRow = new TableItem(tblgrosstrialbal , SWT.NONE);
		closingRow.setFont(new Font(display, "Times New Roman",13,SWT.BOLD));
		//now get the Total Dr and Total Cr.
		//both can be got from the last row of tbdata.
		//get the last row (tbdata.len -1)
		//then access row[0] for TotalDr and row[1] for totalCr.
		//if dr is greater than cr then it is a dr balance.
		//substract the cr amount from dr to get the diff.
		//do exactly the other way round for cr > dr.
		
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		Object[] lastRow = (Object[]) tbData1[tbData1.length-1 ];
		dr= Double.parseDouble(lastRow[0].toString());
		cr= Double.parseDouble(lastRow[1].toString());
		Double diffbal = 0.00;
		if(dr > cr)
		{
			diffbal = dr - cr;
			closingRow.setText(1,"    Difference in Trial Balance");
			closingRow.setText(4,nf.format(diffbal));
			
			Object[] printableRow = new Object[]{""," Difference in Trial Balance","","", diffbal};
			printGrossTrial.add(printableRow);

		}
		if(cr > dr)
		{
			diffbal = cr - dr;
			closingRow.setText(1,"Difference in Trial Balance");
			closingRow.setText(3,nf.format(diffbal));
			
			Object[] printableRow = new Object[]{""," Difference in Trial Balance","", diffbal,""};
			printGrossTrial.add(printableRow);
		}
		
		TableItem totaldrcr = new TableItem(tblgrosstrialbal , SWT.NONE|SWT.SEPARATOR|SWT.BOLD);
		totaldrcr.setFont(new Font(display, "Times New Roman",12,SWT.BOLD|SWT.CENTER));
		if(dr>cr)
		{
			totaldrcr.setText(3, lastRow[0].toString());
			totaldrcr.setText(4, lastRow[0].toString());
			
			Object[] printableRow = new Object[]{"","","",lastRow[0],lastRow[0]};
			printGrossTrial.add(printableRow);
		}
		if(cr>dr)
		{
			totaldrcr.setText(3, lastRow[1].toString());
			totaldrcr.setText(4, lastRow[1].toString());
			
			Object[] printableRow = new Object[]{"","","",lastRow[1],lastRow[1]};
			printGrossTrial.add(printableRow);
		}
		tblgrosstrialbal.pack();
	}
	private void setEvents()
	{
		btnViewTbForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
			}
		});
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewTbForAccount.setFocus();
				}
			}
		});
		
			btnViewTbForAccount.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					
					
					Composite grandParent = (Composite) btnViewTbForAccount.getParent().getParent();
					btnViewTbForAccount.getParent().dispose();
						
						viewTrialBalance vl=new viewTrialBalance(grandParent,SWT.NONE);
						vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
					}
			
			});
		
		
		tblgrosstrialbal.setFocus();
		tblgrosstrialbal.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(accounts.isEmpty())
				{
					btnViewTbForAccount.setFocus();
				}
				else
				{
				accounts.get(0).setFocus();
				}
				
			}
		} );
		for(int accountcounter = 0; accountcounter < accounts.size(); accountcounter ++ )
		{
			accounts.get(accountcounter).addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					Button btncurrent = (Button) arg0.widget;
					String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
					String todate=globals.session[3].toString().substring(6)+"-"+globals.session[3].toString().substring(3,5)+"-"+globals.session[3].toString().substring(0,2);

					Composite grandParent = (Composite) btncurrent.getParent().getParent().getParent();
					
					reportController.showLedger(grandParent, btncurrent.getText(),fromdate,endDateParam, "No Project", true, true,false,"Gross Trial Balance","" );	btncurrent.getParent().getParent().dispose();
					
				}
			});
			accounts.get(accountcounter).addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < accounts.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < accounts.size())
						{
							
						accounts.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0&& counter < accounts.size())
						{
							
							accounts.get(counter).setFocus();
						}
					}
				
					
				}

			});
		}
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				String[] strPrintCol = new String[]{"","","","",""};
				Object[][] finaldata=new Object[printGrossTrial.size() +1][strPrintCol.length];
				Object[] firstRow = new Object[]{"Sr.No","Account Name","Group Name","Total Debit","Total Credit" };
				finaldata[0] = firstRow;
				
				for(int counter=0; counter < printGrossTrial.size(); counter++)
				{
					Object[] printrow=(Object[])printGrossTrial.get(counter);
			//		orgdata[counter]=printrow;
					finaldata[counter +1]=printrow;
				}	
				//printLedgerData.copyInto(finalData);
				
				TableModel model = new DefaultTableModel(finaldata,strPrintCol);
				try 
				{
					final File GrossTrialBalReport = new File("/tmp/gnukhata/Report_Output/GrossTrialBal" );
					final Sheet GrossTrialBalReportSheet = sheetStream.getSpreadSheet().getFirstSheet();
					GrossTrialBalReportSheet.ensureRowCount(100000);
					GrossTrialBalReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
					GrossTrialBalReportSheet.getCellAt(0, 1).setValue("Gross Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
					for(int rowcounter = 0; rowcounter < printGrossTrial.size(); rowcounter ++ )
					{
						Object[] printRow = (Object[]) printGrossTrial.get(rowcounter);
						GrossTrialBalReportSheet.getCellAt(0,rowcounter +3).setValue(printRow[0].toString());
						GrossTrialBalReportSheet.getCellAt(1,rowcounter +3).setValue(printRow[1].toString());
						GrossTrialBalReportSheet.getCellAt(2,rowcounter +3).setValue(printRow[2].toString());
						GrossTrialBalReportSheet.getCellAt(3,rowcounter +3).setValue(printRow[3].toString());
						GrossTrialBalReportSheet.getCellAt(4,rowcounter +3).setValue(printRow[4].toString());
					}
					OOUtils.open(GrossTrialBalReportSheet.getSpreadSheet().saveAs(GrossTrialBalReport));
					//OOUtils.open(AccountReport);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});	
		}
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		/*int vouchercode = 0;
		String voucherType = null;
		viewTrialBalReport vtbr=new viewTrialBalReport(s, SWT.NONE);
		vtbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/


}
