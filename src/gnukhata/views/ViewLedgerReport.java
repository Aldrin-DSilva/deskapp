package gnukhata.views;

import gnukhata.controllers.accountController;
import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.jopendocument.sample.SpreadSheetCreation;
/*import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
*/
import com.sun.org.apache.bcel.internal.generic.NEW;

public class ViewLedgerReport extends Composite {

	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	int counter=0;
	ODPackage sheetStream, sheetStreamNar;
	
	Table ledgerReport;
	TableColumn Date;
	TableColumn Particulars;
	TableColumn VoucherNumber;
	TableColumn Dr;
	TableColumn Cr;
	TableColumn Narration;
	
	TableItem headerRow;
	Label lblOrgDetails;
	Label lblheadline;
	Label lblorgname;
	Label lblDate;
	Label lblParticulars;
	Label lblVoucherNumber;
	Label lblNarration;
	Label lblDr;
	Label lblCr;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblPageName;
	Label lblPeriod;
	TableEditor DateEditor;
	TableEditor ParticularEditor;
	TableEditor VoucherNumberEditor;
	TableEditor DrEditotr;
	TableEditor CrEditor;
	TableEditor NarrationEditor;
	static Display display;
	
	Button btnViewLedgerForPeriod;
	Button btnViewLedgerForAccount;
	Button btnPrint;
	Button btnViewDualLedger;
	
	String tb;
	String strFromDate;
	String strToDate;
	ArrayList<Button> voucherCodes = new ArrayList<Button>();
	
	String accountName="";
	String startDate="";
	String endDate="";
	String projectName;
	String ledgerProject;
	boolean narration1;
	boolean tbflag;
	boolean projectflag;
	boolean ledgerflag;
	boolean dualflag;
	String oldaccname;
	String oldfromdate;
	String oldenddate;
	String oldselectproject;
	String oldprojectname;
	boolean oldnarration;
	Vector<Object> printLedgerData = new Vector<Object>();
	public ViewLedgerReport(Composite parent, int style, Object[] result_f,String ProjectName,String oldprojectname1, boolean narrationFlag, boolean narration, String accountName,String oldaccname1,String frmDate,String oldfromdate1,String toDate,String oldenddate1, boolean tbdrilldown, boolean psdrilldown,String tbType,String selectproject,String oldselectproject1,boolean dualledgerflag) {
		super(parent, style);
		// TODO Auto-generated constructor stub
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		//txtaccname.setText(result[0].toString());
		
		/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+selectproject);
		msg.open();*/
		
		accname=accountName;
		narration1= narrationFlag;
		tbflag = tbdrilldown;
		projectflag =psdrilldown;
		startDate=frmDate;
		endDate=toDate;
		tb = tbType;
		projectName=selectproject;
		ledgerProject=ProjectName;
		dualflag=dualledgerflag;
		
		//old values
		this.oldaccname=oldaccname1;
		this.oldfromdate=oldfromdate1;
		this.oldenddate=oldenddate1;
		this.oldselectproject=oldselectproject1;
		this.oldprojectname=oldprojectname1;
		this.oldnarration=narration;
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    strFromDate=frmDate.substring(8)+"-"+frmDate.substring(5,7)+"-"+frmDate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
	  
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();

		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		 lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(40);
		lblheadline.setLayoutData(layout);
	

		/* lblorgname=new Label(this, SWT.NONE);
		lblorgname.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
	
		lblorgname.setText(globals.session[1].toString());
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(5);
		lblorgname.setLayoutData(layout);*/
		

		Label lblAccName=new Label(this, SWT.NONE);
		lblAccName.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
		/*if(! ProjectName.equals("No Project"))
		{*/
		lblAccName.setText("Account Name: "+accname);
		/*}*/
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(5);
		lblAccName.setLayoutData(layout);
		

		lblPageName = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		lblPageName.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		lblPageName.setText("Period From "+strFromDate+" To "+strToDate);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(35);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblPageName.setLayoutData(layout);
		
		Label lblPrjName=new Label(this, SWT.NONE);
		lblPrjName.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
		//lblPrjName.setText("Project: "+ProjectName);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(70);
		lblPrjName.setLayoutData(layout);
		if(ProjectName=="No Project" || ProjectName=="")
		{
			lblPrjName.setVisible(false);
		}
		else
		{
			lblPrjName.setText("Project: "+ProjectName);
		}
		
		/*MessageBox msg1=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+selectproject);
		msg.open();*/
		
		/*lblPeriod = new Label(this, SWT.NONE);
		lblPeriod.setText("Period: "+strFromDate+"  To "+strToDate);
		lblPeriod.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(55);
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblPeriod.setLayoutData(layout);*/
		
	
	    ledgerReport= new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    ledgerReport.setLinesVisible (true);
		ledgerReport.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblPageName,10);

		if(narrationFlag)
		{
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(90);
		}
		else
		{
			layout.left = new FormAttachment(5);
			layout.right = new FormAttachment(90);
			
		}
		
		layout.bottom = new FormAttachment(74);
		ledgerReport.setLayoutData(layout);
		
		
		
		btnViewLedgerForAccount=new Button(this,SWT.PUSH);
		if(psdrilldown == false||tbdrilldown==false)
		{
				btnViewLedgerForAccount.setText("&View Another Ledger");
		
				 btnViewLedgerForAccount.setData("psdrilldown",psdrilldown );
				// btnViewLedgerForAccount.setVisible(false);
				 btnViewLedgerForAccount.setData("tbdrilldown",tbdrilldown );
					
		}

		if(tbdrilldown==true)
		{
			btnViewLedgerForAccount.setData("tbdrilldown",tbdrilldown );
			btnViewLedgerForAccount.setData("enddate",toDate  );
			btnViewLedgerForAccount.setData("tbtype", tbType  );
			btnViewLedgerForAccount.setText("&Back to Trial Balance ");
			layout = new FormData();
			layout.left = new FormAttachment(35);
			
			btnViewLedgerForAccount.setVisible(false);
		}
		
		
		
		if(psdrilldown==true)
		{
			btnViewLedgerForAccount.setData("psdrilldown",psdrilldown );
			btnViewLedgerForAccount.setData("enddate",toDate  );
			btnViewLedgerForAccount.setData("selectproject", selectproject  );
			btnViewLedgerForAccount.setText("&Back to Project Statement ");
		}
		
		btnViewLedgerForAccount.setFont(new Font(display, "Times New Roman", 14,SWT.BOLD));
		layout = new FormData();
		btnViewLedgerForAccount.setText("&View Another Ledger");
		
		layout.top=new FormAttachment(ledgerReport,8);
		//layout.left=new FormAttachment(30);
		layout.left = new FormAttachment(25);
		btnViewLedgerForAccount.setLayoutData(layout);
		btnViewLedgerForAccount.setFocus();
	
		btnViewDualLedger=new Button(this,SWT.PUSH);
		if(psdrilldown == false)
		{
			btnViewDualLedger.setText("&Dual Ledger Accounts");
			 btnViewDualLedger.setData("psdrilldown",psdrilldown );
		if(tbdrilldown == false)
		{
			btnViewDualLedger.setText("&Dual Ledger Accounts");
			btnViewDualLedger.setData("tbdrilldown",tbdrilldown );
		}
		else
		{
			btnViewDualLedger.setData("tbdrilldown",tbdrilldown );
			btnViewDualLedger.setData("enddate",toDate  );
			btnViewDualLedger.setData("tbtype", tbType  );
			btnViewDualLedger.setText("&Back to Trial Balance ");
		}
		
		
		}
		else
		{
			btnViewDualLedger.setData("psdrilldown",psdrilldown );
			btnViewDualLedger.setData("enddate",toDate  );
			btnViewDualLedger.setData("selectproject", selectproject  );
			btnViewDualLedger.setText("&Back to Project Statement ");
		}
		
		
		btnViewDualLedger.setFont(new Font(display, "Times New Roman", 14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReport,8);
		//layout.left=new FormAttachment(30);
		layout.left = new FormAttachment(10);
		btnViewDualLedger.setLayoutData(layout);
		//btnViewDualLedger.setFocus();
		
		
		
		btnViewLedgerForPeriod=new Button(this, SWT.PUSH);
		btnViewLedgerForPeriod.setText(" View &Ledger For Another Period ");
		btnViewLedgerForPeriod.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReport,8);
		layout.left=new FormAttachment(btnViewLedgerForAccount,5);
		btnViewLedgerForPeriod.setLayoutData(layout);
		if(psdrilldown==true)
		{
			btnViewLedgerForPeriod.setVisible(false);
		}
		if(tbdrilldown==true)
		{
			btnViewLedgerForPeriod.setVisible(false);
		}
		
		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReport,8);
		layout.left=new FormAttachment(btnViewLedgerForPeriod,5);
		btnPrint.setLayoutData(layout);
		
				
	  
	    this.makeaccssible(ledgerReport);
	    this.getAccessible();
	    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/Ledger.ots"),"Ledger");
			sheetStreamNar = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/LedgerNarration.ots"),"LedgerNarration");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    this.setReport(result_f, narrationFlag);
	    //this.pack();
	    this.setEvents();
	   

	}
	public void makeaccssible(Control c)
	{
		c.getAccessible();
	}
	private void setReport(Object[]result,boolean narrationFlag)
	{
		String[] columns;
		if(narrationFlag)
		{
			columns = new String[]{"Date","Particulars","Voucher No.","Dr","Cr","Narration"};
		}
		else
		{
			columns = new String[]{"Date","Particulars","Voucher No.","Dr","Cr"};
		}
		lblDate = new Label(ledgerReport,SWT.BORDER|SWT.CENTER);
		lblDate.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblDate.setText("    Date   ");
		
		lblParticulars = new Label(ledgerReport,SWT.BORDER|SWT.CENTER);
		lblParticulars.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblParticulars = new Label(ledgerReport,SWT.BORDER);
		lblParticulars.setText("    		Particulars		     ");
		
		lblVoucherNumber = new Label(ledgerReport,SWT.BORDER|SWT.CENTER);
		lblVoucherNumber.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblVoucherNumber = new Label(ledgerReport,SWT.BORDER);
		lblVoucherNumber.setText(" V.Number");
		
		lblDr = new Label(ledgerReport,SWT.BORDER|SWT.CENTER);
		lblDr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblDr = new Label(ledgerReport,SWT.BORDER);
		lblDr.setText("    		Dr		     ");
		
		lblCr = new Label(ledgerReport,SWT.BORDER|SWT.CENTER);
		lblCr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblCr = new Label(ledgerReport,SWT.BORDER);
		lblCr.setText("    		Cr		     ");
		
		lblNarration = new Label(ledgerReport,SWT.BORDER|SWT.CENTER);
		lblNarration.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReport,SWT.BORDER);
		lblNarration.setText("   Narration   ");
		  final int ledgwidth = ledgerReport.getClientArea().width;
		 
	    ledgerReport.addListener(SWT.MeasureItem, new Listener() 
		{
	    	@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				
				if(narration1==true)
				{
					Date.setWidth(10 * ledgwidth / 100);
					Particulars.setWidth(25 * ledgwidth / 100);
					VoucherNumber.setWidth(10 * ledgwidth / 100);
					Dr.setWidth(15 * ledgwidth / 100);
					Cr.setWidth(15 * ledgwidth / 100);
					Narration.setWidth(25 * ledgwidth / 100);
				}
				else
				{
					Date.setWidth(15 * ledgwidth / 100);
					Particulars.setWidth(35 * ledgwidth / 100);
					VoucherNumber.setWidth(10 * ledgwidth / 100);
					Dr.setWidth(20 * ledgwidth / 100);
					Cr.setWidth(20 * ledgwidth / 100);
				}

				
				event.height = 11;
			    
			    
			};
		});
		
	    headerRow = new TableItem(ledgerReport, SWT.NONE);
		TableItem[] items = ledgerReport.getItems();
		Date = new TableColumn(ledgerReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
		Particulars = new TableColumn(ledgerReport, SWT.BORDER);
		VoucherNumber = new TableColumn(ledgerReport, SWT.RIGHT);
		Dr= new TableColumn(ledgerReport, SWT.RIGHT);
		Cr= new TableColumn(ledgerReport, SWT.RIGHT);
		if(narration1)
		{
			Narration=new TableColumn(ledgerReport, SWT.BORDER);
		}

		
	    DateEditor = new TableEditor(ledgerReport);
		DateEditor.grabHorizontal = true;
		DateEditor.setEditor(lblDate,items[0],0);
		
		ParticularEditor = new TableEditor(ledgerReport);
		ParticularEditor.grabHorizontal = true;
		ParticularEditor.setEditor(lblParticulars,items[0],1);
		
		VoucherNumberEditor = new TableEditor(ledgerReport);
		VoucherNumberEditor.grabHorizontal = true;
		VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
		
		DrEditotr = new TableEditor(ledgerReport);
		DrEditotr.grabHorizontal = true;
		DrEditotr.setEditor(lblDr,items[0],3);
		
		CrEditor = new TableEditor(ledgerReport);
		CrEditor.grabHorizontal = true;
		CrEditor.setEditor(lblCr,items[0],4);
		
		Date.pack();
		Particulars.pack();
		VoucherNumber.pack();
	    Dr.pack();
		Cr.pack();
		
		if(narrationFlag==true)
		{
			NarrationEditor = new TableEditor(ledgerReport);
			NarrationEditor.grabHorizontal = true;
			NarrationEditor.setEditor(lblNarration,items[0],5);
			Narration.pack();
			
		}
		btnPrint.setData("printcolumns",columns );
		for(int rowcounter =0; rowcounter < result.length; rowcounter ++)
		{
			

			Object[] ledgerRecord = (Object[]) result[rowcounter];
			Object[] particulars = (Object[])  ledgerRecord[1];
			if(! ledgerProject.equals("No Project")&& particulars[0].toString().equals("Opening Balance b/f") )
			{
				continue;
			}
			TableItem ledgerRow = new TableItem(ledgerReport, SWT.NONE);
			ledgerRow.setText(0,ledgerRecord[0].toString() );

			
			for(int particularcounter = 0; particularcounter < particulars.length; particularcounter ++)
			{
				ledgerRow.setText(1, ledgerRow.getText(1) +"\n"+particulars[particularcounter].toString() + "\n" );
			}
			if(!ledgerRecord[2].toString().equals(""))
			{
				TableEditor codeEditor = new TableEditor(ledgerReport);
				Button btnVoucherCode = new Button(ledgerReport, SWT.FLAT);
				btnVoucherCode.setText(ledgerRecord[2].toString());
				codeEditor.grabHorizontal = true;
				codeEditor.setEditor(btnVoucherCode, ledgerRow,2);
				btnVoucherCode.setData("vouchercode", ledgerRecord[ledgerRecord.length -1] );
				voucherCodes.add(btnVoucherCode);
				
			}
			else
			{
				ledgerRow.setText(2, ledgerRecord[2].toString() );
				ledgerRow.setFont(new Font(display,"Times New Roman",12,SWT.BOLD));
			}
			
			ledgerRow.setText(3, ledgerRecord[3].toString() );
			ledgerRow.setText(4, ledgerRecord[4].toString() );
			
			if(narrationFlag==true)
			{
				Object[] printableRow = new Object[]{ledgerRecord[0], ledgerRecord[1],ledgerRecord[2],ledgerRecord[3], ledgerRecord[4],ledgerRecord[5]};
				printLedgerData.add(printableRow);
				
				ledgerRow.setText(5, ledgerRecord[5].toString());
			}
			else
			{
				Object[] printableRow = new Object[]{ledgerRecord[0], ledgerRecord[1],ledgerRecord[2],ledgerRecord[3], ledgerRecord[4]};
				printLedgerData.add(printableRow);
				
			}
		}
		//ledgerReport.pack();

	}
	
	public void setEvents()
	{
		
		btnViewLedgerForPeriod.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Composite grandParent=(Composite)btnViewLedgerForPeriod.getParent().getParent();
				btnViewLedgerForPeriod.getParent().dispose();
				ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,"","","","",false,false,false,"","",false);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				vl.dropdownAccountName.setText(accname);
				vl.dropdownAccountName.setEnabled(false);
				vl.txtFromDtDay.setFocus();
				vl.btnView.setEnabled(true);
			}
		});
		
		btnViewLedgerForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewLedgerForAccount.getParent().getParent();
				
				if( (Boolean) btnViewLedgerForAccount.getData("psdrilldown") == false )
				{
				if( (Boolean) btnViewLedgerForAccount.getData("tbdrilldown") == false )
				{
					btnViewLedgerForAccount.getParent().dispose();
					
					ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,"","","","",false,false,false,"","",false);
						vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
				else
				{
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String tbtype = btnViewLedgerForAccount.getData("tbtype").toString();
					btnViewLedgerForAccount.getParent().dispose();
					reportController.showTrialBalance(grandParent, enddate, tbtype);
				}
				}

				else
				{
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String sp = btnViewLedgerForAccount.getData("selectproject").toString();
					btnViewLedgerForAccount.getParent().dispose();
					reportController.showProjectStatement(grandParent, enddate, sp);
				}
		
		
		
			}
		
	});
		

		btnViewDualLedger.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//String accnamesent=btnViewDualLedger.getData("accname").toString();
				
				
				
				Composite grandParent = (Composite) btnViewDualLedger.getParent().getParent();
				
				
				if( (Boolean) btnViewDualLedger.getData("psdrilldown") == false )
				{
				if( (Boolean) btnViewDualLedger.getData("tbdrilldown") == false )
				{
				/*	String accnamesent=btnViewDualLedger.getData("accname").toString();
					String fromDate = btnViewDualLedger.getData("startDate").toString();
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String projectName=btnViewDualLedger.getData("ledgerProject").toString();
					String narrationflag1=btnViewDualLedger.getData("narration").toString();
					String tbDrilldown=btnViewDualLedger.getData("tbflag").toString();
					String psDrilldown=btnViewDualLedger.getData("projectflag").toString();
					String tbtype = btnViewLedgerForAccount.getData("tb").toString();	
					String selectproject=btnViewDualLedger.getData("projectName").toString();
				*/	btnViewDualLedger.getParent().dispose();
				if(narration1==false)
				{
			
				ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,accname,startDate,endDate,ledgerProject,false,false,false,"","",true);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
					if(narration1==true)
					{
						ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,accname,startDate,endDate,ledgerProject,true,false,false,"","",true);
						vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
			
					}
				}
				else
				{
					/*String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String tbtype = btnViewLedgerForAccount.getData("tbtype").toString();
			*/	
					btnViewDualLedger.getParent().dispose();
					reportController.showTrialBalance(grandParent, endDate, tb);
				}
				}

				else
				{
					/*String enddate = btnViewDualLedger.getData("enddate").toString();
					String sp = btnViewDualLedger.getData("selectproject").toString();
					*/
					btnViewDualLedger.getParent().dispose();
					reportController.showProjectStatement(grandParent, endDate, projectName);
				}
		}
		
		
		
		
	});

		
		
		
		btnViewLedgerForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnViewLedgerForPeriod.setFocus();
				}
			}
		});
		
		btnViewLedgerForPeriod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
			}
		});
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewDualLedger.setFocus();
				}
			}
		});
		
		btnViewDualLedger.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnViewLedgerForAccount.setFocus();
				}
			}
		});
		
		for(int voucherCodeCounter=0; voucherCodeCounter < voucherCodes.size(); voucherCodeCounter++)
		{
			voucherCodes.get(voucherCodeCounter).addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					Button btncurrent=(Button) arg0.widget;
					int vouchercode= Integer.valueOf(arg0.widget.getData("vouchercode").toString());
					
					Composite grandParent=(Composite) btncurrent.getParent().getParent().getParent();
					btncurrent.getParent().getParent().dispose();
					//transactionController.showVoucherDetail(grandParent,"", vouchercode,true);
					
					//transactionController.showVoucherDetail(grandParent, "", vouchercode, tbflag, projectflag, tb, true, startDate, endDate, accname, narration, projectName);
					//transactionController.showVoucherDetail(grandParent, "", vouchercode, tbflag, projectflag, tb1, true, startDate,oldfromdate1, endDate,oldenddate1, accountName1,oldaccName1, ledgerProject,oldprojectName1, narrationflag,narration1, projectName,oldselectproject1,dualflag1);

					transactionController.showVoucherDetail(grandParent, "", vouchercode, tbflag, projectflag, tb, true, startDate,oldfromdate, endDate,oldenddate, accname,oldaccname, ledgerProject,oldprojectname, narration1,oldnarration, projectName,oldprojectname,dualflag);
				}
			});
			
			voucherCodes.get(voucherCodeCounter).addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < voucherCodes.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < voucherCodes.size())
						{
							
							voucherCodes.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0&& counter < voucherCodes.size())
						{
							
							voucherCodes.get(counter).setFocus();
						}
					}
				
					
				}
			});
		}
		
		ledgerReport.setFocus();
		ledgerReport.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(voucherCodes.isEmpty())
				{
					btnViewDualLedger.setFocus();
				
				}
				else
				{
					voucherCodes.get(0).setFocus();
				}
			}
		});
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				String[] columns = (String[]) btnPrint.getData("printcolumns");
				Object[][] finalData =new Object[printLedgerData.size()][columns.length ];
				for(int counter = 0; counter < printLedgerData.size(); counter ++ )
				{
					Object[] printRow = (Object[]) printLedgerData.get(counter);
					String particulars = "";
					Object[] rawParticular = (Object[]) printRow[1];
					for (int r = 0; r < rawParticular.length; r ++ )
					{
						particulars = particulars + rawParticular[r].toString() + "\n";
					}
					printRow[1] = particulars;
					finalData[counter] = printRow;
				}
					
				//printLedgerData.copyInto(finalData);
				
									TableModel model = new DefaultTableModel(finalData,columns);
									try {
										final File ledger = new File("/tmp/gnukhata/Report_Output/ledger" );
					
//										SpreadSheet.createEmpty(model).saveAs(ledger);
										if (narration1== true)
										{
											final Sheet ledgerSheetNar = sheetStreamNar.getSpreadSheet().getFirstSheet();
											ledgerSheetNar.ensureRowCount(100000);
											ledgerSheetNar.getCellAt(0,0).setValue(globals.session[1].toString());
											ledgerSheetNar.getCellAt(0,1).setValue("Ledger Account");
											System.out.println("This is prj name:"+ ledgerProject);
											if(ledgerProject=="No Project" || ledgerProject=="")
											{
												ledgerSheetNar.getCellAt(0,2).setValue("Account Name: "+ accname+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Period From "+globals.session[2]+" To "+globals.session[3]);
											}
											else
											{
												ledgerSheetNar.getCellAt(0,2).setValue("Account Name: "+ accname+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Period From "+globals.session[2]+" To "+globals.session[3]+ " "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Project Name: "+ledgerProject);	
											}
											for(int rowcounter=0; rowcounter<printLedgerData.size(); rowcounter++)
											{
												Object[] printrow = (Object[]) printLedgerData.get(rowcounter);
												ledgerSheetNar.getCellAt(0,rowcounter+4).setValue(printrow[0].toString());
												ledgerSheetNar.getCellAt(1,rowcounter+4).setValue(printrow[1].toString());
												ledgerSheetNar.getCellAt(2,rowcounter+4).setValue(printrow[2].toString());
												ledgerSheetNar.getCellAt(3,rowcounter+4).setValue(printrow[3].toString());
												ledgerSheetNar.getCellAt(4,rowcounter+4).setValue(printrow[4].toString());
												ledgerSheetNar.getCellAt(5,rowcounter+4).setValue(printrow[5].toString());
												//System.out.println("This is prj name:"+ledgerProject );
											}
											OOUtils.open(ledgerSheetNar.getSpreadSheet().saveAs(ledger));
										}
										else
										{
											final Sheet ledgerSheet = sheetStream.getSpreadSheet().getFirstSheet();
//											ledgerSheet.getColumn(1).setWidth(new Integer(50));
											ledgerSheet.ensureRowCount(100000);
											ledgerSheet.getCellAt(0,0).setValue(globals.session[1].toString());
											ledgerSheet.getCellAt(0,1).setValue("Ledger Account");
											if(ledgerProject=="No Project" || ledgerProject=="")
											{
												ledgerSheet.getCellAt(0,2).setValue("Account Name: "+ accname+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Period From "+globals.session[2]+" To "+globals.session[3]);
											}
											else
											{
												ledgerSheet.getCellAt(0,2).setValue("Account Name: "+ accname+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Period From "+globals.session[2]+" To "+globals.session[3]+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Project Name: "+ledgerProject);	
											}
										
										
											for(int rowcounter=0; rowcounter<printLedgerData.size(); rowcounter++)
												{
													Object[] printrow = (Object[]) printLedgerData.get(rowcounter);
													ledgerSheet.getCellAt(0,rowcounter+4).setValue(printrow[0].toString());
													ledgerSheet.getCellAt(1,rowcounter+4).setValue(printrow[1].toString());
													ledgerSheet.getCellAt(2,rowcounter+4).setValue(printrow[2].toString());
													ledgerSheet.getCellAt(3,rowcounter+4).setValue(printrow[3].toString());
													ledgerSheet.getCellAt(4,rowcounter+4).setValue(printrow[4].toString());
												//	System.out.println("This is prj name:"+ ledgerProject);
												}
										
										OOUtils.open(ledgerSheet.getSpreadSheet().saveAs(ledger));
									}	

//						OOUtils.open(ledger);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
				
				


			}
		});
		
		
}
}
	

	
