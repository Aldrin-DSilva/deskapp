package gnukhata.views;

import gnukhata.globals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Text;

public class viewProfitAndLoss extends Composite {
	
	static String strOrgName;
	static String strFromYear;
	static String strToYear;
	public static String typeFlag;
	static Display display;
	Text txtddate;
	Text txtmdate;
	Text txtyrdate;
	Label dash1;
	Label dash2;
	Button btnView;
	
	
	Vector<Object> params;
	protected int[] orgNameList;
	
	 public viewProfitAndLoss(Composite parent,int style) 
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		    
		strToYear =  globals.session[3].toString();
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(55);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 18, SWT.ITALIC ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(6);
		layout.left = new FormAttachment(5);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("---------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,2);
		layout.left = new FormAttachment(5);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		
		Label lblprofitandloss = new Label(this, SWT.NONE);
		
		
		if(globals.session[4].equals("profit making"))
		{
			lblprofitandloss.setText(" View Profit and Loss Account :");
		}
		if(globals.session[4].equals("ngo"))
		{
			lblprofitandloss.setText(" View Income and Expenditure Account :");
		}
		
		//lblprofitandloss.setText("View Profit And Loss:");
		lblprofitandloss.setFont(new Font(display, "Times New Roman", 20, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(30);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblprofitandloss.setLayoutData(layout);
		

		Label lblperiod = new Label(this,SWT.NONE);
		lblperiod.setFont( new Font(display,"Times New Roman", 16, SWT.ITALIC ) );
		lblperiod.setText("\n\n"+"Period: "+"From: "+globals.session[2]+" To: ");
		layout = new FormData();
		layout.top = new FormAttachment(lblprofitandloss,40);
		layout.left = new FormAttachment(25);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblperiod.setLayoutData(layout);
	
		//Composite this = new Composite(tfTransaction,SWT.BORDER);
		//this.setLayout(formlayout);
		txtddate = new Text(this,SWT.BORDER);
		txtddate.setFocus();
	//	txtddate.setMessage("dd");
		txtddate.setText(strToYear.substring(0,2));
		txtddate.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(lblprofitandloss,85);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(txtddate,30);
		//layout.bottom = new FormAttachment();
		txtddate.setFocus();
		txtddate.selectAll();
		txtddate.setLayoutData(layout);
		
		
		dash1 = new Label(this,SWT.NONE);
		dash1.setText("-");
		dash1.setFont(new Font(display, "Time New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblprofitandloss,85);
		layout.left = new FormAttachment(53);
		//layout.right = new FormAttachment(22);
		//layout.bottom = new FormAttachment(9);
		dash1.setLayoutData(layout);
		
		txtmdate = new Text(this,SWT.BORDER);
		//txtmdate.setMessage("mm");
		txtmdate.setText(strToYear.substring(3,5));
		txtmdate.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(lblprofitandloss,85);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(txtmdate,30);
		//layout.bottom = new FormAttachment(9);
		txtmdate.setLayoutData(layout);
		
		dash2 = new Label(this,SWT.NONE);
		dash2.setText("-");
		dash2.setFont(new Font(display, "Time New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblprofitandloss,85);
		layout.left = new FormAttachment(57);
		//layout.right = new FormAttachment(27);
		//layout.bottom = new FormAttachment(9);
		dash2.setLayoutData(layout);
		
		txtyrdate = new Text(this,SWT.BORDER);
		/*txtyrdate.setMessage("yyyy");*/
		txtyrdate.setText(strToYear.substring(6));
		txtyrdate.setTextLimit(4);
		layout = new FormData();
		layout.top = new FormAttachment(lblprofitandloss,85);
		layout.left = new FormAttachment(58);
		layout.right = new FormAttachment(txtyrdate,50);
		//layout.bottom = new FormAttachment(9);
		txtyrdate.setLayoutData(layout);

		btnView  =new Button(this,SWT.PUSH);
		btnView.setText("&View");
		btnView.setEnabled(true);
		btnView.setFont(new Font(display, "Times New Roman", 16, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(txtddate,80);
		layout.left = new FormAttachment(40);
		layout.right=new FormAttachment(50);
		btnView.setLayoutData(layout);
		//btnView.setVisible(true);
	
		
		this.getAccessible();
		this.setEvents();
		this.pack();

	}
 

	 public void makeaccessible(Control c)
		{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
			c.getAccessible();
		}
		
		private void setEvents(){
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if(txtddate.getText().length()==txtddate.getTextLimit())
					{
						
						//txtDtDOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtmdate.setFocus();
						txtmdate.selectAll();
					}
					

				}
			});
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
					if(!txtddate.getText().equals("") && Integer.valueOf ( txtddate.getText())<10 && txtddate.getText().length()< txtddate.getTextLimit())
					{
						txtddate.setText("0"+ txtddate.getText());
						//txtFromDtMonth.setFocus();
						txtmdate.setFocus();
						return;
						
						
						
					}
					}
					

				}
			});
			

			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(txtmdate.getText().length()==txtmdate.getTextLimit())
					{
						txtyrdate.setFocus();
						txtyrdate.selectAll();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtddate.setFocus();
						txtddate.selectAll();
					}
				

				}
			});
			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{
					if(!txtmdate.getText().equals("") && Integer.valueOf ( txtmdate.getText())<10 && txtmdate.getText().length()< txtmdate.getTextLimit())
					{
						txtmdate.setText("0"+ txtmdate.getText());
						//txtFromDtMonth.setFocus();
						txtyrdate.setFocus();
						return;
						
						
						
					}
					}
					

				}
			});
			
		txtyrdate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(txtyrdate.getText().length()==txtyrdate.getTextLimit())
				{
					btnView.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtmdate.setFocus();
					txtmdate.selectAll();
				}
			

			}
		});
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			txtyrdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
				txtddate.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtddate.getText().equals("") && (Integer.valueOf(txtddate.getText())> 31 || Integer.valueOf(txtddate.getText()) <= 0) )
					{
						MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
						msgbox.setMessage("you have entered an invalid date");
						msgbox.open();
						
						txtddate.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtddate.setFocus();
							}
						});
						return;
					}
					if(!txtddate.getText().equals("") && (Integer.valueOf(txtddate.getText())< 10 && txtddate.getText().length() < txtddate.getTextLimit()))
							{
						txtddate.setText("0"+ txtddate.getText());
						return;
							}
					if(txtddate.getText().equals(""))
					{
						txtddate.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtddate.setFocus();
							}
						});
						return;
					}
					
				}
			});
				
			
			txtmdate.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtmdate.getText().equals("") && (Integer.valueOf(txtmdate.getText())> 12 || Integer.valueOf(txtmdate.getText()) <= 0) )
					{
						MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
						msgbox.setMessage("you have entered an invalid month");
						msgbox.open();
						
						txtmdate.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtmdate.setFocus();
							}
						});
						return;
					}
					if(!txtmdate.getText().equals("") && (Integer.valueOf(txtmdate.getText())< 10 && txtmdate.getText().length() < txtmdate.getTextLimit()))
							{
						txtmdate.setText("0"+ txtmdate.getText());
						return;
							}
					if(txtmdate.getText().equals(""))
					{
						txtmdate.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
							txtmdate.setFocus();
							}
						});
						return;
					}
				}
			});
			
			txtyrdate.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					
					if(txtyrdate.getText().trim().equals(""))
					{
						txtyrdate.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtyrdate.setFocus();
								txtyrdate.selectAll();
								
							}
						});
						return;
					}
					if(!txtyrdate.getText().trim().equals("") && Integer.valueOf(txtyrdate.getText()) < 0000) 
					{
				MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
				msgbox.setMessage("you have entered an invalid year");
				msgbox.open();
			
				txtyrdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtyrdate.setFocus();
						txtyrdate.selectAll();
						
					}
				});
				return;
			}
					
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					try {
						Date voucherDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
						Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
						Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
						
						if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
						{
							MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
							errMsg.setMessage("please enter the date within the financial year");
							errMsg.open();
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtyrdate.setFocus();
									txtyrdate.setText("");
								}
							});
							
							return;
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.getMessage();
					}
				
					
				}
			});
			
			btnView.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//make a call to the reportController.getLedger()
					Composite grandParent = (Composite) btnView.getParent().getParent();
					String toDate = txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText();
					//String org=globals.session[4].toString();
					btnView.getParent().dispose();
				 	gnukhata.controllers.reportController.showProfitAndLoss(grandParent, toDate);
					
				 	//dispose();
					
						
					
				}
				
			});
					
		}

		

		protected void checkSubclass()
		{
		//this is blank method so will disable the check that prevents subclassing of shells.
		}
		


}