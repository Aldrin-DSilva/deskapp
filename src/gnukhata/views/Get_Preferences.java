package gnukhata.views;
import java.net.MalformedURLException;
import java.net.URL;

import gnukhata.globals;

import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
 
public class Get_Preferences extends Dialog {
  String value;
 
  /**
   * @param parent
   */
  public Get_Preferences(Shell parent) {
    super(parent);
  }
 
  /**
   * @param parent
   * @param style
   */
  public Get_Preferences(Shell parent, int style) {
    super(parent, style);
  }
 
  /**
   * Makes the dialog visible.
   *
   * @return
   */
  public String open() {
    Shell parent = getParent();
    final Shell shell =
      new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
    shell.setText("URL Input");
 
    shell.setLayout(new GridLayout(2, true));
 
    Label label = new Label(shell, SWT.NULL);
    label.setText("Please enter &URL address:");
 
    final Text text = new Text(shell, SWT.DOUBLE_BUFFERED| SWT.BORDER);
 
    final Button buttonOK = new Button(shell, SWT.PUSH);
    buttonOK.setText("&Ok");
    buttonOK.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
    Button buttonCancel = new Button(shell, SWT.PUSH);
    buttonCancel.setText("&Cancel");
 
    text.addListener(SWT.Modify, new Listener() {
      public void handleEvent(Event event) {
        try {
          value = new String(text.getText()+ ":7081");

          buttonOK.setEnabled(true);
        } catch (Exception e) {
          buttonOK.setEnabled(true);
        }
      }
    });
    System.out.println(value); 
    buttonOK.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
			XmlRpcClientConfigImpl	  conf = new XmlRpcClientConfigImpl();
			try {
				conf.setServerURL(new URL(value));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			globals.client.setConfig(conf);

        shell.dispose();
      }
    });
 
    buttonCancel.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
        value = null;
        shell.dispose();
      }
    });
    shell.addListener(SWT.Traverse, new Listener() {
      public void handleEvent(Event event) {
        if(event.detail == SWT.TRAVERSE_ESCAPE)
          event.doit = false;
      }
    });
 
    text.setText("");
    shell.pack();
    shell.open();
 
    Display display = parent.getDisplay();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
 
    return value;
  }
 
  public static void main(String[] args) {
  
  }
}