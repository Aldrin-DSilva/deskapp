package gnukhata.views;

import java.text.NumberFormat;

import gnukhata.globals;
import gnukhata.controllers.accountController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

//import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;

public class FindAndEditAccountComposite extends Composite 
{
	double newvalue;
	double oldvalue;
	boolean editamt1=false;
	boolean focusflag=false;
	boolean editamt=false;
	boolean onceEdited = false;
	boolean VerifyFlag = false;
	boolean enterflag=false;
	
	String accountCode = "";
	String searchText = "";
	private long searchtextTimeout = 0;
	
	NumberFormat nf = NumberFormat.getInstance();
	
	static Display display;
	TabItem tiAddNewAccount;
	Composite cmpAddNewAccount;
	Composite newAComposite;
	Label lblGroupName;
	Text txtGroupName;
	Label lblSubGroupName;
	Text txtSubGroupName;
	Label lblAccountName;
	Text txtAccountName;
	Label lblOpeningBalance; 
	Text txtOpeningBalance;
	Label lblTotalDrOpeningBalance;
	Text txtTotalDrOpeningBalance ;
	Label lblTotalCrOpeningBalance;
	Text txtTotalCrOpeningBalance;
	Label lblDiffInOpeningBalance;
	Text txtDiffInOpeningBalance;
	Combo dropdownAllAccounts;
	Button btnSearch;
	Button btnEdit;
	Button btnConfirm;
	Button btnDelete;
	TabItem tiEditAccount;
	Label lblrupeesymbol;
	//Label lblrupeesymbol1;
	//Label lblrupeesymbo2;

	public FindAndEditAccountComposite(Composite parent, int style) {
		super(parent,style);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setGroupingUsed(false);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		// TODO Auto-generated constructor stub
		
	    
	   
		 lblAccountName = new Label(this, SWT.NONE);
		    lblAccountName.setText("A&ccount Name :");
		    lblAccountName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			FormData layout = new FormData();
			layout.top = new FormAttachment(20);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(24);
			layout.bottom = new FormAttachment(25);
			lblAccountName.setLayoutData(layout);
			 
			dropdownAllAccounts = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
			layout = new FormData();
			layout.top = new FormAttachment(19);
			layout.left = new FormAttachment(27);
			layout.right = new FormAttachment(50);
			layout.bottom = new FormAttachment(25);
			dropdownAllAccounts.setLayoutData(layout);
			//this.dropdownAllAccounts.setFocus();
			
			btnSearch = new Button(this, SWT.NONE);
			btnSearch.setText("&Search");
			btnSearch.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(19);
			layout.left = new FormAttachment(dropdownAllAccounts,10);
			layout.right = new FormAttachment(61);
			layout.bottom = new FormAttachment(25);
			btnSearch.setLayoutData(layout);
			
			lblGroupName = new Label(this, SWT.NONE);
			lblGroupName.setText("Group Name :");
			lblGroupName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(26);
			layout.bottom = new FormAttachment(34);
			lblGroupName.setLayoutData(layout);
			txtGroupName = new Text(this, SWT.BORDER | SWT.READ_ONLY);
			layout = new FormData();
			layout.top = new FormAttachment(28);
			layout.left = new FormAttachment(27);
			layout.right = new FormAttachment(50);
			layout.bottom = new FormAttachment(34);
			txtGroupName.setLayoutData(layout);
			
			lblSubGroupName = new Label(this, SWT.NONE);
			lblSubGroupName.setText("Sub-Group Name :");
			lblSubGroupName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(38);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(26);
			layout.bottom = new FormAttachment(43);
			lblSubGroupName.setLayoutData(layout);
			//txtSubGroupName.setEditable(false); 
			txtSubGroupName = new Text(this, SWT.BORDER |SWT.READ_ONLY);
			layout = new FormData();
			layout.top = new FormAttachment(37);
			layout.left = new FormAttachment(27);
			layout.right = new FormAttachment(50);
			layout.bottom = new FormAttachment(43);
			txtSubGroupName.setLayoutData(layout);
			
			lblAccountName = new Label(this, SWT.NONE);
			lblAccountName.setText("Account Name :");
			lblAccountName.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(47);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(24);
			layout.bottom = new FormAttachment(52);
			lblAccountName.setLayoutData(layout);
			
			txtAccountName = new Text(this, SWT.BORDER|SWT.READ_ONLY);
			layout = new FormData();
			layout.top = new FormAttachment(46);
			layout.left = new FormAttachment(27);
			layout.right = new FormAttachment(50);
			layout.bottom = new FormAttachment(52);
			txtAccountName.setLayoutData(layout);
					
			lblOpeningBalance = new Label(this,SWT.NONE );
			lblOpeningBalance.setText("Opening Balance (₹) :");
			lblOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(56);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(26);
			layout.bottom = new FormAttachment(61);
			lblOpeningBalance.setLayoutData(layout);
			
			txtOpeningBalance = new Text(this, SWT.RIGHT| SWT.READ_ONLY | SWT.BORDER);
			//txtOpeningBalance.setMessage("0.00");
			layout = new FormData();
			layout.top = new FormAttachment(55);
			layout.left = new FormAttachment(27);
			layout.right = new FormAttachment(50);
			layout.bottom = new FormAttachment(61);
			txtOpeningBalance.setLayoutData(layout);
			txtOpeningBalance.setText("0.00");
			
			
			lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
			lblTotalDrOpeningBalance.setText("Total Debit Opening Balance:");
			lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(20);
			layout.left = new FormAttachment(63);
			layout.right = new FormAttachment(98);
			layout.bottom = new FormAttachment(25);
			lblTotalDrOpeningBalance.setLayoutData(layout);
			
			txtTotalDrOpeningBalance = new Text(this, SWT.RIGHT);
			nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			nf.setGroupingUsed(false);
			txtTotalDrOpeningBalance.setText(nf.format( accountController.getTotalDr()));
			//txtTotalDrOpeningBalance.setMessage("0.00");
			layout = new FormData();
			layout.top = new FormAttachment(28);
			layout.left = new FormAttachment(75);
			layout.right = new FormAttachment(90);
			layout.bottom = new FormAttachment(34);
			txtTotalDrOpeningBalance.setLayoutData(layout);
			
			lblrupeesymbol = new Label(this, SWT.NONE);
			lblrupeesymbol.setText("(₹):");
			lblrupeesymbol.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(28);
			layout.left = new FormAttachment(91);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(34);
			lblrupeesymbol.setLayoutData(layout);
			
			lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
			lblTotalCrOpeningBalance.setText("Total Credit Opening Balance:");
			lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(38);
			layout.left = new FormAttachment(63);
			layout.right = new FormAttachment(98);
			layout.bottom = new FormAttachment(43);
			lblTotalCrOpeningBalance.setLayoutData(layout);
			
			txtTotalCrOpeningBalance = new Text(this, SWT.RIGHT);
			txtTotalCrOpeningBalance.setText(nf.format(accountController.getTotalCr() ) );
			//txtTotalCrOpeningBalance.setMessage("0.00");
			layout = new FormData();
			layout.top = new FormAttachment(46);
			layout.left = new FormAttachment(75);
			layout.right = new FormAttachment(90);
			layout.bottom = new FormAttachment(52);
			txtTotalCrOpeningBalance.setLayoutData(layout);
			
			lblrupeesymbol= new Label(this, SWT.NONE);
			lblrupeesymbol.setText("(₹):");
			lblrupeesymbol.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(46);
			layout.left = new FormAttachment(91);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(52);
			lblrupeesymbol.setLayoutData(layout);
		
			lblDiffInOpeningBalance = new Label(this, SWT.NONE);
			lblDiffInOpeningBalance.setText("Difference in Opening Balance:");
			lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(56);
			layout.left = new FormAttachment(63);
			layout.right = new FormAttachment(98);
			layout.bottom = new FormAttachment(61);
			lblDiffInOpeningBalance.setLayoutData(layout);
			
			txtDiffInOpeningBalance = new Text(this, SWT.RIGHT);
			double balDiff = accountController.getTotalDr() - accountController.getTotalCr();
			Double val=Math.abs(balDiff);
			txtDiffInOpeningBalance.setText(nf.format(Double.parseDouble(val.toString())));
			layout = new FormData();
			layout.top = new FormAttachment(64);
			layout.left = new FormAttachment(75);
			layout.right = new FormAttachment(90);
			layout.bottom = new FormAttachment(70);
			txtDiffInOpeningBalance.setLayoutData(layout);
			
			lblrupeesymbol = new Label(this, SWT.NONE);
			lblrupeesymbol.setText("(₹):");
			lblrupeesymbol.setFont(new Font(display,"Times New Romen",13,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(64);
			layout.left = new FormAttachment(91);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(70);
			lblrupeesymbol.setLayoutData(layout);
		
		
		btnEdit = new Button(this, SWT.NONE);
		btnEdit.setText("&Edit");
		btnEdit.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(28);
		layout.bottom = new FormAttachment(96);
		btnEdit.setEnabled(false);
		btnEdit.setLayoutData(layout);
		
		btnConfirm = new Button(this, SWT.NONE);
		btnConfirm.setText("C&onfirm");
		btnConfirm.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(32);
		layout.right = new FormAttachment(42);
		layout.bottom = new FormAttachment(96);
		btnConfirm.setEnabled(false);
		btnConfirm.setLayoutData(layout) ;
		
		btnDelete = new Button(this, SWT.NONE);
		btnDelete.setText("&Delete");
		btnDelete.setFont(new Font(display,"Times New Romen",14,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(96);
		btnDelete.setEnabled(false);
		btnDelete.setLayoutData(layout);
		
		layout=new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(69);
		layout.right = new FormAttachment(77);
		layout.bottom=new FormAttachment(96);
				
		String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
		dropdownAllAccounts.add("----------Please select----------");
		dropdownAllAccounts.select(0);
		
		for (int i = 0; i < allAccounts.length; i++ )
		{
			dropdownAllAccounts.add(allAccounts[i]);
			
		}
		
		this.setEvents();
		this.pack();
	this.makeaccssible(this);
	
			
		}
	private void setEvents()
	{
		this.txtAccountName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				focusflag=true;
			}
			public void focusLost(FocusEvent arg0) {
				
				
				
				if(!txtAccountName.getText().trim().equals(""))
				{
					if(!onceEdited)
					{
					txtAccountName.setText(Character.toUpperCase(txtAccountName.getText().charAt(0)) + txtAccountName.getText().substring(1) );
					onceEdited = true;
					}
					
					
				}
			}
		});
		dropdownAllAccounts.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR)
				{
					//MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					//msg.setMessage("Enter clicked");
					//msg.open();
					if(dropdownAllAccounts.getSelectionIndex() > 0)
					{
						btnSearch.notifyListeners(SWT.Selection, new Event());
					}
					
					
				}
			}
		});
		btnEdit.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
				
				if(btnDelete.isEnabled())
				{
					if(e.keyCode==SWT.ARROW_RIGHT)
					{
						btnDelete.setFocus();
					}
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					dropdownAllAccounts.setFocus();
				}
			}
		});
		btnDelete.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
					if(e.keyCode==SWT.ARROW_LEFT)
					{
						btnEdit.setFocus();
					}
				
			}
		});
		
		dropdownAllAccounts.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				//code here
				if(arg0.keyCode== SWT.CR | arg0.keyCode== SWT.KEYPAD_CR)
				{
					dropdownAllAccounts.notifyListeners(SWT.Selection ,new Event()  );
					//dropdownFinancialYear.setFocus();
					return;
				}
				/*if(!Character.isLetterOrDigit(arg0.character) )
				{
					return;
				}
				
				*/
				long now = System.currentTimeMillis();
				if (now > searchtextTimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchtextTimeout = now + 500;
				
				for(int i = 0; i < dropdownAllAccounts.getItemCount(); i++ )
				{
					if(dropdownAllAccounts.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownAllAccounts.select(i);
					}
				}
			}
		});
		
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//super.widgetSelected(e);
				//make a call to the controler for getting account details.
				String[] details = accountController.getAccountDetails(2, dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()));
				if(dropdownAllAccounts.getSelectionIndex()== 0 )
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
					errMessage.setText("Error!");
					errMessage.setMessage("Please select an account");
					errMessage.open();
					dropdownAllAccounts.setFocus();
					return;
				}
				txtGroupName.setText(details[1]);
				if(details[2].equals("0.0"))
				{
					txtSubGroupName.setText("");
				}
				else
				{
					txtSubGroupName.setText(details[2]);
				}
					txtAccountName.setEnabled(true);
				txtAccountName.setText(details[3]);
				if(txtGroupName.getText().equals("Direct Income")|| txtGroupName.getText().equals("Direct Expense") || txtGroupName.getText().equals("Indirect Income") || txtGroupName.getText().equals("Indirect Expense") )
				{
					txtOpeningBalance.setEnabled(false);
				}
				else
				{
					txtOpeningBalance.setEnabled(true);
					try {
						
						txtOpeningBalance.setText(nf.format(Double.parseDouble(details[4].toString())));
						
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.getMessage();
					}

				}
				accountCode = details[0];
				if(details[5].equals("false") &&details[6].equals("false"))
				{
					btnDelete.setEnabled(true);
				}
				else
				{
					btnDelete.setEnabled(false);
				}
				btnEdit.setEnabled(true);
				btnEdit.setFocus();
				
				
				
			}
		});
		btnSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			//	super.keyPressed(arg0);
				if(arg0.keyCode== SWT.ARROW_LEFT)
				{
					dropdownAllAccounts.setFocus();
				}
				if(arg0.keyCode== SWT.ARROW_DOWN)
				{
					btnEdit.setFocus();
				}
			}
		});
		txtGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				dropdownAllAccounts.setFocus();
			}
		});
		txtSubGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				dropdownAllAccounts.setFocus();
			}
		});
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//super.widgetSelected(e);
				MessageBox	 delConfirm = new MessageBox(new Shell(),SWT.YES | SWT.NO | SWT.ICON_QUESTION);
				delConfirm.setMessage("you are about to delete the account "+ txtAccountName.getText() + " are you sure you wish to do this?");
				int answer = delConfirm.open();
				if( answer == SWT.YES)
				{
					accountController.deleteAccount(txtAccountName.getText());
					btnDelete.setEnabled(false);
					btnEdit.setEnabled(false);
					btnConfirm.setEnabled(false);
					txtGroupName.setText("");
					txtSubGroupName.setText("");
					txtAccountName.setText("");
					//txtOpeningBalance.setText("");
					dropdownAllAccounts.removeAll();
					String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
					dropdownAllAccounts.add("----------Please Select----------");
					dropdownAllAccounts.select(0);
					for (int i = 0; i < allAccounts.length; i++ )
					{
						dropdownAllAccounts.add(allAccounts[i]);
						
					}	
					dropdownAllAccounts.setFocus();

					
				}
			}
		});
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//super.widgetSelected(e);
				txtAccountName.setEditable(true);
				txtAccountName.selectAll();
				txtAccountName.setFocus();
				txtOpeningBalance.setEditable(true);
				btnConfirm.setEnabled(true);
				btnEdit.setEnabled(false);
				dropdownAllAccounts.setEnabled(false);
				btnSearch.setVisible(false);
				
			}
		});
		txtAccountName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				
				
				
					
				if(arg0.keyCode== SWT.CR | arg0.keyCode== SWT.KEYPAD_CR)
				{
					if(enterflag==true)
					{
						editamt=true;
					}
					else if(enterflag==false)
					{
						editamt=false;
					}
					
					if(txtOpeningBalance.isEnabled())
					{
						txtOpeningBalance.setFocus();
					}
					else
					{
						btnConfirm.notifyListeners(SWT.Selection, new Event()  );
					}
				}
				else
				{
					enterflag=true;
					editamt=true;
				}
			}
		});
		
		
		txtOpeningBalance.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				
				
										
				if(arg0.keyCode== SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
				{
					btnConfirm.setFocus();
					btnConfirm.notifyListeners(SWT.Selection,new Event() );
				}
				else if(arg0.keyCode==SWT.ARROW_UP)
				{
						
					txtAccountName.setFocus();
				
				}
				
			}
		});
		txtOpeningBalance.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(VerifyFlag == false)
				{
					arg0.doit= true;
					return;
				}
				if(arg0.keyCode==46)
				{
					return;
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }

	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

				

				
			}
		});

		
		this.txtOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				oldvalue=Double.parseDouble(txtOpeningBalance.getText());
				
				VerifyFlag = true;
			}
			public void focusLost(FocusEvent arg0){
				VerifyFlag=false;
				newvalue=Double.parseDouble(txtOpeningBalance.getText());
				
				if(oldvalue==newvalue)
				{
					editamt1=false;
					
				}
				else if(oldvalue!=newvalue)
				{
					
					editamt1=true;
				}
				
				try {
					txtOpeningBalance.setText(nf.format(Double.parseDouble(txtOpeningBalance.getText())));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					txtOpeningBalance.setText("0.00");
					
					e.printStackTrace();
				}
			}
		});
		
		btnConfirm.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e){
								
				
				if(txtAccountName.getText().equals("") )
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK|SWT.ERROR );
					errMessage.setMessage("Please enter an account name");
					errMessage.open();
					txtAccountName.setFocus();
					return;
				}
				if(! txtOpeningBalance.getEnabled()|| txtOpeningBalance.getText().equals(""))
				{
					txtOpeningBalance.setText("0.00");
				}
				try {
					double newOpeningBalance = accountController.editAccount(txtAccountName.getText(),accountCode, txtGroupName.getText(),Double.valueOf(nf.format(Double.valueOf(txtOpeningBalance.getText()))));
					txtOpeningBalance.setText(nf.format(newOpeningBalance));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				txtAccountName.setEditable(false);
				onceEdited = false;
				
				if(editamt1==true | editamt==true)
				{
			
			CustomDialog confirm= new CustomDialog(new Shell());
			confirm.SetMessage("Do you want to save?");
			int answer=confirm.open();
			if(answer==SWT.YES)
			{
			MessageBox confirmMessage = new MessageBox(new Shell(),SWT.OK|SWT.ICON_INFORMATION);
			confirmMessage.setMessage("Account edited successfully");
			confirmMessage.open();
			btnEdit.setEnabled(false);
			btnConfirm.setEnabled(false);
			dropdownAllAccounts.setEnabled(true);
			btnSearch.setVisible(true);
			
			dropdownAllAccounts.removeAll();
			String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
			dropdownAllAccounts.add("----------Please select----------");
			dropdownAllAccounts.select(0);
			for (int i = 0; i < allAccounts.length; i++ )
			{
				dropdownAllAccounts.add(allAccounts[i]);
				
			}	
			dropdownAllAccounts.setFocus();
			VerifyFlag = false;
			txtOpeningBalance.setText("0.00");
			txtAccountName.setText("");
			txtGroupName.setText("");
			txtSubGroupName.setText("");
			txtAccountName.setEnabled(true);
			txtOpeningBalance.setEnabled(true);
			

			Double totalDrOpeningBalance = 0.00;
			Double totalCrOpeningBalance = 0.00;
			Double diffBalance = 0.00;
			totalCrOpeningBalance= accountController.getTotalCr();
			totalDrOpeningBalance = accountController.getTotalDr();
			txtTotalCrOpeningBalance.setText(nf.format(totalCrOpeningBalance));
			txtTotalDrOpeningBalance.setText( nf.format(totalDrOpeningBalance));
			diffBalance = totalDrOpeningBalance - totalCrOpeningBalance;
			Double val=	Math.abs(diffBalance);
			txtDiffInOpeningBalance.setText(nf.format(Double.parseDouble(val.toString())));
			}
			else if(answer==SWT.NO)
			{
				txtAccountName.setEditable(true);
				txtOpeningBalance.setEditable(true);
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtAccountName.setFocus();
						
					}
				});
				
			}
			
			}
			else if(editamt==false && editamt1 ==false)
				{
					MessageBox noedit= new MessageBox(new Shell(),SWT.OK);
					noedit.setMessage("Nothing edited");
					txtAccountName.setEditable(true);
					txtOpeningBalance.setEditable(true);
					noedit.open();
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtAccountName.setFocus();
														
						}
					});
					
				}
			}
		});
		
		txtTotalCrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(btnConfirm.getEnabled())
				{					
					btnConfirm.setFocus();
				}
				else
				{
					dropdownAllAccounts.setFocus();
				}
			}
		} );
		txtTotalDrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(btnConfirm.getEnabled())
				{
					btnConfirm.setFocus();
				}
				else
				{
					dropdownAllAccounts.setFocus();
				}

			}
		});
		txtDiffInOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(btnConfirm.getEnabled())
				{
					btnConfirm.setFocus();
				}
				else
				{
					dropdownAllAccounts.setFocus();
				}
			}
		});

	}
	
	
	
		public void makeaccssible(Control c)
			{
				c.getAccessible();
			}
		
	}