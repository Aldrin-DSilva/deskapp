package gnukhata.views;




import javax.swing.GroupLayout.Alignment;

import gnukhata.globals;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Drawable;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.sun.net.httpserver.Filter;

/*
 * @authors 
 *T Amit Chougule <acamit333@gmail.com>,
 * Girish Joshi <girish946@gmail.com>
 * * Girish Mane <girishmane8692@gmail.com>
 * * Vinay Khedekar <vinay.itengg@gmail.com>
 * * Sayali Mane <sayali@dff.org.in>
 * * Ujwala Pawade <ujwalahpawade@gmail.com>
 * * Krishnakant Mane <kk@dff.org.in>	
 */

public class MainShell extends Shell
{
	 Menu menubar;	
	 Menu master;
	 int counter;

	 MenuItem mastermenu;
	 
	static Label lblLogo;
	static Label lblLine;
	static Label lblOrgDetails;
	Composite grandParent;

	 MenuItem create_account;
	 MenuItem edit_org;
	 MenuItem add_proj;
	 MenuItem bank_rec_statement;
	 
	 Menu transactions;
	 MenuItem transactionsmenu;
	 MenuItem Contra;                              
	 MenuItem   Journal;                              
	 MenuItem   Payment;                          
	 MenuItem   Receipt;                                  
	 MenuItem   Credit_Note;                                
	 MenuItem   Debit_Note;                                
	 MenuItem   Sales;                                
	 MenuItem   Sales_Return;                                
	 MenuItem   Purchase;                                
	 MenuItem   Purchase_Return;
	 MenuItem   FindVoucher;

	 Menu reports;
	 MenuItem reportsmenu;
	 MenuItem Ledger;
	 MenuItem Trial_Balance;
	 MenuItem Project_Statement;
	 MenuItem Cash_Flow;
	 MenuItem Balance_Sheet;
	 MenuItem Profit_and_Loss_Account;
	 MenuItem Account_List;

	 
	 Menu admin;
	 MenuItem adminmenu;
	 MenuItem  New_User;                                    
	 MenuItem Change_Password;                                    
	 MenuItem Roll_Over; 
	 MenuItem Delete_Organisation;
	   
	 Menu help;
	 Menu sessionbtn;
	 MenuItem sbtn;
	 MenuItem helpmenu;
	 MenuItem About_Gnukhata;
	 MenuItem Logout;
	 MenuItem Quit;
	 MenuItem Authors;                        
	 MenuItem Shortcut_Keys;                            
	 MenuItem Gnukhata_License;                     
	 MenuItem Gnukhata_Manual;
	 
	 ToolItem Purchase_Rerurn;
	 ToolItem tishowhide;
	Label lblorgname;
	 //Button logout;
	
	 Composite currentcomposite;
	 Composite basecomposite;
	 
	 static String strOrgName;
	 static String strFromYear;
	 static String strToYear;
	 static String strtype;
	 Label lblRegiNo;
	 Label lblnote;
	 int flag=0;
	 			protected Shell shell;
	 Composite formArea;
	 ToolBar tb;
	 
	public MainShell(Display display)
	{	
		super(display,SWT.SHELL_TRIM );
		this.setFullScreen(true);
		FormLayout	  fl = new FormLayout();
		Rectangle bounds = this.getDisplay().getPrimaryMonitor().getBounds();
		this.setBounds(bounds);
		
		
		this.setLayout(fl);
		this.setText("Gnukhata");
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout=new FormData();
	    
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(strOrgName+ "\n"+"For Financial Year "+"From "+strFromYear+" To "+strToYear);
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(40);
		layout.bottom = new FormAttachment(7);
		lblOrgDetails.setLayoutData(layout);
		
		lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(82);
		layout.bottom = new FormAttachment(7);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display, "finallogo1.png");
		lblLogo.setImage(globals.logo);
			
		lblLine = new Label(this, SWT.NONE);
		lblLine.setText("------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(82);
		lblLine.setLayoutData(layout);
	
	//initialise menus.
		this.menubar = new Menu(this,SWT.BAR);
		this.sessionbtn=new Menu(menubar); 
		this.master=new Menu(menubar);

		 this.transactions=new Menu(menubar);
		 this.reports=new Menu(menubar);
		 this.admin=new Menu(menubar);
		 this.help=new Menu(menubar);
		 
		 this.sbtn=new MenuItem(menubar, SWT.CASCADE);
		 this.sbtn.setText("Sessi&on");
		 this.sbtn.setMenu(sessionbtn);
		 
		 this.mastermenu=new MenuItem(menubar,SWT.CASCADE);
		 this.mastermenu.setText("&Master");
		 this.mastermenu.setMenu(master);
		 
		 this.transactionsmenu=new MenuItem(menubar,SWT.CASCADE);
		 this.transactionsmenu.setText("&Transactions");
		 this.transactionsmenu.setMenu(this.transactions);
		 
		 this.reportsmenu=new MenuItem(menubar,SWT.CASCADE);
		 this.reportsmenu.setText("&Reports");
		 this.reportsmenu.setMenu(this.reports);
		 
		 this.adminmenu=new MenuItem(menubar,SWT.CASCADE);
		 this.adminmenu.setText("&Administration");
		 this.adminmenu.setMenu(this.admin);
		 
		 this.helpmenu=new MenuItem(menubar,SWT.CASCADE);
		 this.helpmenu.setText("&Help");
		 this.helpmenu.setMenu(this.help);
		 
		 
		 
		 this.create_account=new MenuItem(master,SWT.None);
		 this.create_account.setText("&Account Creation/Find/Edit");
		 this.create_account.setAccelerator(SWT.F2);
		 this.edit_org=new MenuItem(master,SWT.None);
		 this.edit_org.setText("Edit Organization Details");
		 this.edit_org.setAccelerator(SWT.CTRL + 'e');

		 this.add_proj=new MenuItem(master,SWT.None);
		 this.add_proj.setText("Add &More Projects");
		 this.add_proj.setAccelerator(SWT.CTRL + 'm');
		 this.bank_rec_statement=new MenuItem(master,SWT.None);
		 this.bank_rec_statement.setText("Bank Reconciliation Statement");
		 this.bank_rec_statement.setAccelerator(SWT.F12);
		 
		 this.Contra=new MenuItem(this.transactions,SWT.None);
		 this.Contra.setText("&Contra");
		 this.Contra.setAccelerator(SWT.F4);
		 this.Payment=new MenuItem(this.transactions,SWT.None);
		 this.Payment.setText("&Payment");
		 this.Payment.setAccelerator(SWT.F5);
		 this.Receipt=new MenuItem(this.transactions,SWT.None);
		 this.Receipt.setText("&Receipt");
		 this.Receipt.setAccelerator(SWT.F6);
		 this.Journal=new MenuItem(this.transactions,SWT.None);
		 this.Journal.setText("&Journal");
		 this.Journal.setAccelerator(SWT.F7);
		 this.Sales=new MenuItem(this.transactions,SWT.None);
		 this.Sales.setText("&Sales");
		 this.Sales.setAccelerator(SWT.F8);
		 this.Purchase=new MenuItem(this.transactions,SWT.None);
		 this.Purchase.setText("P&urchase");
		 this.Purchase.setAccelerator(SWT.F9);
		 this.Credit_Note=new MenuItem(this.transactions,SWT.None);
		 this.Credit_Note.setText("Cre&dit Note");
		 this.Credit_Note.setAccelerator(SWT.CTRL + SWT.F2);
		 this.Debit_Note=new MenuItem(this.transactions,SWT.None);
		 this.Debit_Note.setText("De&bit Note");
		 this.Debit_Note.setAccelerator(SWT.CTRL + SWT.F3);
		 
		 this.Sales_Return=new MenuItem(this.transactions,SWT.None);
		 this.Sales_Return.setText("Sales Return");
		 this.Sales_Return.setAccelerator(SWT.CTRL + SWT.F4);
		 
		 this.Purchase_Return=new MenuItem(this.transactions,SWT.None);
		 this.Purchase_Return.setText("Purc&hase Return");
		 this.Purchase_Return.setAccelerator(SWT.CTRL + SWT.F5);
		 
		 this.FindVoucher=new MenuItem(this.transactions, SWT.None);
		 this.FindVoucher.setText("Find Voucher");
		 this.FindVoucher.setAccelerator(SWT.CTRL + 'F');
		                                    
		 this.Ledger=new MenuItem(this.reports,SWT.None);
		 this.Ledger.setText("&Ledger");
		 this.Ledger.setAccelerator(SWT.F3);
		 this.Trial_Balance=new MenuItem(this.reports,SWT.None);
		 this.Trial_Balance.setText("&Trial Balance");
		 this.Trial_Balance.setAccelerator(SWT.CTRL + SWT.F7);
		 this.Project_Statement=new MenuItem(this.reports,SWT.None);
		 this.Project_Statement.setText("Pr&oject Statement");
		 this.Project_Statement.setAccelerator(SWT.CTRL + SWT.F11);
		 this.Cash_Flow=new MenuItem(this.reports,SWT.None);
		 this.Cash_Flow.setText("Cash &Flow");
		 this.Cash_Flow.setAccelerator(SWT.CTRL + SWT.F10);
		 this.Balance_Sheet=new MenuItem(this.reports,SWT.None);
		 if(globals.session[4].equals("profit making"))
		 {
			 this.Balance_Sheet.setText("Bala&nce Sheet"); 
		 }
		 if(globals.session[4].equals("ngo"))
			{
				this.Balance_Sheet.setText("Statement of &Affairs");
			}
		 this.Balance_Sheet.setAccelerator(SWT.CTRL + SWT.F9);
		 
		 this.Profit_and_Loss_Account=new MenuItem(this.reports,SWT.None);
		 
		 if(globals.session[4].equals("profit making"))
		{
			this.Profit_and_Loss_Account.setText("Prof&it and Loss Account");
		}
		if(globals.session[4].equals("ngo"))
		{
			this.Profit_and_Loss_Account.setText("&Income and Expenditure Account");
		}
		this.Profit_and_Loss_Account.setAccelerator(SWT.CTRL+SWT.F8);
		
		 this.Account_List=new MenuItem(this.reports, SWT.None);
		 this.Account_List.setText("List Of Accounts");
		 this.Account_List.setAccelerator(SWT.CTRL + 'R');
		 
		
		if (globals.session[7].toString().equals("-1"))
		 {
			 this.New_User=new MenuItem(this.admin,SWT.None);
			 this.New_User.setText("New &User");
			 this.New_User.setAccelerator(SWT.CTRL+'u');
			 this.Change_Password=new MenuItem(this.admin,SWT.None);
			 this.Change_Password.setText("Change Pass&word");
			 this.Change_Password.setAccelerator(SWT.CTRL+'w');
			 this.Roll_Over=new MenuItem(this.admin,SWT.None);
			 this.Roll_Over.setText("Roll &Over");
			 this.Roll_Over.setAccelerator(SWT.CTRL+'o');
			 this.Delete_Organisation=new MenuItem(this.admin,SWT.None);
			 this.Delete_Organisation.setText("&Delete Organisation");
			 this.Delete_Organisation.setAccelerator(SWT.CTRL+'d');
		 }
		 if (globals.session[7].toString().equals("0"))
		 {
			 this.New_User=new MenuItem(this.admin,SWT.None);
			 this.New_User.setText("New &User");
			 this.New_User.setAccelerator(SWT.CTRL+'u');
			 this.Change_Password=new MenuItem(this.admin,SWT.None);
			 this.Change_Password.setText("Change Pass&word");
			 this.Change_Password.setAccelerator(SWT.CTRL+'w');
		 }
		 if (globals.session[7].toString().equals("1"))
		 {
			 
			 this.Change_Password=new MenuItem(this.admin,SWT.None);
			 this.Change_Password.setText("Change Pass&word");
			 this.Change_Password.setAccelerator(SWT.CTRL+'w');
		 }
		 this.Gnukhata_Manual=new MenuItem(this.help,SWT.None);
		 this.Gnukhata_Manual.setText("Gnukhata Manual ");
		 this.Authors=new MenuItem(this.help,SWT.None);
		 this.Authors.setText("Authors");
		 this.Shortcut_Keys=new MenuItem(this.help,SWT.None);
		 this.Shortcut_Keys.setText("Shortcut Keys");
		 this.Gnukhata_License=new MenuItem(this.help,SWT.None);
		 this.Gnukhata_License.setText("Gnukhata License");
		 this.About_Gnukhata=new MenuItem(this.help,SWT.None);
		 this.About_Gnukhata.setText("About Gnukhata ");
		
		 this.Logout=new MenuItem(this.sessionbtn,SWT.None);
		 this.Logout.setText("Logout");
		 this.Logout.setAccelerator(SWT.CTRL+'g');
		 this.Quit=new MenuItem(this.sessionbtn,SWT.None);
		 this.Quit.setText("Quit");
		 this.Quit.setAccelerator(SWT.CTRL+'q');
		 
		
		    this.setMenuBar(menubar);

tb = new ToolBar(menubar.getShell(),SWT.BORDER| SWT.VERTICAL);
//tb.setLocation(0,menubar.getShell().getClientArea().width-15  );
FormData fd = new FormData();
fd.top = new FormAttachment(0);
fd.left = new FormAttachment(lblLogo,2);
fd.right = new FormAttachment(100);			
tb.setLayoutData(fd);
Color clrBlack = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY);
Color clrWhite = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);


tb.setBackground(clrBlack);
tb.setForeground(clrWhite);

tishowhide = new ToolItem(tb, SWT.PUSH);
tishowhide.setText("Show/Hide ToolBar: f1");
Color clr = Display.getCurrent().getSystemColor(SWT.COLOR_BLUE);
Image hotImage = new Image(display, 100, 10);
GC gc = new GC(hotImage);
gc.setBackground(clr);
gc.fillRectangle(hotImage.getBounds());
tishowhide.setImage(hotImage);
gc.dispose();
hotImage.dispose();
/*tishowhide.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		tishowhide.notifyListeners(SWT.Selection, new Event());
	}
});*/


ToolItem tiAccount = new ToolItem(tb , SWT.PUSH);
tiAccount.setText("Create Account: f2");
//tiAccount.setFont(new Font(display,"Times New Roman",14,clr));
tiAccount.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		create_account.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiAccountReport = new ToolItem(tb, SWT.PUSH);
tiAccountReport.setText("List Of Accounts : CTRL + R");
tiAccountReport.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		Account_List.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiLedger = new ToolItem(tb, SWT.PUSH);
tiLedger.setText("Ledger: f3");		

tiLedger.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Ledger.notifyListeners(SWT.Selection, new Event());
	}
});
			
ToolItem tiContra = new ToolItem(tb, SWT.PUSH);
tiContra.setText("Contra: f4");
tiContra.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Contra.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiPayment = new ToolItem(tb, SWT.PUSH);
tiPayment.setText("Payment: f5");
tiPayment.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Payment.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiReceipt  = new ToolItem(tb, SWT.PUSH);
tiReceipt.setText("Receipt: f6");
tiReceipt.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Receipt.notifyListeners(SWT.Selection, new Event());
	}
});
ToolItem tiJournal = new ToolItem(tb, SWT.PUSH);
tiJournal.setText("Journal: f7");		
tiJournal.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Journal.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiSales = new ToolItem(tb, SWT.PUSH);
tiSales.setText("Sales: f8");
tiSales.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Sales.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiPurchase = new ToolItem(tb, SWT.PUSH);
tiPurchase.setText("Purchase: f9");
tiPurchase.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Purchase.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiCredit = new ToolItem(tb, SWT.PUSH );
tiCredit.setText("Credit Note: CTRL+f2");
tiCredit.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Credit_Note.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiDebit = new ToolItem(tb, SWT.PUSH);
tiDebit.setText("Debit Note: CTRL+f3");
tiDebit.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Debit_Note.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem tiSalesReturn = new ToolItem(tb, SWT.PUSH);
tiSalesReturn.setText("Sales Return: CTRL+f4");
tiSalesReturn.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Sales_Return.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiPurchaseReturn = new ToolItem(tb, SWT.PUSH);
tiPurchaseReturn.setText("Purchase Return: CTRL+f5");
tiPurchaseReturn.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Purchase_Return.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem tiTrialBalance = new ToolItem(tb, SWT.PUSH);
tiTrialBalance.setText("Trial Balance: CTRL+f7");
tiTrialBalance.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Trial_Balance.notifyListeners(SWT.Selection, new Event());
	}
});
ToolItem pnl = new ToolItem(tb, SWT.PUSH);
pnl.setText("Profit And Loss: CTRL+f8");
pnl.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Profit_and_Loss_Account.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem tiBalanceSheet = new ToolItem(tb, SWT.PUSH);
tiBalanceSheet.setText("Balance Sheet :  CTRL+f9");		

tiBalanceSheet.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Balance_Sheet.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem tiCash_Flow = new ToolItem(tb, SWT.PUSH);
tiCash_Flow.setText("Cash Flow: CTRL+f10");
tiCash_Flow.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Cash_Flow.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem tiProjectStatement = new ToolItem(tb, SWT.PUSH);
tiProjectStatement.setText("Project Statement: CTRL+f11");
tiProjectStatement.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Project_Statement.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiEditVoucher=new ToolItem(tb, SWT.PUSH);
tiEditVoucher.setText("Edit Organisation : CTRL+E");
tiEditVoucher.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		edit_org.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem tiAddProject=new ToolItem(tb, SWT.PUSH);
tiAddProject.setText("Add Project: CTRL+M");
tiAddProject.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		add_proj.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem tiFindVoucher=new ToolItem(tb, SWT.PUSH);
tiFindVoucher.setText("Find Voucher: CTRL+F");
tiFindVoucher.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		FindVoucher.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem logout = new ToolItem(tb, SWT.PUSH);
logout.setText("Logout: CTRL+G");
logout.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Logout.notifyListeners(SWT.Selection, new Event());
	}
});


ToolItem quit= new ToolItem(tb, SWT.PUSH);
quit.setText("Quit: CTRL+Q");
quit.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Quit.notifyListeners(SWT.Selection, new Event());
	}
});

/*ToolItem NewUser= new ToolItem(tb, SWT.PUSH);
NewUser.setText("New User: CTRL+U");
NewUser.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		New_User.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem ChangePass= new ToolItem(tb, SWT.PUSH);
ChangePass.setText("Change Password: CTRL+W");
ChangePass.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Change_Password.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem Deleteorg= new ToolItem(tb, SWT.PUSH);
Deleteorg.setText("Delete Organisation: CTRL+D");
Deleteorg.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Delete_Organisation.notifyListeners(SWT.Selection, new Event());
	}
});
*/
tb.setVisible(true);
tb.pack();
this.pack();

			
			/*MessageBox mswidth = new MessageBox(new Shell(),SWT.OK);
			mswidth.setMessage("now the width of main shell is " + Integer.toString(this.getClientArea().width ));
			mswidth.open();*/
			formArea  = new Composite(this, SWT.NONE);
			fd = new FormData();
			fd.top = new FormAttachment(5);
			fd.left= new FormAttachment(0);
			fd.bottom= new FormAttachment(100);
			fd.right = new FormAttachment(100);
			formArea.setLayoutData(fd);
			formArea.setBounds(bounds);
			//formArea.pack();
			/*MessageBox wval = new MessageBox(new Shell(),SWT.OK);
			wval.setMessage("with of form area is " + Integer.toString(formArea.getClientArea().width ));
			wval.open();*/
			Group grpMain = new Group(formArea, SWT.NONE);
			layout = new FormData();
			//grpMain.setText("Note");
			layout.right=new FormAttachment(60);
			grpMain.setLocation(90, 100);//left,top
			
			
			lblnote = new Label(grpMain, SWT.CENTER);
			lblnote.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
			lblnote.setText("\tPress and Hold alt for screen specific shortcuts.");
			lblnote.setLocation(20,20);
			lblnote.pack();   
			
			Label lblnote2 = new Label(grpMain, SWT.CENTER);
			lblnote2.setFont(new Font(display, "Times New Roman", 16, SWT.ITALIC));
			lblnote2.setText("\n\nAfter opening the Opening Stock Account under"+"\n"+"\t the Group Current Assets, you will have to transfer "+"\n"+"this account to Profit and Loss Account by way of"+"\n"+"Journal entry.");
			lblnote2.setLocation(20, 45);
			lblnote2.pack();
			//grpMain.pack();
		        
			
			Label lblnote3 = new Label(grpMain, SWT.NONE);
			lblnote3.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
			lblnote3.setText("\n\n\n\n\n\n\t"+"Press f1 for Help side bar."+"\n"+"\tPress f1 again for closing of Help side bar.");
			lblnote3.setLocation(20, 65);
			lblnote3.pack();
			grpMain.pack();
		        
		    this.setImage(globals.icon);
		    this.makeaccssible(this);
		    this.setEvent();
		    this.pack();
		    this.open();
		    this.showView();
	}
	
	private void setEvent()
	{
		this.getDisplay().addFilter(SWT.KeyDown, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.F1 )					
				{
					if(tb.getVisible())
					{
					tb.setVisible(false);
					
					}
					else
					{
						tb.setVisible(true);
					
					}
				
				}

			}
		});
		formArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				//make the toolbar visible.
			}
			});
		
		this.tishowhide.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent se){
				
				tb.setVisible(false);
			}
		});
		
			this.create_account.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent se){
				Control[] ctrls = formArea.getChildren();
				if(ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				
			AccountTabForm tb = new AccountTabForm(formArea,SWT.NONE);
			tb.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
		
			}
	   });
			
		/*this.edit_org.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				EditOrganisation eo=new EditOrganisation(formArea, SWT.None);
				eo.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
					
			}
		});
		*/
			
		/*this.add_proj.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				AddMoreProjects amp=new AddMoreProjects(formArea, SWT.None);
				amp.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				amp.chkbtnManualAccCode.setFocus();
			}
		});*/
		
		this.Contra.addSelectionListener(new SelectionAdapter() 
			{
				public void widgetSelected(SelectionEvent se)
				{
					Control[] ctrls = formArea.getChildren();
					
					if (ctrls.length > 0)
					{
						ctrls[0].dispose();
					}

					VoucherTabForm.typeFlag = "Contra";
					VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
					vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				}
		
		});
		this.Journal.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
			
				VoucherTabForm.typeFlag = "Journal";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		this.Payment.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				VoucherTabForm.typeFlag = "Payment";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		this.Receipt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				VoucherTabForm.typeFlag = "Receipt";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		this.Credit_Note.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				VoucherTabForm.typeFlag = "Credit Note";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		this.Debit_Note.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				VoucherTabForm.typeFlag = "Debit Note";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		this.Sales.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				VoucherTabForm.typeFlag = "Sales";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		this.Sales_Return.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				VoucherTabForm.typeFlag = "Sales Return";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		this.Purchase.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				VoucherTabForm.typeFlag = "Purchase";
				VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
				vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		this.Purchase_Return.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}			
				VoucherTabForm.typeFlag = "Purchase Return";
			VoucherTabForm vf = new VoucherTabForm(formArea,SWT.NONE);
			vf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		
		this.FindVoucher.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if(ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				VoucherTabForm.typeFlag = "Find Voucher";
				VoucherTabForm vtb = new VoucherTabForm(formArea, SWT.None);
				vtb.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
				FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(vtb.tfTransaction, SWT.NONE);
				vtb.tfTransaction.setSelection(1);
				vtb.tifdrecord.setControl(fdvoucher);
				vtb.tinewvoucher.dispose();
				}
		});
		
		this.bank_rec_statement.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewReconciliation vr = new viewReconciliation(formArea,SWT.NONE,false);
				vr.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		
		this.Trial_Balance.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewTrialBalance vtb = new viewTrialBalance(formArea,SWT.NONE);
				vtb.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		this.Project_Statement.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				ViewProjectStatement vps = new ViewProjectStatement(formArea, SWT.NONE);
				vps.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
if(globals.session[7].toString().equals("-1") || globals.session[7].toString().equals("0"))
{
	
		this.New_User.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				createNewUser cnu= new createNewUser(formArea, SWT.NONE);
				cnu.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		}
		
		this.Profit_and_Loss_Account.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewProfitAndLoss vpl= new viewProfitAndLoss(formArea,SWT.NONE);
				vpl.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		
		this.Balance_Sheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewBalanceSheet vbs= new viewBalanceSheet(formArea, SWT.NONE);
				vbs.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});


		this.Cash_Flow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewCashflow vcf = new viewCashflow(formArea,SWT.NONE);
				vcf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		this.Account_List.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			Control[] ctrls = formArea.getChildren();
			if (ctrls.length > 0)
			{
				ctrls[0].dispose();
			}
			
			gnukhata.controllers.reportController.getAccountReport(formArea);
		
		}
	});
		
		this.Ledger.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				ViewLedger vl=new ViewLedger(formArea,SWT.NONE,"","","","",false,false,false,"","",false);
								vl.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
			}
		});
		
		this.Logout.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setMessage("are you sure you wish to log out?");
				int answer = msg.open();
				if(answer == SWT.YES)
				{
					tb.dispose();
					menubar.getShell().getDisplay().dispose();
					//menubar.getShell().dispose();
					System.gc();
					startupForm sf = new startupForm();
				}
			}
		});
		
		this.Quit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setMessage("are you sure you wish to quit?");
				int answer = msg.open();
				if(answer == SWT.YES)
				{
					menubar.getShell().dispose();
					System.exit(0);
				}				
			}
		});	
		if(globals.session[7].toString().equals("-1"))
		{
		this.Delete_Organisation.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setMessage("Are you sure you want to Delete this Organisation?");
				int answer = msg.open();
				if(answer == SWT.NO)
				{
					return;
				}
				if(answer==SWT.YES)
				{
					tb.dispose();
					menubar.getShell().getDisplay().dispose();
					System.gc();
					gnukhata.controllers.StartupController.DeleteOrg(grandParent, strOrgName, strFromYear, strToYear);
					//dispose();
					gnukhata.controllers.StartupController.showstartupForm();
					
				}
			}
		});
		
		this.Roll_Over.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				RolloverandClosebooks rc = new RolloverandClosebooks(formArea, SWT.NONE);
				rc.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
			}
		});
		}
		if(globals.session[7].toString().equals("-1") || globals.session[7].toString().equals("0") || globals.session[7].toString().equals("1"))
		{
		
		this.Change_Password.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				ChangePassword cp=new ChangePassword(formArea, SWT.NONE);
				cp.setSize(formArea.getClientArea().width, formArea.getClientArea().height);		
				
			}
		});
		}
	
		}

	
	
	
	 protected void checkSubclass()
	 	{
	        // Disable the check that prevents subclassing of SWT components
	    }
	
	 public void makeaccssible(Control c)
		{
			c.getAccessible();
		}
	 
	 private void showView()
		{
			while(! this.isDisposed())
			{
				if(! this.getDisplay().readAndDispatch())
				{
					this.getDisplay().sleep();
					if ( ! this.getMaximized())
					{
						this.setMaximized(true);
					}
				}				
			}
			this.dispose();


		}
	 
	 // TODO kindly remove the main() after testing the user interface.
	public static void main(String[] args) 
	{
		Display display=Display.getDefault();
		MainShell mainscr =new MainShell(display);
		mainscr.pack();
		mainscr.open();
		
		while (!mainscr.isDisposed() ) {
		if (!display.readAndDispatch())
		{
			 display.sleep();
		}
	}
		
	}
}
