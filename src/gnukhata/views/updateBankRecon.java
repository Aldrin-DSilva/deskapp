package gnukhata.views;

import gnukhata.controllers.accountController;
import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

//import org.classpath.icedtea.pulseaudio.Stream.UpdateTimingInfoListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.jopendocument.sample.SpreadSheetCreation;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;
/*import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
*/
import com.sun.org.apache.bcel.internal.generic.NEW;

public class updateBankRecon extends Composite {

	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	int counter=0;
	boolean narration1;

	Table ledgerReconReport;
	Table RecoStmt;
	TableColumn clrecoparticular;
	TableColumn clrecoamt;
	TableColumn Srno;
	TableColumn Date;
	TableColumn Particulars;
	TableColumn VoucherNumber;
	TableColumn Dr;
	TableColumn Cr;
	TableColumn Narration;
	TableColumn Clrdate;
	TableColumn Memo;
	TableItem headerRow;
	TableItem headerRow1;
	Label lblOrgDetails;
	Label lblheadline;
	Label lblorgname;
	Label lblsrno;
	Label lblDate;
	Label lblParticulars;
	Label lblVoucherNumber;
	Label lblNarration;
	Label lblDr;
	Label lblCr;
	Label lblclrdate;
	Label lblmemo;
	Label lblrecoparticular;
	Label lblrecoamt;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblPageName;
	Label lblPeriod;
	TableEditor recoparticulareditor;
	TableEditor recoamteditor;
	TableEditor srnoeditor;
	TableEditor DateEditor;
	TableEditor ParticularEditor;
	TableEditor VoucherNumberEditor;
	TableEditor DrEditotr;
	TableEditor CrEditor;
	TableEditor clreditor;
	TableEditor memoeditor;
	
	TableEditor NarrationEditor;
	static Display display;
	
	Button btnViewLedgerReconLedgerRecon;
	//Button btnViewLedgerReconLedgerForAccount;
	Button btnRecon;
	//Button btnViewLedgerReconDualLedger;
	Label cleareditem;
	Button Btnclear;
	//String tb;
	String strFromDate;
	String strToDate;
	ArrayList<Text> txtclearDates = new ArrayList<Text>();
	ArrayList<Text> txtmemos = new ArrayList<Text>();
	ArrayList<String> voucherCodes = new ArrayList<String>();
	ArrayList<String> refdates=new ArrayList<String>();
	ArrayList<String> accountnames=new ArrayList<String>();
	ArrayList<String> dramounts=new ArrayList<String>();
	ArrayList<String> cramounts=new ArrayList<String>();
	
	String bankname="";
	String startDate="";
	String endDate="";
	String projectName;
	String ledgerProject;
	//boolean narration1;
	boolean tbflag;
	boolean projectflag;
	boolean ledgerflag;
	boolean dualflag;
	String oldaccname;
	String oldfromdate;
	String oldenddate;
	String oldselectproject;
	String projectname;
	boolean oldnarration;
	Button btnok;
	ArrayList<Text> txtcleardate = new ArrayList<Text>();
	
	ArrayList<Text>  txtmemo= new ArrayList<Text>();
	//Vector<Object> printLedgerData = new Vector<Object>();
	public updateBankRecon(Composite parent,int style,String selectbank,String fromdate,String toDate,String financialStart,String ProjectName,Boolean narration, Object[] result_f) {
		super(parent, style);
		// TODO Auto-generated constructor stub
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		//txtaccname.setText(result[0].toString());
		
		/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+selectproject);
		msg.open();*/
		narration1= narration;
		bankname=selectbank;
		startDate=fromdate;
		endDate=toDate;
		projectname=ProjectName;
	
		/*MessageBox msg = new MessageBox(new Shell(), SWT.OK);
		msg.setMessage("Update caleed" );
		msg.open();*/
		//old values
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
	    
	    strFromDate=fromdate.substring(8)+"-"+fromdate.substring(5,7)+"-"+fromdate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
	  
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();

		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		 lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(35);
		lblheadline.setLayoutData(layout);
	

		
		Label lblAccName=new Label(this, SWT.NONE);
		lblAccName.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
		/*if(! ProjectName.equals("No Project"))
		{*/
		lblAccName.setText("Bank Name: "+bankname);
		/*}*/
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(5);
		lblAccName.setLayoutData(layout);
		

		lblPageName = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		lblPageName.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		lblPageName.setText("Period From "+strFromDate+" To "+strToDate);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(60);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblPageName.setLayoutData(layout);
		
		Label cleareditem1  = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		cleareditem1.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		cleareditem1.setText("BANK RECONCILIATION");
		layout = new FormData();
		layout.top = new FormAttachment(lblPageName,1);
		layout.left = new FormAttachment(30);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		cleareditem1.setLayoutData(layout);
	
		
		cleareditem  = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		cleareditem.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		cleareditem.setText("STATEMENT OF UNCLEARED ITEMS");
		layout = new FormData();
		layout.top = new FormAttachment(cleareditem1,1);
		layout.left = new FormAttachment(25);
	
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		cleareditem.setLayoutData(layout);
	
	

	
	    ledgerReconReport= new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    ledgerReconReport.setLinesVisible (true);
		ledgerReconReport.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(cleareditem,10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(55);
		ledgerReconReport.setLayoutData(layout);
		
		 RecoStmt= new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
		 RecoStmt.setLinesVisible (true);
		 RecoStmt.setHeaderVisible (false);
		 layout = new FormData();
		 layout.top = new FormAttachment(ledgerReconReport,10);
		 layout.left = new FormAttachment(20);
		 layout.right = new FormAttachment(70);
		 layout.bottom = new FormAttachment(80);
		 RecoStmt.setLayoutData(layout);
			
				
		btnRecon =new Button(this,SWT.PUSH);
		btnRecon.setText(" Reconcile ");
		btnRecon.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(RecoStmt,8);
		layout.left=new FormAttachment(20);
		btnRecon.setLayoutData(layout);
		
		
		btnViewLedgerReconLedgerRecon=new Button(this, SWT.PUSH);
		btnViewLedgerReconLedgerRecon.setText(" Back To Reconciliation Menu");
		btnViewLedgerReconLedgerRecon.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(RecoStmt,8);
		layout.left=new FormAttachment(btnRecon,20);
		btnViewLedgerReconLedgerRecon.setLayoutData(layout);
		

				
	  
	    this.makeaccessible(ledgerReconReport);
	    this.makeaccessible(RecoStmt);
	    this.getAccessible();
	    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	    this.setReport(result_f);
	    
	    //this.pack();
	    this.setEvents();
	   

	}
	private void setReport(Object[] result_f)
	{
		
		
		lblDate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDate.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		lblDate.setText("    Date   ");
		
		lblParticulars = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblParticulars.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblParticulars = new Label(ledgerReconReport,SWT.BORDER);
		lblParticulars.setText("    		Particulars		     ");
		
		lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblVoucherNumber.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER);
		lblVoucherNumber.setText(" V.Number");
		
		lblDr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblDr = new Label(ledgerReconReport,SWT.BORDER);
		lblDr.setText("    		Debit		     ");
		
		lblCr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblCr.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblCr = new Label(ledgerReconReport,SWT.BORDER);
		lblCr.setText("    		Credit		     ");
		
		lblNarration = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblNarration.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblNarration.setText("   Narration   ");
		
		lblclrdate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblclrdate.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblclrdate.setText("  Clearance Date   ");
		
		lblmemo = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblmemo.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblmemo.setText("   Memo   ");
		

		lblrecoparticular = new Label(RecoStmt,SWT.BORDER|SWT.CENTER);
		lblrecoparticular.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblrecoparticular.setText("  Particulars  ");
		
		lblrecoamt = new Label(RecoStmt,SWT.BORDER|SWT.CENTER);
		lblrecoamt.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblrecoamt.setText("   Amount   ");
		
		
		final int ledgwidth = ledgerReconReport.getClientArea().width;
		final int tblreco = RecoStmt.getClientArea().width;
		
		
		 ledgerReconReport.addListener(SWT.MeasureItem, new Listener() 
			{
		    	@Override
				public void handleEvent(Event event) {
					// TODO Auto-generated method stub
					// Srno.setWidth(5 * ledgwidth / 100);
		    		if(narration1==true)
					{
					
						Date.setWidth(10 * ledgwidth / 100);
						Particulars.setWidth(15 * ledgwidth / 100);
						VoucherNumber.setWidth(10 * ledgwidth / 100);
						Dr.setWidth(10 * ledgwidth / 100);
						Cr.setWidth(10 * ledgwidth / 100);
						Narration.setWidth(20 * ledgwidth / 100);
						Clrdate.setWidth(10 * ledgwidth / 100);
						Memo.setWidth(15 * ledgwidth / 100);
					}
		    		else
					{
						Date.setWidth(15 * ledgwidth / 100);
						Particulars.setWidth(35 * ledgwidth / 100);
						VoucherNumber.setWidth(10 * ledgwidth / 100);
						Dr.setWidth(20 * ledgwidth / 100);
						Cr.setWidth(20 * ledgwidth / 100);
						Clrdate.setWidth(15 * ledgwidth / 100);
						Memo.setWidth(15 * ledgwidth / 100);
			
					}


									
					event.height = 11;
				    
				    
				};
			});
		
		RecoStmt.addListener(SWT.MeasureItem, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				clrecoparticular.setWidth(70*tblreco/100);
				clrecoamt.setWidth(25*tblreco/100);
				//asset_tolamt.setWidth(25*tblreco/100);
				event.height = 11;
			}
		});
		
		 headerRow = new TableItem(ledgerReconReport, SWT.NONE);
			TableItem[] items = ledgerReconReport.getItems();
			//Srno = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
			Date = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
			Particulars = new TableColumn(ledgerReconReport, SWT.BORDER);
			VoucherNumber = new TableColumn(ledgerReconReport, SWT.RIGHT);
			Dr= new TableColumn(ledgerReconReport, SWT.RIGHT);
			Cr= new TableColumn(ledgerReconReport, SWT.RIGHT);
			if(narration1==true)
			{
				Narration=new TableColumn(ledgerReconReport, SWT.BORDER);
			}
			Clrdate= new TableColumn(ledgerReconReport, SWT.RIGHT);
			Memo= new TableColumn(ledgerReconReport, SWT.RIGHT);

			
			headerRow1 = new TableItem(RecoStmt, SWT.NONE);
				TableItem[] items1 = RecoStmt.getItems();
				//Srno = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
				clrecoparticular = new TableColumn(RecoStmt, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
				clrecoamt = new TableColumn(RecoStmt, SWT.BORDER);
				
			
		    DateEditor = new TableEditor(ledgerReconReport);
			DateEditor.grabHorizontal = true;
			DateEditor.setEditor(lblDate,items[0],0);
			
			ParticularEditor = new TableEditor(ledgerReconReport);
			ParticularEditor.grabHorizontal = true;
			ParticularEditor.setEditor(lblParticulars,items[0],1);
			
			VoucherNumberEditor = new TableEditor(ledgerReconReport);
			VoucherNumberEditor.grabHorizontal = true;
			VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
			
			DrEditotr = new TableEditor(ledgerReconReport);
			DrEditotr.grabHorizontal = true;
			DrEditotr.setEditor(lblDr,items[0],3);
			
			CrEditor = new TableEditor(ledgerReconReport);
			CrEditor.grabHorizontal = true;
			CrEditor.setEditor(lblCr,items[0],4);
			
			
			if(narration1==true)
			{ 
				NarrationEditor = new TableEditor(ledgerReconReport);
				NarrationEditor.grabHorizontal = true;
				NarrationEditor.setEditor(lblNarration,items[0],5);
				Narration.pack();
				
				clreditor = new TableEditor(ledgerReconReport);
				clreditor.grabHorizontal = true;
				clreditor.setEditor(lblclrdate,items[0],6);
				memoeditor = new TableEditor(ledgerReconReport);
				memoeditor.grabHorizontal = true;
				memoeditor.setEditor(lblmemo,items[0],7);
				
			}
			else
			{
				clreditor = new TableEditor(ledgerReconReport);
				clreditor.grabHorizontal = true;
				clreditor.setEditor(lblclrdate,items[0],5);
				memoeditor = new TableEditor(ledgerReconReport);
				memoeditor.grabHorizontal = true;
				memoeditor.setEditor(lblmemo,items[0],6);
			}
			//Srno.pack();
			Date.pack();
			Particulars.pack();
			VoucherNumber.pack();
		    Dr.pack();
			Cr.pack();
			
			Clrdate.pack();
			Memo.pack();
			
			
			double dr = 0.00;
			double cr = 0.00;
			
			voucherCodes.clear();
			refdates.clear();
			accountnames.clear();
			dramounts.clear();
			cramounts.clear();
			txtclearDates.clear();
			txtmemos.clear();
			for(int rowcounter =0; rowcounter <= result_f.length -7; rowcounter ++)
			{
				
				TableItem tbrow = new TableItem(ledgerReconReport , SWT.NONE);
				Object[] ledgerrecorecord = (Object[]) result_f[rowcounter];
	
				if(rowcounter == result_f.length-7)
					
				{
					tbrow.setText(0,"");
					tbrow.setText(1,"");
					tbrow.setText(2,"Total");
					tbrow.setFont(new Font(display, "Times New Roman",12, SWT.BOLD));
					tbrow.setText(3, ledgerrecorecord[4].toString());
					tbrow.setText(4, ledgerrecorecord[5].toString());
					break;
				}
				
					tbrow.setText(0, ledgerrecorecord[0].toString());
					refdates.add(ledgerrecorecord[0].toString());
					tbrow.setText(1, ledgerrecorecord[1].toString());
					accountnames.add(ledgerrecorecord[1].toString());
					tbrow.setText(2, ledgerrecorecord[2].toString());
					voucherCodes.add(ledgerrecorecord[3].toString());
					tbrow.setText(3, ledgerrecorecord[4].toString());
					dramounts.add(ledgerrecorecord[4].toString());
					tbrow.setText(4, ledgerrecorecord[5].toString());
					cramounts.add(ledgerrecorecord[5].toString());
				
					/*tbrow.setText(0, ledgerrecorecord[0].toString());
					tbrow.setText(1, ledgerrecorecord[1].toString());
					tbrow.setText(2, ledgerrecorecord[3].toString());
					tbrow.setText(3, ledgerrecorecord[4].toString());
					tbrow.setText(4, ledgerrecorecord[5].toString());*/
				
					if(narration1==true)
					{
						if(ledgerrecorecord.length==7)
						{
							tbrow.setText(5,ledgerrecorecord[6].toString() );
							Text txtcldt = new Text(ledgerReconReport , SWT.NONE);
							txtcldt.setVisible(true);
							TableEditor txteditcl = new TableEditor(ledgerReconReport);
							txteditcl.grabHorizontal = true;
							txteditcl.setEditor(txtcldt,tbrow,6);
							txtclearDates.add(txtcldt);
						//	txtcldt.setFocus();
						
							Text txtmem = new Text(ledgerReconReport , SWT.NONE);
							txtmem.setEditable(true);
							TableEditor txteditmem = new TableEditor(ledgerReconReport);
							txtmem.setText("");
							txteditmem.grabHorizontal = true;
							txteditmem.setEditor(txtmem,tbrow,7);
							txtmemos.add(txtmem);
						
		
						}
						else
						{
							tbrow.setText(5,"" );
							Text txtcldt = new Text(ledgerReconReport , SWT.NONE);
							txtcldt.setVisible(true);
							TableEditor txteditcl = new TableEditor(ledgerReconReport);
							txteditcl.grabHorizontal = true;
							txteditcl.setEditor(txtcldt,tbrow,6);
							txtclearDates.add(txtcldt);
						//	txtcldt.setFocus();
						
							Text txtmem = new Text(ledgerReconReport , SWT.NONE);
							txtmem.setEditable(true);
							TableEditor txteditmem = new TableEditor(ledgerReconReport);
							txtmem.setText("");
							txteditmem.grabHorizontal = true;
							txteditmem.setEditor(txtmem,tbrow,7);
							txtmemos.add(txtmem);
						
		
						}
				
				
					
				}
				else
				{
					Text txtcldt = new Text(ledgerReconReport , SWT.NONE);
					txtcldt.setVisible(true);
		            TableEditor txteditcl = new TableEditor(ledgerReconReport);
					txteditcl.grabHorizontal = true;
					txteditcl.setEditor(txtcldt,tbrow,5);
					txtclearDates.add(txtcldt);
					//txtcldt.setFocus();
					
					Text txtmem = new Text(ledgerReconReport , SWT.NONE);
					txtmem.setEditable(true);
					TableEditor txteditmem = new TableEditor(ledgerReconReport);
					txtmem.setText("");
					txteditmem.grabHorizontal = true;
					txteditmem.setEditor(txtmem,tbrow,6);
					txtmemos.add(txtmem);
					
								
				}

				
			//}
			
			
			}
			recoparticulareditor = new TableEditor(RecoStmt);
			recoparticulareditor.grabHorizontal = true;
			recoparticulareditor.setEditor(lblrecoparticular,items1[0],0);
			
			recoamteditor = new TableEditor(RecoStmt);
			recoamteditor.grabHorizontal = true;
			recoamteditor.setEditor(lblrecoamt,items1[0],1);

			//Srno.pack();
			clrecoparticular.pack();
			clrecoamt.pack();
			
			for(int rowcounter = result_f.length -5;  rowcounter < result_f.length; rowcounter ++)
			{
				
				TableItem tbrow = new TableItem(RecoStmt , SWT.NONE);
				Object[] ledgerrecorecord = (Object[]) result_f[rowcounter];
	
				
					tbrow.setText(0, ledgerrecorecord[1].toString());
					tbrow.setText(1, ledgerrecorecord[5].toString());
					

					
				
			}
	}
	
	public void setEvents()
	{
		
				
		
		btnViewLedgerReconLedgerRecon.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnRecon.setFocus();
				}
				
			}
		});
		
		
		btnRecon.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewLedgerReconLedgerRecon.setFocus();
				}
				
			}
		});
		
		btnRecon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				int reconCounter = 0;
				for (int counter = 0; counter < txtclearDates.size(); counter ++)
				{
					if(! txtclearDates.get(counter).getText().equals(""))
					{
						reconCounter ++;
					}
				}
				Object[][] reconData = new Object[reconCounter][7];
				int rowMarker = 0;
				for(int fillrecon = 0; fillrecon< txtclearDates.size(); fillrecon ++)
				{
					if(! txtclearDates.get(fillrecon).getText().equals(""))
					{
						reconData[rowMarker][0] = voucherCodes.get(fillrecon);
						reconData[rowMarker][1] = refdates.get(fillrecon).substring(6) + "-" + refdates.get(fillrecon).substring(3,5)+"-"+refdates.get(fillrecon).substring(0,2); 
						reconData[rowMarker][2] =  accountnames.get(fillrecon);
						reconData[rowMarker][3] = dramounts.get(fillrecon);
						reconData[rowMarker][4] = cramounts.get(fillrecon);
						reconData[rowMarker][5] = txtclearDates.get(fillrecon).getText().substring(6)+"-"+txtclearDates.get(fillrecon).getText().substring(3,5)+"-"+txtclearDates.get(fillrecon).getText().substring(0,2);
						reconData[rowMarker][6] = txtmemos.get(fillrecon).getText();
						
						rowMarker ++;
					}
				}
				
				Composite grandParent= (Composite)btnRecon.getParent().getParent();
				//dispose();
				btnRecon.getParent().dispose();
				reportController.setReconcile(grandParent, reconData, bankname, startDate, endDate,strFromYear,projectname,narration1);

			
				}
		
		});

		btnViewLedgerReconLedgerRecon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewLedgerReconLedgerRecon.getParent().getParent();
				btnViewLedgerReconLedgerRecon.getParent().dispose();
				
				viewReconciliation vl=new viewReconciliation(grandParent,SWT.NONE,narration1);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
			ledgerReconReport.setFocus();
			}
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}

}
	

	